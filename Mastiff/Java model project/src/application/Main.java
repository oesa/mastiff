/*
 * Main for run application
 */
package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;


/**
*
* @author Laura Meier
* @version 05.07.21
*/


public class Main extends Application {
	 private Stage primaryStage;
	    private AnchorPane rootLayout;
	    public static int varPar1;
	    public static double varPar2;

	    @Override
	    public void start(Stage primaryStage) {
	    	
	    	
	        this.primaryStage = primaryStage;
	        this.primaryStage.setTitle("Mastiff Modell");
	      

	        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Icon_Mastiff2.png")));
	        
	        initRootLayout();
	    }
	    
	    /**
	     * Initializes the root layout.
	     */
	    public void initRootLayout() {
	        try {
	            // Load root layout from fxml file.
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(Main.class.getResource("view/MainLayout.fxml"));
	            rootLayout = loader.load();
	            
	            // Show the scene containing the root layout.
	            Scene scene = new Scene(rootLayout);
	            primaryStage.setScene(scene);
	            primaryStage.show();
	            
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	
	public static void main(String[] args) {
		if(args.length != 0) {
			varPar1 = Integer.valueOf(args[0]);
			varPar2 = Double.valueOf(args[1]);
		}
		launch(args);
	}
}
