package application.view;

/**
*
* @author Laura Meier
* @version 05.07.21
*/


	public class MySegment {

		private int segID;
		private String LUT;
		private double shade;
	    private double epsi;
	    private double mu;
	    private double delta;
	    private double nut;
	    private double beta1;
	    private double Binit;
	    private int limpet1;
	    private int limpet2;
	    private int mayfly;
	    private int chironomid;
	    private int stonefly1;
	    private int stonefly2;
	    private int stonefly3;
	    private double herbi;
	    private double insect;
		
		

		public MySegment(int segID, String LUT, double shade , double epsi, double mu, double delta, double nut, double beta1, double Binit, int limpet1, int limpet2, int mayfly, int chironomid, int stonefly1, int stonefly2, int stonefly3, double herbi, double insect) {

			this.segID = segID;
			this.LUT = LUT;
			this.shade = shade;
	        this.epsi = epsi;
	        this.mu = mu;
	        this.delta = delta;
	        this.nut = nut;
	        this.beta1 = beta1;
	        this.Binit = Binit;
	        this.limpet1 = limpet1;
	        this.limpet2 = limpet2;
	        this.mayfly = mayfly;
	        this.chironomid = chironomid;
	        this.stonefly1 = stonefly1;
	        this.stonefly2 = stonefly2;
	        this.stonefly3 = stonefly3;
	        this.herbi = herbi;
	        this.insect = insect;
			
		}

		// ******** Getter and Setter methods *********
		public double getBeta1() {
			return beta1;
		}

		
		public void setBeta1(double beta1) {
			this.beta1 = beta1;
		}
		
		
		public double getNut() {
			return nut;
		}

		
		public void setNut(double nut) {
			this.nut = nut;
		}
		
		
		public double getHerbi() {
			return herbi;
		}


		public void setHerbi(double herbi) {
			this.herbi = herbi;
		}


		public double getInsect() {
			return insect;
		}


		public void setInsect(double insect) {
			this.insect = insect;
		}


		public int getSegID() {
			return segID;
		}


		public void setSegID(int segID) {
			this.segID = segID;
		}

		
		public String getLUT() {
			return LUT;
		}


		public void setLUT(String LUT) {
			this.LUT = LUT;
		}

		public double getShade() {
			return shade;
		}


		public void setShade(double shade) {
			this.shade = shade;
		}


		public double getEpsi() {
			return epsi;
		}


		public void setEpsi(double epsi) {
			this.epsi = epsi;
		}


		public double getMu() {
			return mu;
		}


		public void setMu(double mu) {
			this.mu = mu;
		}


		public double getDelta() {
			return delta;
		}


		public void setDelta(double delta) {
			this.delta = delta;
		}


		public double getBinit() {
			return Binit;
		}


		public void setBinit(double binit) {
			Binit = binit;
		}


		public int getLimpet1() {
			return limpet1;
		}


		public void setLimpet1(int limpet1) {
			this.limpet1 = limpet1;
		}


		public int getLimpet2() {
			return limpet2;
		}


		public void setLimpet2(int limpet2) {
			this.limpet2 = limpet2;
		}


		public int getMayfly() {
			return mayfly;
		}


		public void setMayfly(int mayfly) {
			this.mayfly = mayfly;
		}


		public int getChironomid() {
			return chironomid;
		}


		public void setChironomid(int chironomid) {
			this.chironomid = chironomid;
		}


		public int getStonefly1() {
			return stonefly1;
		}


		public void setStonefly1(int stonefly1) {
			this.stonefly1 = stonefly1;
		}


		public int getStonefly2() {
			return stonefly2;
		}


		public void setStonefly2(int stonefly2) {
			this.stonefly2 = stonefly2;
		}


		public int getStonefly3() {
			return stonefly3;
		}


		public void setStonefly3(int stonefly3) {
			this.stonefly3 = stonefly3;
		}
	
}
