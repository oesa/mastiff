/*
 * Main Controller for GUI
 */

package application.view;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.paint.Color;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

import rivermodel.SimulationMain;

/**
*
* @author Laura Meier
* @version 05.07.21
*/

public class MainController {

	@FXML
	private Button ButtonInit;

	@FXML
	private Button ButtonRun;

	@FXML
	private Button ButtonRunM;
	
	@FXML
	private Label LabelRunning;
	
	@FXML
	private Label LabelRunningM;
	
	@FXML 
	private ProgressBar bar;
	
	@FXML 
	private ProgressBar barM;

	@FXML 
	protected TextField Scenario;
	
	@FXML 
	protected TextField ScenarioM;

	@FXML
	private TextField Runs;
	
	@FXML
	private TextField EnterPath;

	@FXML
	private TextField EnterPathM;
	
	@FXML
	private TableView<MySegment> TableParameter;

	@FXML
	private TableColumn<MySegment, Integer> SegC;

	@FXML
	private TableColumn<MySegment, Double> ShadeC;

	@FXML
	private TableColumn<MySegment, Double> epsiC;

	@FXML
	private TableColumn<MySegment, Double> muC;

	@FXML
	private TableColumn<MySegment, Double> deltaC;

	@FXML
	private TableColumn<MySegment, Double> BinitC;

	@FXML
	private TableColumn<MySegment, Integer> Limpet1C;

	@FXML
	private TableColumn<MySegment, Integer> Limpet2C;

	@FXML
	private TableColumn<MySegment, Integer> MayflyC;

	@FXML
	private TableColumn<MySegment, Integer> ChironomidC;

	@FXML
	private TableColumn<MySegment, Integer> Stonefly1C;

	@FXML
	private TableColumn<MySegment, Integer> Stonefly2C;

	@FXML
	private TableColumn<MySegment, Integer> Stonefly3C;

	@FXML
	private TableColumn<MySegment, Double> HerbiC;

	@FXML
	private TableColumn<MySegment, Double> InsectiC;
	
	@FXML
	private TableColumn<MySegment, Double> NutC;
	
	@FXML
	private TableColumn<MySegment, String> LUTC;
	
	@FXML
	private TableColumn<MySegment, Double> beta1C;

	@FXML
	protected TextField SegNumb;

	@FXML
	private TextField StartMonth;

	@FXML
	protected TextField SimTime;

	@FXML
	private CheckBox IntegerT;

	@FXML
	private TextField ShadetypA;

	@FXML
	private TextField ShadetypB;
	
	@FXML
	private TextField ShadetypBStar;

	@FXML
	private TextField ShadetypC;

	@FXML
	private TextField ShadetypD;

	@FXML
	private TextField ShadetypE;

	@FXML
	private TextField ShadetypF;
	
	@FXML
	private CheckBox nonseasonalShade;

	@FXML
	private TextField ANut;

	@FXML
	private TextField BNut;
	
	@FXML
	private TextField BStarNut;

	@FXML
	private TextField CNut;

	@FXML
	private TextField DNut;

	@FXML
	private TextField ENut;

	@FXML
	private TextField FNut;
	
	@FXML
	private TextField ASeg;

	@FXML
	private TextField BSeg;
	
	@FXML
	private TextField BStarSeg;

	@FXML
	private TextField CSeg;

	@FXML
	private TextField DSeg;

	@FXML
	private TextField ESeg;

	@FXML
	private TextField FSeg;
	
	@FXML
	private CheckBox ManualLUT;
	
	@FXML
	private TextField SequenceLUT;

	@FXML
	private TextField EpsilonMin;

	@FXML
	private TextField EpsilonMax;

	@FXML
	private TextField MuMin;

	@FXML
	private TextField MuMax;
	
	@FXML
	private TextField beta1Min;

	@FXML
	private TextField beta1Max;
	
	@FXML
	private TextField beta0;
	
	@FXML
	private TextField bRate;
	
	@FXML
	private TextField B0;
	
	@FXML
	private TextField B1;
	
	@FXML
	private TextField pfMax;
	
	@FXML
	private TextField betaF;
	
	@FXML
	private CheckBox stochLength;
	
	@FXML
	private CheckBox DepOnLight;
	
	@FXML
	private CheckBox GrowWinter;

	@FXML
	private TextField delta;
	
	@FXML
	private TextField minNut;
	
	@FXML
	private CheckBox constNut;
	
	@FXML
	private TextField scmortality;
	
	@FXML
	private CheckBox ddM;
	
	@FXML
	private TextField mortC;
	
	@FXML
	private TextField mortM;
	
	@FXML
	private TextField mortL;
	
	@FXML
	private TextField mortEggFila;
	
	@FXML
	private Label LabelOff;
	
	@FXML
	private TextField offspringC;
	
	@FXML
	private TextField offspringM;
	
	@FXML
	private TextField offspringL;
	
	@FXML
	private Label LabelOffAbs;
	
	@FXML
	private TextField offspringCAbs;
	
	@FXML
	private TextField offspringMAbs;
	
	@FXML
	private TextField offspringLAbs;
	
	@FXML 
	private TextField scOff;
	
	@FXML
	private TextField probstayM;
	
	@FXML
	private TextField probstayC;
	
	@FXML
	private TextField probupM;
	
	@FXML
	private TextField probupC;
	
	@FXML
	private TextField probdownM;
	
	@FXML
	private TextField probdownC;
	
	@FXML
	private TextField distmeanupM;
	
	@FXML
	private TextField distmeanupC;
	
	@FXML
	private TextField distmeandownM;
	
	@FXML
	private TextField distmeandownC;
	
	@FXML
	private RadioButton ovidyn;
	
	@FXML
	private RadioButton ovicon;
	
	@FXML
	private RadioButton ovihom;
	
	@FXML
	private RadioButton ovisum;
	
	@FXML
	private TextField airTempSh;
	
	@FXML
	private TextField airTempNo;

	@FXML
	private CheckBox constTemp;
	
	@FXML
	private TextField Binit;

	@FXML
	protected TextField Limpet1;

	@FXML
	protected TextField Limpet2;

	@FXML
	protected TextField Mayfly;

	@FXML
	protected TextField Chironomid;

	@FXML
	private TextField Stonefly1;

	@FXML
	private TextField Stonefly2;

	@FXML
	private TextField Stonefly3;
	
	@FXML
	private RadioButton withGrazers;
	
	@FXML
	private RadioButton withoutGrazers;

	@FXML
	private Slider grazingMethode;
	
	@FXML
	private TextField compL;
	
	@FXML
	private TextField compM;
	
	@FXML
	private TextField compC;
	
	@FXML
	private Slider grazingFila;
	
	@FXML
	private TextField HerbicideConDiff;

	@FXML
	private TextField InsecticideConDiff;
	
	@FXML
	private TextField HerbicideConPoint;

	@FXML
	private TextField InsecticideConPoint;

	@FXML
	private TextField Till;

	@FXML
	private CheckBox upstreamDirection;

	@FXML
	private TextField StressorEvent;

	@FXML
	private Button AddStressor;

	@FXML
	private ListView ListStress;
	
	@FXML
	private TextField tempRiseT;
	
	@FXML
	private TextField heatWavesWeeks;
	
	@FXML
	private CheckBox OutAir;
	
	@FXML
	private CheckBox OutWater;
	
	@FXML
	private CheckBox OutCon;
	
	@FXML
	private CheckBox OutCon2;
	
	@FXML
	private CheckBox OutPartEu;
	
	@FXML
	private CheckBox OutpF;
	
	@FXML
	private CheckBox OutBiofilm;
	
	@FXML
	private CheckBox OutAbun;
	
	@FXML
	private CheckBox OutDemand;
	
	@FXML
	private CheckBox OutFeed;
	
	@FXML
	private CheckBox OutInfOutflow;
	
	@FXML
	private CheckBox OutDisLoss;
	
	@FXML
	private CheckBox OutAbunBio;
	
	@FXML
	private CheckBox OutHerbi;
	
	@FXML
	private CheckBox OutInsecti;
	
	@FXML
	private CheckBox OutHerbiE;
	
	@FXML
	private CheckBox OutInsectiE;
	
	@FXML
	private CheckBox ProofCoexist;
	
	@FXML
	private CheckBox ProofRange;
	
	
	BufferedWriter bw10 = null;
	FileWriter fw10 = null;
	ObservableList<String> myList = FXCollections.observableArrayList("#Month,Segment,Type,Intensity,k");

	String[] LUTData;
	double[] shadeData;
	double[] epsiData;
	double[] muData;
	double[] deltaData;
	double[] nutData;
	double[] beta1Data;
	double[] airTempData;
	double[] BinitData;
	int[] Limpet1Data;
	int[] Limpet2Data;
	int[] MayflyData;
	int[] ChironomidData;
	int[] Stonefly1Data;
	int[] Stonefly2Data;
	int[] Stonefly3Data;
	double[] herbiData;
	double[] insectiData;

	/**
	 * Initialize model
	 */
	@FXML
	protected void initialize() {
		
		int s = Integer.parseInt(SegNumb.getText());
		shadeData = new double[s];
		epsiData = new double[s];
		muData = new double[s];
		deltaData = new double[s];
		nutData = new double[s];
		beta1Data = new double[s];
		BinitData = new double[s];
		Limpet1Data = new int[s];
		Limpet2Data = new int[s];
		MayflyData = new int[s];
		ChironomidData = new int[s];
		Stonefly1Data = new int[s];
		Stonefly2Data = new int[s];
		Stonefly3Data = new int[s];
		herbiData = new double[s];
		insectiData = new double[s];
		changeMineN();

//		String filename10 = SimulationMain.pathString + SimulationMain.pathParam + "Stressor_Events"+ Scenario.getText() + ".csv";
//		//System.out.println(filename10);
//		
//		try {
//			fw10 = new FileWriter(filename10);
//			bw10 = new BufferedWriter(fw10);
//
//			// Write into file
//
//			bw10.write("#t,#seg,#pesticide,#intensity,#k_in_e^kc");
//			bw10.newLine();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (bw10 != null)
//					bw10.close();
//				if (fw10 != null)
//					fw10.close();
//			} catch (IOException ex) {
//				ex.printStackTrace();
//			}
//		}

		//ListStress.setItems(myList);
		

		if(SimulationMain.autoRan) onButtonClickvaryPar();
		
	}

	/**
	 * Initialize parameters: chosen/change parameter in GUI set to run model
	 */
	@FXML
	public void onButtonClickInitialize() {
		
		bar.setProgress(0.0);
		int s = Integer.parseInt(SegNumb.getText());
		shadeData = new double[s];
		epsiData = new double[s];
		muData = new double[s];
		deltaData = new double[s];
		nutData = new double[s];
		beta1Data = new double[s];
		BinitData = new double[s];
		Limpet1Data = new int[s];
		Limpet2Data = new int[s];
		MayflyData = new int[s];
		ChironomidData = new int[s];
		Stonefly1Data = new int[s];
		Stonefly2Data = new int[s];
		Stonefly3Data = new int[s];

		// Parameter in Text Datein schreiben um sie vom Modell einlesen zu lassen
		if (Integer.parseInt(SegNumb.getText()) < Integer.parseInt(Till.getText())) Till.setText(SegNumb.getText());
		setParameter();
		setSpecies();
		setStressorsCon();

		ButtonRun.setDisable(false);

		showInTable();
		LabelRunning.setText("Press Run Model!");
		
	}

	/**
	 * Save input parameters in text files to read in for simulation 
	 */
	private void saveInTextFiles() {
		// Parameter in Text Datein schreiben um sie vom Modell einlesen zu lassen

		saveParameter(SimulationMain.pathString + SimulationMain.pathParam +"Parameters_"+ Scenario.getText() + ".csv/");
		saveParameterSeg(SimulationMain.pathString + SimulationMain.pathParam + "ParametersSeg_"+ Scenario.getText() + ".csv/");
		saveSpecies(SimulationMain.pathString + SimulationMain.pathParam + "SpeciesTab_"+ Scenario.getText() + ".csv/");
		saveStressorsCon(SimulationMain.pathString + SimulationMain.pathParam + "Stressors_Herbicide_"+ Scenario.getText() + ".csv/", SimulationMain.pathString + SimulationMain.pathParam + "Stressors_Insecticide_"+ Scenario.getText() + ".csv/");
		saveStressorEvent(SimulationMain.pathString + SimulationMain.pathParam +"Stressor_Events_"+ Scenario.getText() + ".csv/");
		saveOutput(SimulationMain.pathString + SimulationMain.pathParam +"OutputFiles_"+ Scenario.getText() + ".csv/");
	}
	
	/**
	 * Save input parameters in folder with results
	 */
	private void saveInTable() {
		
		String filepath = SimulationMain.pathString + SimulationMain.pathOutput + EnterPath.getText();
		
		saveParameter(filepath +"/Parameters_" + Scenario.getText() + ".csv/");
		saveParameterSeg(filepath +"/ParametersSeg_" + Scenario.getText() + ".csv/");
		saveSpecies(filepath +"/SpeciesTab_"+ Scenario.getText() + ".csv/");
		saveStressorsCon(filepath +"/Stressors_Herbicide_"+ Scenario.getText() + ".csv/",filepath +"/Stressors_Insecticide_"+ Scenario.getText() + ".csv/");
		saveStressorEvent(filepath +"/Stressor_Events_" + Scenario.getText() + ".csv/");
		saveOutput(filepath +"/OutputFiles_" + Scenario.getText() + ".csv/");
		
		final String filename = filepath +"/ParameterTable_"+ Scenario.getText() + ".csv/";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file	
			bw.write("#SegNumb, Landusetype, Shade, Epsilon, Mu, Delta, Nutrients, beta1, Binit, Limpet1, Limpet2, Myfly, Chironomid, Stonefly1, Stonefly2, Stonefly3, herbicide, insecticide");
			
			for (int i = 0; i < Double.parseDouble(SegNumb.getText()); i++) {
				bw.newLine();
				bw.write(i + "," + LUTData[i] + "," + shadeData[i] + ","+ epsiData[i] +","+ muData[i]+","+ deltaData[i]+"," + nutData[i] +"," + beta1Data[i] + ","+ BinitData[i]+","+
						Limpet1Data[i]+","+ Limpet2Data[i]+","+ MayflyData[i]+","+ ChironomidData[i]+","+ Stonefly1Data[i]+","+
						Stonefly2Data[i]+","+ Stonefly3Data[i]+","+ herbiData[i]+","+ insectiData[i]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Run simulation (save parameters, start simulation)
	 */
	@FXML
	public void onButtonClickRun() {
		// Save in TextFiles
		bar.setProgress(0.25);
		saveInTextFiles();
		saveInTable();
		bar.setProgress(0.5);
		// Run Model
		LabelRunning.setText("Model is running!");
		LabelRunning.setTextFill(Color.RED);
		String[] ScenarioSet = new String[3];
		ScenarioSet[0] = Scenario.getText();
		ScenarioSet[1] = Runs.getText();
		ScenarioSet[2] = EnterPath.getText();
		rivermodel.SimulationMain.main(ScenarioSet);
		LabelRunning.setText("Simulation finished!");
		LabelRunning.setTextFill(Color.GREEN);
		bar.setProgress(1.0);

	}

	/**
	 * Update data in Table in GUI
	 */
	@FXML
	public void showInTable() {

		SegC.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("segID"));
		LUTC.setCellValueFactory(new PropertyValueFactory<MySegment, String>("LUT"));
		ShadeC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("shade"));
		epsiC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("epsi"));
		muC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("mu"));
		deltaC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("delta"));
		NutC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("nut"));
		beta1C.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("beta1"));
		BinitC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("Binit"));
		Limpet1C.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("limpet1"));
		Limpet2C.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("limpet2"));
		MayflyC.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("mayfly"));
		ChironomidC.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("chironomid"));
		Stonefly1C.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("stonefly1"));
		Stonefly2C.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("stonefly2"));
		Stonefly3C.setCellValueFactory(new PropertyValueFactory<MySegment, Integer>("stonefly3"));
		HerbiC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("herbi"));
		InsectiC.setCellValueFactory(new PropertyValueFactory<MySegment, Double>("insect"));

		ObservableList<MySegment> d = FXCollections.observableArrayList();

		int s = Integer.parseInt(SegNumb.getText());
		for (int i = 0; i < s; i++) {
			d.add(new MySegment(i, LUTData[i],shadeData[i], epsiData[i], muData[i], deltaData[i], nutData[i],beta1Data[i], BinitData[i],
					Limpet1Data[i], Limpet2Data[i], MayflyData[i], ChironomidData[i], Stonefly1Data[i],
					Stonefly2Data[i], Stonefly3Data[i], herbiData[i], insectiData[i]));
		}

		TableParameter.setItems(d);
		
		TableParameter.setEditable(true);
		ShadeC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		epsiC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		muC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		deltaC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		NutC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		beta1C.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		BinitC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));	
		Limpet1C.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		Limpet2C.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		MayflyC.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		ChironomidC.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		Stonefly1C.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		Stonefly2C.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		Stonefly3C.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		HerbiC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
		InsectiC.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
	}
	
	/**
	 * Change selected parameter in a specific segment
	 * @param edditedCell click into field and change it
	 */
	public void onEditCellShade(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setShade(Double.parseDouble(edditedCell.getNewValue().toString()));	
		shadeData[segmentSelect.getSegID()] = segmentSelect.getShade();
	}	
	
	public void onEditCellEpsi(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setEpsi(Double.parseDouble(edditedCell.getNewValue().toString()));	
		epsiData[segmentSelect.getSegID()] = segmentSelect.getEpsi();
	}	
	
	public void onEditCellMu(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setMu(Double.parseDouble(edditedCell.getNewValue().toString()));	
		muData[segmentSelect.getSegID()] = segmentSelect.getMu();
	}		

	public void onEditCellDelta(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setDelta(Double.parseDouble(edditedCell.getNewValue().toString()));	
		deltaData[segmentSelect.getSegID()] = segmentSelect.getDelta();
	}	
	
	public void onEditCellNut(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setNut(Double.parseDouble(edditedCell.getNewValue().toString()));	
		nutData[segmentSelect.getSegID()] = segmentSelect.getNut();
	}	
	
	public void onEditCellBeta1(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setBeta1(Double.parseDouble(edditedCell.getNewValue().toString()));	
		beta1Data[segmentSelect.getSegID()] = segmentSelect.getBeta1();
	}	
	
	public void onEditCellBinit(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setBinit(Double.parseDouble(edditedCell.getNewValue().toString()));	
		BinitData[segmentSelect.getSegID()] = segmentSelect.getBinit();
	}		
	
	public void onEditCellL1(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setLimpet1(Integer.parseInt(edditedCell.getNewValue().toString()));	
		Limpet1Data[segmentSelect.getSegID()] = segmentSelect.getLimpet1();
	}

	public void onEditCellL2(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setLimpet2(Integer.parseInt(edditedCell.getNewValue().toString()));	
		Limpet2Data[segmentSelect.getSegID()] = segmentSelect.getLimpet2();
	}

	public void onEditCellM(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setMayfly(Integer.parseInt(edditedCell.getNewValue().toString()));
		MayflyData[segmentSelect.getSegID()] = segmentSelect.getMayfly();
	}
	
	public void onEditCellC(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setChironomid(Integer.parseInt(edditedCell.getNewValue().toString()));	
		ChironomidData[segmentSelect.getSegID()] = segmentSelect.getChironomid();
	}
	
	public void onEditCellS1(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setStonefly1(Integer.parseInt(edditedCell.getNewValue().toString()));	
		Stonefly1Data[segmentSelect.getSegID()] = segmentSelect.getStonefly1();
	}
	
	public void onEditCellS2(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setStonefly2(Integer.parseInt(edditedCell.getNewValue().toString()));	
		Stonefly2Data[segmentSelect.getSegID()] = segmentSelect.getStonefly2();
	}
	
	public void onEditCellS3(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setStonefly3(Integer.parseInt(edditedCell.getNewValue().toString()));	
		Stonefly3Data[segmentSelect.getSegID()] = segmentSelect.getStonefly3();
	}
	
	public void onEditCellHerbi(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setHerbi(Double.parseDouble(edditedCell.getNewValue().toString()));	
		herbiData[segmentSelect.getSegID()] = segmentSelect.getHerbi();
	}
	
	public void onEditCellInsect(CellEditEvent edditedCell) {
		MySegment segmentSelect = TableParameter.getSelectionModel().getSelectedItem();
		segmentSelect.setInsect(Double.parseDouble(edditedCell.getNewValue().toString()));
		insectiData[segmentSelect.getSegID()] = segmentSelect.getInsect();
	}
	
	/**
	 * Create range
	 * @param seg number of segments
	 * @param start start value
	 * @param end end value
	 * @return vector with gradient from start to end
	 */
	private double[] createRange(int seg, double start, double end) {
		double[] range = new double[seg];
		double h;
		if(seg ==1) {
			h = 0;
		}else {
			h = (end - start) / ((double) seg - 1);
		} 
		
		for (int i = 0; i < seg; i++) {
			range[i] = start + h * i;
		}
		return range;
	}

	/**
	 * Create range from a given segment on
	 * @param seg number of segments in total
	 * @param forHowMany segment at which the gradient ends 
	 * @param start start value
	 * @param end end value at the specific segment
	 * @return vector with gradient till forHowMany and zeros for the rest
	 */
	private double[] createRangeS(int seg, int forHowMany, double start, double end) {
		double[] range = new double[seg];
		double h;
		if (seg == 1) {
			h = 0;
		}else {
			h = (end - start) / ((double) forHowMany - 1);
		}
		
		for (int i = 0; i < forHowMany; i++) {
			range[i] = start + h * i;
		}
		for (int i = forHowMany; i < seg; i++) {
			range[i] = 0;
		}

		return range;
	}

	/**
	 * Set maximal shading for a segment
	 * @param type + if shaded, - if not shaded
	 * @return maximal shading
	 */
	private double setShade(String type) {
		if(type.equals("+")) {
			return 0.6; //shaded
		}
		else if(type.equals("-")) {
			return 0.0; //unshaded
		}
		else return -1;
	}
	
	/**
	 * Set air temperature for a segment
	 * @param type +, air temperature for shaded segment, - air temperature for not shaded segment
	 * @return air temperature
	 */
	private String setAirtemp(String type) {
			if(type.equals("+")) {
				return airTempSh.getText(); //shaded
			}
			else if(type.equals("-")) {
				return airTempNo.getText(); //unshaded
			}
			else return "NoValidEntry";
		
	}
	
	
	/**
	 * Set nutrients
	 * @param nut nutrients factor for each segment given by land use type
	 * @return vector with nutrient factor for each segment
	 */
	private double[] setNutrients2(double[] nut) { //nur mineN mineN 1 1 1 mineN 1 1 
		double[] out = new double[Integer.parseInt(SegNumb.getText())];
		boolean alreadyNut = false;

		for(int i = 0; i < Integer.parseInt(SegNumb.getText()); i++) {
			if(nut[i] != 1 && alreadyNut == false) { //upstream minNut as long no 1
				out[i] = nut[i];
			}else {
				if(alreadyNut == false) alreadyNut = true;
				out[i] = 1;
			}
		}
		
		return out;	
	}
	
	/**
	 * Set parameter data
	 */
	private void setParameter() {

		shadeData = new double[Integer.parseInt(SegNumb.getText())];
		shadeData = createRange(Integer.parseInt(SegNumb.getText()), 0, 0);
		LUTData = new String[Integer.parseInt(SegNumb.getText())];
		airTempData = new double[Integer.parseInt(SegNumb.getText())];
		nutData = new double[Integer.parseInt(SegNumb.getText())];
		changeMineN();

		if (ManualLUT.isSelected() && !SequenceLUT.getText().equals("")) {
			String[] seq = SequenceLUT.getText().split(",");
			if (Integer.parseInt(SegNumb.getText()) != seq.length) {
				System.out.println("Segment number does not fit to LUT Array!");
			}
				
			for (int i = 0; i < seq.length; i++) {
				if(seq[i].equals("A")) {
					shadeData[i] = setShade(ShadetypA.getText());
					LUTData[i]="A";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypA.getText())); 
					nutData[i] = Double.parseDouble(ANut.getText());
				}
				if(seq[i].equals("B")) {
					shadeData[i] = setShade(ShadetypB.getText());
					LUTData[i]="B";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypB.getText())); 
					nutData[i] = Double.parseDouble(BNut.getText());
				}
				if(seq[i].equals("B*")) {
					shadeData[i] = setShade(ShadetypBStar.getText());
					LUTData[i]="B*";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypBStar.getText())); 
					nutData[i] = Double.parseDouble(BStarNut.getText());
				}
				if(seq[i].equals("C")) {
					shadeData[i] = setShade(ShadetypC.getText());
					LUTData[i]="C";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypC.getText())); 
					nutData[i] = Double.parseDouble(CNut.getText());
				}
				if(seq[i].equals("D")) {
					shadeData[i] = setShade(ShadetypD.getText());
					LUTData[i]="D";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypD.getText())); 
					nutData[i] = Double.parseDouble(DNut.getText());
				}
				if(seq[i].equals("E")) {
					shadeData[i] = setShade(ShadetypE.getText());
					LUTData[i]="E";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypE.getText())); 
					nutData[i] = Double.parseDouble(ENut.getText());
				}
				if(seq[i].equals("F")) {
					shadeData[i] = setShade(ShadetypF.getText());
					LUTData[i]="F";
					airTempData[i] = Double.parseDouble(setAirtemp(ShadetypF.getText())); 
					nutData[i] = Double.parseDouble(FNut.getText());
				}
				
			}		
			
		}
		else {
		
			String[] data1;
	
			if (!ASeg.getText().equals("")) {
				data1 = ASeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypA.getText());
							LUTData[j]="A";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypA.getText())); 
							nutData[j] = Double.parseDouble(ANut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypA.getText());
						LUTData[Integer.parseInt(d[0])]="A";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypA.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(ANut.getText());
					}
				}
			}
	
			if (!BSeg.getText().equals("")) {
				data1 = BSeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypB.getText());
							LUTData[j]="B";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypB.getText())); 
							nutData[j] = Double.parseDouble(BNut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypB.getText());
						LUTData[Integer.parseInt(d[0])]="B";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypB.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(BNut.getText());
					}
				}
			}
	
			
			if (!BStarSeg.getText().equals("")) {
				data1 = BStarSeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypBStar.getText());
							LUTData[j]="B*";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypBStar.getText())); 
							nutData[j] = Double.parseDouble(BStarNut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypBStar.getText());
						LUTData[Integer.parseInt(d[0])]="B*";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypBStar.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(BStarNut.getText());
					}
				}
			}
			
			
			if (!CSeg.getText().equals("")) {
				data1 = CSeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypC.getText());
							LUTData[j]="C";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypC.getText())); 
							nutData[j] = Double.parseDouble(CNut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypC.getText());
						LUTData[Integer.parseInt(d[0])]="C";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypC.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(CNut.getText());
					}
				}
			}
	
			if (!DSeg.getText().equals("")) {
				data1 = DSeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypD.getText());
							LUTData[j]="D";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypD.getText())); 
							nutData[j] = Double.parseDouble(DNut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypD.getText());
						LUTData[Integer.parseInt(d[0])]="D";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypD.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(DNut.getText());
					}
				}
			}
	
			if (!ESeg.getText().equals("")) {
				data1 = ESeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypE.getText());
							LUTData[j]="E";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypE.getText())); 
							nutData[j] = Double.parseDouble(ENut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypE.getText());
						LUTData[Integer.parseInt(d[0])]="E";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypE.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(ENut.getText());
					}
				}
			}
	
			if (!FSeg.getText().equals("")) {
				data1 = FSeg.getText().split(",");
				for (int i = 0; i < data1.length; i++) {
					String[] d = data1[i].split("-");
					if (d.length != 1) {
						for (int j = Integer.parseInt(d[0]); j <= Integer.parseInt(d[1]); j++) {
							shadeData[j] = setShade(ShadetypF.getText());
							LUTData[j]="F";
							airTempData[j] = Double.parseDouble(setAirtemp(ShadetypF.getText())); 
							nutData[j] = Double.parseDouble(FNut.getText());
						}
					} else {
						shadeData[Integer.parseInt(d[0])] = setShade(ShadetypF.getText());
						LUTData[Integer.parseInt(d[0])]="F";
						airTempData[Integer.parseInt(d[0])] = Double.parseDouble(setAirtemp(ShadetypF.getText())); 
						nutData[Integer.parseInt(d[0])] = Double.parseDouble(FNut.getText());
					}
				}
			}
		}


		double[] epsi = createRange(Integer.parseInt(SegNumb.getText()), Double.parseDouble(EpsilonMin.getText()),
				Double.parseDouble(EpsilonMax.getText()));	
		if (IntegerT.isSelected()) {
			for(int i = 0; i < epsiData.length; i++) {
				epsiData[i] = Math.round(epsi[i]);
			}
		} else {
			epsiData = epsi;
		}
		

		double[] mu = createRange(Integer.parseInt(SegNumb.getText()), Double.parseDouble(MuMin.getText()),
				Double.parseDouble(MuMax.getText()));
		if (IntegerT.isSelected()) {
			for(int i = 0; i < muData.length; i++) {
				muData[i] = Math.round(mu[i]);
			}
		} else {
			muData = mu;
		}


		double[] del = createRange(Integer.parseInt(SegNumb.getText()), Double.parseDouble(delta.getText()),
				Double.parseDouble(delta.getText()));
		deltaData = del;
		
		
		if (constNut.isSelected()) { //constant 1.0 or gradient
			nutData =  createRange(Integer.parseInt(SegNumb.getText()),Double.parseDouble(minNut.getText()),1.0);
		}
		else { //LUT specific
			double[] nutD = setNutrients2(nutData);
			nutData =  nutD;
		
		}	
		
		double[] beta1 = createRange(Integer.parseInt(SegNumb.getText()), Double.parseDouble(beta1Min.getText()),
				Double.parseDouble(beta1Max.getText()));
		beta1Data = beta1;
		
		
	}

	/**
	 * Save general simulation parameters in file (not segment specific)
	 * @param f file name
	 */
	private void saveParameter(String f) {
		final String filename = f;

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file
			bw.write("SimTime," + SimTime.getText());
			bw.newLine();

			bw.write("startMonth," + StartMonth.getText());
			bw.newLine();

			bw.write("Segments," + SegNumb.getText());
			bw.newLine();
			
			bw.write("beta0," + beta0.getText());
			bw.newLine();
			
			bw.write("b," + bRate.getText());
			bw.newLine();
			
			bw.write("B0," + B0.getText());
			bw.newLine();
			
			bw.write("B1," + B1.getText());
			bw.newLine();
			
			bw.write("pFilamentousMax," + pfMax.getText());
			bw.newLine();
			
			bw.write("pFilamentousThres," + betaF.getText());
			bw.newLine();
			
			if(stochLength.isSelected()) {
				bw.write("stochLength,true");
			}else {
				bw.write("stochLength,false");
			}
			bw.newLine();
			
			if(DepOnLight.isSelected()) {
				bw.write("DepOnLight,true");
			}else {
				bw.write("DepOnLight,false");
			}
			bw.newLine();		
			
			bw.write("scaleMortality," + scmortality.getText());
			bw.newLine();
			
			if(ddM.isSelected()) {
				bw.write("ddMortality,true");
			}else {
				bw.write("ddMortality,false");
			}
			bw.newLine();
			
			bw.write("mortalityChiro," + mortC.getText());
			bw.newLine();
			
			bw.write("mortalityMayfly," + mortM.getText());
			bw.newLine();
			
			bw.write("mortalityLimpet," + mortL.getText());
			bw.newLine();
			
			bw.write("mortalityEggsFila,"+ mortEggFila.getText());
			bw.newLine();
			
			//Dispersal Mayfly
			bw.write("probstayM," + probstayM.getText());
			bw.newLine();
			bw.write("probupM," + probupM.getText());
			bw.newLine();
			bw.write("probdownM," + probdownM.getText());
			bw.newLine();
			bw.write("distmeanupM," + distmeanupM.getText());
			bw.newLine();
			bw.write("distmeandownM," + distmeandownM.getText());
			bw.newLine();
			
			//Dispersal Chiro
			bw.write("probstayC," + probstayC.getText());
			bw.newLine();
			bw.write("probupC," + probupC.getText());
			bw.newLine();
			bw.write("probdownC," + probdownC.getText());
			bw.newLine();
			bw.write("distmeanupC," + distmeanupC.getText());
			bw.newLine();
			bw.write("distmeandownC," + distmeandownC.getText());
			bw.newLine();
			
			if(ovidyn.isSelected() | ovisum.isSelected() | ovihom.isSelected()) {
				if(ovidyn.isSelected() | ovihom.isSelected()) bw.write("oviposition,dynamic");
				if(ovisum.isSelected()) bw.write("oviposition,sum");
				bw.newLine();
				
				bw.write("offspringL," + offspringL.getText());
				bw.newLine();
				
				bw.write("offspringM," + offspringM.getText());
				bw.newLine();
				
				bw.write("offspringC," + offspringC.getText());
				bw.newLine();
			}else {
				if (ovicon.isSelected()) bw.write("oviposition,constant");
				else System.out.println("No dispersal/oviposition method choosen?!");
				bw.newLine();
				
				bw.write("offspringL," + offspringLAbs.getText());
				bw.newLine();
				
				bw.write("offspringM," + offspringMAbs.getText());
				bw.newLine();
				
				bw.write("offspringC," + offspringCAbs.getText());
				bw.newLine();
				
			}
			
			
			bw.write("scaleOff," + scOff.getText());
			bw.newLine();
			
			bw.write("grazing," + grazingMethode.getValue());
			bw.newLine();
			
			bw.write("grazingFila," + grazingFila.getValue());
			bw.newLine();
			
			bw.write("compL," + compL.getText());
			bw.newLine();
			
			bw.write("compM," + compM.getText());
			bw.newLine();
			
			bw.write("compC," + compC.getText());
			bw.newLine();
			
			if(GrowWinter.isSelected()) {
				bw.write("GrowWinter,true");
			}else {
				bw.write("GrowWinter,false");
			}
			bw.newLine();
			
			bw.write("tempRise," +tempRiseT.getText());
			bw.newLine();
			
			bw.write("weeksOfHW," + heatWavesWeeks.getText());
			bw.newLine();
			
			if(constTemp.isSelected()) {
				bw.write("constTemp,true" );
			}else {
				bw.write("constTemp,false");
			}
			bw.newLine();
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Save segment specific parameter in file
	 * @param f filename
	 */
	private void saveParameterSeg(String f) {
		final String filename = f;

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file			
			bw.write("Landusetype,");
			for (int i = 0; i < LUTData.length - 1; i++) {
				bw.write(LUTData[i] + ",");
			}
			bw.write(LUTData[LUTData.length - 1]);
			bw.newLine();

			bw.write("Shade,");
			for (int i = 0; i < shadeData.length - 1; i++) {
				bw.write(shadeData[i]  + ",");
			}
			bw.write(Double.toString(shadeData[shadeData.length - 1] ));
			bw.newLine();
			
			bw.write("AirTemperature,");
			for (int i = 0; i < airTempData.length - 1; i++) {
				bw.write(airTempData[i]  + ",");
			}
			bw.write(Double.toString(airTempData[airTempData.length - 1] ));
			bw.newLine();
			
			
			bw.write("epsilon,");
			for (int i = 0; i < epsiData.length - 1; i++) {
				if (IntegerT.isSelected()) {
					bw.write(Math.round(epsiData[i]) / 100.0 + ",");
				} else {
					bw.write(epsiData[i] / 100.0 + ",");
				}
			}
			bw.write(Double.toString(epsiData[epsiData.length - 1] / 100.0));
			bw.newLine();

			bw.write("mu,");
			for (int i = 0; i < muData.length - 1; i++) {
				if (IntegerT.isSelected()) {
					bw.write(Math.round(muData[i]) / 100.0 + ",");
				} else {
					bw.write(muData[i] / 100.0 + ",");
				}
			}
			bw.write(Double.toString(muData[muData.length - 1] / 100.0));
			bw.newLine();

			bw.write("delta,");
			for (int i = 0; i < deltaData.length - 1; i++) {
				bw.write(deltaData[i] + ",");
			}
			bw.write(Double.toString(deltaData[deltaData.length - 1]));
			bw.newLine();
			
			bw.write("nut,");
			for (int i = 0; i < nutData.length - 1; i++) {
				bw.write(nutData[i] + ",");
			}
			bw.write(Double.toString(nutData[nutData.length - 1]));
			bw.newLine();
			
			
			bw.write("beta1,");
			for (int i = 0; i < beta1Data.length - 1; i++) {
				bw.write(beta1Data[i] + ",");
			}
			bw.write(Double.toString(beta1Data[beta1Data.length - 1]));
			bw.newLine();
			


		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Save choosen result variables which show be printed out after simulation 
	 * @param f filename
	 */
	private void saveOutput(String f) {
		final String filename = f;

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file
			bw.write("OutAir," + OutAir.isSelected());		
			bw.newLine();
			bw.write("OutWater," + OutWater.isSelected());		
			bw.newLine();
			bw.write("OutBiofilm," + OutBiofilm.isSelected());		
			bw.newLine();
			bw.write("OutCon," + OutCon.isSelected());		
			bw.newLine();
			bw.write("OutCon2," + OutCon2.isSelected());	
			bw.newLine();
			bw.write("OutPartEu," + OutPartEu.isSelected());		
			bw.newLine();
			bw.write("OutpF," + OutpF.isSelected());		
			bw.newLine();
			
			bw.write("OutAbun," + OutAbun.isSelected());		
			bw.newLine();
			bw.write("OutDemand," + OutDemand.isSelected());		
			bw.newLine();
			bw.write("OutFeed," + OutFeed.isSelected());		
			bw.newLine();
			bw.write("OutInfOutflow," + OutInfOutflow.isSelected());		
			bw.newLine();
			bw.write("OutDisLoss," + OutDisLoss.isSelected());
			bw.newLine();
			
			bw.write("OutHerbi," + OutHerbi.isSelected());		
			bw.newLine();
			bw.write("OutInsecti," + OutInsecti.isSelected());		
			bw.newLine();
			bw.write("OutHerbiE," + OutHerbiE.isSelected());		
			bw.newLine();
			bw.write("OutInsectiE," + OutInsectiE.isSelected());		
			bw.newLine();
			bw.write("OutAbunBio," + OutAbunBio.isSelected());		
			bw.newLine();
			
			bw.write("ProofCoexist," + ProofCoexist.isSelected());
			bw.newLine();
			bw.write("ProofRange," + ProofRange.isSelected());
			bw.newLine();
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Set initialization parameter biofilm biomass and abundances of species for each segment
	 */
	private void setSpecies() {
		double[] species1;
		double[] species2;
		double[] species3;
		double[] species4;
		double[] species5;
		double[] species6;
		double[] species7;
		double[] species8;

		String[] data1 = Binit.getText().split(":");
		species1 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data1[0]), Double.parseDouble(data1[1]));
		String[] data2 = Limpet1.getText().split(":");
		species2 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data2[0]), Double.parseDouble(data2[1]));
		String[] data3 = Limpet2.getText().split(":");
		species3 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data3[0]), Double.parseDouble(data3[1]));
		String[] data4 = Mayfly.getText().split(":");
		species4 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data4[0]), Double.parseDouble(data4[1]));
		String[] data5 = Chironomid.getText().split(":");
		species5 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data5[0]), Double.parseDouble(data5[1]));
		String[] data6 = Stonefly1.getText().split(":");
		species6 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data6[0]), Double.parseDouble(data6[1]));
		String[] data7 = Stonefly2.getText().split(":");
		species7 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data7[0]), Double.parseDouble(data7[1]));
		String[] data8 = Stonefly3.getText().split(":");
		species8 = createRangeS(Integer.parseInt(SegNumb.getText()), Integer.parseInt(Till.getText()),
				Double.parseDouble(data8[0]), Double.parseDouble(data8[1]));

		if (upstreamDirection.isSelected()) {
			for (int i = 0; i < Integer.parseInt(SegNumb.getText()); i++) {
				BinitData[i] = species1[i];
				Limpet1Data[i] = (int) Math.round(species2[i]);
				Limpet2Data[i] = (int) Math.round(species3[i]);
				MayflyData[i] = (int) Math.round(species4[i]);
				ChironomidData[i] = (int) Math.round(species5[i]);
				Stonefly1Data[i] = (int) Math.round(species6[i]);
				Stonefly2Data[i] = (int) Math.round(species7[i]);
				Stonefly3Data[i] = (int) Math.round(species8[i]);
			}

		} else {

			for (int i = Integer.parseInt(SegNumb.getText()) - 1; i >= 0; i--) {
				BinitData[i] = species1[i];
				Limpet1Data[i] = (int) Math.round(species2[i]);
				Limpet2Data[i] = (int) Math.round(species3[i]);
				MayflyData[i] = (int) Math.round(species4[i]);
				ChironomidData[i] = (int) Math.round(species5[i]);
				Stonefly1Data[i] = (int) Math.round(species6[i]);
				Stonefly2Data[i] = (int) Math.round(species7[i]);
				Stonefly3Data[i] = (int) Math.round(species8[i]);
			}
		}
	}

	/**
	 * Save biofilm biomass and abundance of species
	 * @param f filename
	 */
	private void saveSpecies(String f) {
		final String filename = f;

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file
			bw.write("#Binit, Limpet1, Limpet2, Mayfly, Chironomids, Stonefly1, Stonefly2, Stonefly3");
			bw.newLine();

			if (upstreamDirection.isSelected()) {
				for (int i = 0; i < Integer.parseInt(SegNumb.getText()); i++) {
					bw.write(BinitData[i] + "," + Limpet1Data[i] + "," + Limpet2Data[i] + "," + MayflyData[i] + ","
							+ ChironomidData[i] + "," + Stonefly1Data[i] + "," + Stonefly2Data[i] + ","
							+ Stonefly3Data[i]);
					bw.newLine();
				}

			} else {

				for (int i = Integer.parseInt(SegNumb.getText()) - 1; i >= 0; i--) {
					bw.write(BinitData[i] + "," + Limpet1Data[i] + "," + Limpet2Data[i] + "," + MayflyData[i] + ","
							+ ChironomidData[i] + "," + Stonefly1Data[i] + "," + Stonefly2Data[i] + ","
							+ Stonefly3Data[i]);
					bw.newLine();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Set constant pesticides (herbicide/insecticide)
	 */
	private void setStressorsCon() {
		herbiData = new double[Integer.parseInt(SegNumb.getText())];
		insectiData = new double[Integer.parseInt(SegNumb.getText())];
		double[] StressorH = new double[Integer.parseInt(SegNumb.getText())];
		double[] StressorI = new double[Integer.parseInt(SegNumb.getText())];
		Arrays.fill(StressorH, 0.0);
		Arrays.fill(StressorI, 0.0);
		

		double diffStressH;
		double diffStressI;
		double driftingRate = 0.5;
		
		
		for(int j = 0; j < Integer.parseInt(SegNumb.getText()); j++) {
			//search for agricultural landscape
			if(LUTData[j].equals("E")) {
				int seg = j; //Segment of diffusive source
				diffStressH = Double.parseDouble(HerbicideConDiff.getText())*0.5;
				diffStressI = Double.parseDouble(InsecticideConDiff.getText())*0.5;
				double newPesti;
				
				for (int w = seg; w < Integer.parseInt(SegNumb.getText()); w++) {
					//herbicide
					newPesti = diffStressH * Math.exp(-driftingRate * (w - seg));
					//StressorH[w] = (double) Math.round((1.0 - (1.0 - StressorH[w]) * (1.0 - newPesti))*100d)/100d;
					StressorH[w] = (double) StressorH[w] + newPesti;	
					//insecticide
					newPesti = diffStressI * Math.exp(-driftingRate * (w - seg));
					//StressorI[w] = (double) Math.round((1.0 - (1.0 - StressorI[w]) * (1.0 - newPesti))*100d)/100d;
					StressorI[w] = (double) StressorI[w] + newPesti;	
					
				}			
			}
			else if(LUTData[j].equals("F")) {
				int seg = j; //Segment of diffusive source
				diffStressH = Double.parseDouble(HerbicideConDiff.getText());
				diffStressI = Double.parseDouble(InsecticideConDiff.getText());
				double newPesti;
				
				for (int w = seg; w < Integer.parseInt(SegNumb.getText()); w++) {
					//herbicide
					newPesti = diffStressH * Math.exp(-driftingRate * (w - seg));
					//StressorH[w] = (double) Math.round((1.0 - (1.0 - StressorH[w]) * (1.0 - newPesti))*100d)/100d;	
					StressorH[w] = (double) StressorH[w] + newPesti;
					//insecticide
					newPesti = diffStressI * Math.exp(-driftingRate * (w - seg));
					//StressorI[w] = (double) Math.round((1.0 - (1.0 - StressorI[w]) * (1.0 - newPesti))*100d)/100d;	
					StressorI[w] = (double) StressorI[w] + newPesti;
					
				}	
			}
			
			//search for point sources
			else if(LUTData[j].equals("D")) {
				int seg = j; //Segment of diffusive source
				diffStressH = Double.parseDouble(HerbicideConPoint.getText());
				diffStressI = Double.parseDouble(InsecticideConPoint.getText());
				double newPesti;
				
				for (int w = seg; w < Integer.parseInt(SegNumb.getText()); w++) {
					//herbicide
					newPesti = diffStressH * Math.exp(-driftingRate * (w - seg));
					//StressorH[w] = (double) Math.round((1.0 - (1.0 - StressorH[w]) * (1.0 - newPesti))*100d)/100d;
					StressorH[w] = (double) StressorH[w] + newPesti;
					//insecticide
					newPesti = diffStressI * Math.exp(-driftingRate * (w - seg));
					//StressorI[w] = (double) Math.round((1.0 - (1.0 - StressorI[w]) * (1.0 - newPesti))*100d)/100d;	
					StressorI[w] = (double) StressorI[w] + newPesti;
				}	

			}
			
		}
		herbiData = StressorH;
		insectiData = StressorI;


	}
	

	/**
	 * Save constant pesticides (herbicides/insecticides)
	 * @param f1 filename for herbicide
	 * @param f2 filename for insecticide
	 */
	private void saveStressorsCon(String f1, String f2) {
		final String filename = f1;

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int i = 0; i < Integer.parseInt(SegNumb.getText()); i++) {
				for (int j = 0; j < Integer.parseInt(SimTime.getText()); j++) {
					if (j != 0)
						bw.write(",");
					bw.write(herbiData[i] + "");
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		final String filename1 = f2;

		// BufferedWriter bw = null;
		// FileWriter fw = null;

		try {
			fw = new FileWriter(filename1);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int i = 0; i < Integer.parseInt(SegNumb.getText()); i++) {
				for (int j = 0; j < Integer.parseInt(SimTime.getText()); j++) {
					if (j != 0)
						bw.write(",");
					bw.write(insectiData[i] + "");
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Save events of pesticide input
	 * @param filename10 filename
	 */
	public void saveStressorEvent(String filename10) {
		//String filename10 = SimulationMain.pathString + SimulationMain.pathParam + "Stressor_Events"+ Scenario.getText() + ".csv";
		//System.out.println(filename10);
		
		try {
			fw10 = new FileWriter(filename10);
			bw10 = new BufferedWriter(fw10);

			// Write into file

			//bw10.write("#t,#seg,#pesticide,#intensity,#k_in_e^kc");
			//bw10.newLine();

			for(int i = 0;i < myList.size(); i++) {
				bw10.write(myList.get(i));
				bw10.newLine();
			}		
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw10 != null)
					bw10.close();
				if (fw10 != null)
					fw10.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	@FXML
	public void onButtonClickAdd() {

//		String filename10 = SimulationMain.pathString + SimulationMain.pathParam + "Stressor_Events_"+ Scenario.getText() + ".csv";
//		
//		
//		try {
//			fw10 = new FileWriter(filename10, true);
//			bw10 = new BufferedWriter(fw10);
//
//			// Write into file
//
//			bw10.write(StressorEvent.getText());
//			bw10.newLine();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (bw10 != null)
//					bw10.close();
//				if (fw10 != null)
//					fw10.close();
//			} catch (IOException ex) {
//				ex.printStackTrace();
//			}
//		}

		myList.add(StressorEvent.getText());
		ListStress.setItems(myList);

		StressorEvent.clear();

	}

	@FXML
	public void changeTill() {
		Till.setText(SegNumb.getText());
	}

	/**
	 * Set nutrient factor to limited nutrient for land use type A,B and C
	 */
	@FXML
	public void changeMineN() {
		ANut.setText(minNut.getText());
		BNut.setText(minNut.getText());
		CNut.setText(minNut.getText());
	}
	
	/**
	 * Change probability for downstream flight if upstream flight changes
	 */
	@FXML
	public void changeprobdown() {
		double newprobM = ((double)Math.round((1.0 - Double.parseDouble(probupM.getText()))*100))/100; 
		probdownM.setText(""+newprobM);
		
		double newprobC = ((double)Math.round((1.0 - Double.parseDouble(probupC.getText()))*100))/100;
		probdownC.setText(String.valueOf(newprobC));
		//probdownC.setText(String.valueOf(1.0-Double.parseDouble(probupC.getText())));
		
	}
	
	/**
	 * Change offspring equally by a factor scOff 
	 */
	@FXML 
	public void changeOffSpring() {
		double newLoff = Double.parseDouble(offspringL.getText())* Double.parseDouble(scOff.getText());
		offspringL.setText(String.valueOf(newLoff));
		double newMoff = Double.parseDouble(offspringM.getText())* Double.parseDouble(scOff.getText());
		offspringM.setText(String.valueOf(newMoff));
		double newCoff = Double.parseDouble(offspringC.getText())* Double.parseDouble(scOff.getText());
		offspringC.setText(String.valueOf(newCoff));
	}

	/**
	 * Change oviposition to dynamic(locally dependent)  and species specific dispersal
	 */
	@FXML
	public void changeOviToDyn() {
		ovicon.setSelected(false);
		ovihom.setSelected(false);
		ovisum.setSelected(false);
		LabelOffAbs.setDisable(true);
		LabelOff.setDisable(false);
		offspringL.setDisable(false);
		offspringM.setDisable(false);
		offspringC.setDisable(false);
		offspringLAbs.setDisable(true);
		offspringMAbs.setDisable(true);
		offspringCAbs.setDisable(true);
		probstayM.setDisable(false);
		probstayC.setDisable(false);
		probupM.setDisable(false);
		probupC.setDisable(false);
		distmeanupM.setDisable(false);
		distmeanupC.setDisable(false);
		distmeandownM.setDisable(false);
		distmeandownC.setDisable(false);
		probstayM.setText(String.valueOf(0.04));
		probstayC.setText(String.valueOf(0.15));
		probupM.setText(String.valueOf(0.9));
		probupC.setText(String.valueOf(0.6));
		distmeanupM.setText(String.valueOf(17));
		distmeanupC.setText(String.valueOf(3));
		distmeandownM.setText(String.valueOf(2));
		distmeandownC.setText(String.valueOf(2));
		changeprobdown();
	}
	
	/**
	 * Change oviposition to constant (constant input of eggs)
	 */
	@FXML
	public void changeOviToCon() {
		ovidyn.setSelected(false);
		ovihom.setSelected(false);
		ovisum.setSelected(false);
		LabelOff.setDisable(true);
		LabelOffAbs.setDisable(false);
		offspringL.setDisable(true);
		offspringM.setDisable(true);
		offspringC.setDisable(true);
		offspringLAbs.setDisable(false);
		offspringMAbs.setDisable(false);
		offspringCAbs.setDisable(false);	
		probstayM.setDisable(true);
		probstayC.setDisable(true);
		probupM.setDisable(true);
		probupC.setDisable(true);
		distmeanupM.setDisable(true);
		distmeanupC.setDisable(true);
		distmeandownM.setDisable(true);
		distmeandownC.setDisable(true);
	}
	
	/**
	 * Change oviposition to dynamic(locally dependent) and equal dispersal between species
	 */
	@FXML
	public void changeToHom() {
		ovidyn.setSelected(false);
		ovicon.setSelected(false);
		ovisum.setSelected(false);
		LabelOffAbs.setDisable(true);
		LabelOff.setDisable(false);
		offspringL.setDisable(false);
		offspringM.setDisable(false);
		offspringC.setDisable(false);
		offspringLAbs.setDisable(true);
		offspringMAbs.setDisable(true);
		offspringCAbs.setDisable(true);	
		probstayM.setDisable(false);
		probstayC.setDisable(false);
		probupM.setDisable(false);
		probupC.setDisable(false);
		distmeanupM.setDisable(false);
		distmeanupC.setDisable(false);
		distmeandownM.setDisable(false);
		distmeandownC.setDisable(false);
		probstayM.setText(String.valueOf(0.15));
		probstayC.setText(String.valueOf(0.15));
		probupM.setText(String.valueOf(0.5));
		probupC.setText(String.valueOf(0.5));
		distmeanupM.setText(String.valueOf(50));
		distmeanupC.setText(String.valueOf(50));
		distmeandownM.setText(String.valueOf(50));
		distmeandownC.setText(String.valueOf(50));
		changeprobdown();
	}
	
	/**
	 * Set oviposition to sum (global oviposition: sums up eggs and equally distribute them independet of location)
	 */
	@FXML
	public void changeToSum() {
		ovidyn.setSelected(false);
		ovihom.setSelected(false);
		ovicon.setSelected(false);
		LabelOffAbs.setDisable(true);
		LabelOff.setDisable(false);
		offspringL.setDisable(false);
		offspringM.setDisable(false);
		offspringC.setDisable(false);
		offspringLAbs.setDisable(true);
		offspringMAbs.setDisable(true);
		offspringCAbs.setDisable(true);	
		probstayM.setDisable(true);
		probstayC.setDisable(true);
		probupM.setDisable(true);
		probupC.setDisable(true);
		distmeanupM.setDisable(true);
		distmeanupC.setDisable(true);
		distmeandownM.setDisable(true);
		distmeandownC.setDisable(true);
	}
	
	/**
	 * Set initial abundance to zero
	 */
	@FXML
	public void changeToNoGrazers() {
		Limpet1.setText("0:0");
		Limpet2.setText("0:0");
		Mayfly.setText("0:0");
		Chironomid.setText("0:0");
		withGrazers.setSelected(false);
	}
	
	/** 
	 * Set initial abundances to non-zero
	 */
	@FXML
	public void changeToGrazers() {
		Limpet1.setText("60000:60000");
		Limpet2.setText("0:0");
		Mayfly.setText("30000:30000");
		Chironomid.setText("600000:600000");
		withoutGrazers.setSelected(false);
	}
	
	
	/**
	 * Run method for varying parameters 
	 */
	@FXML
	public void onButtonClickvaryPar() {
		java.util.Date date = new java.util.Date();
		System.out.println("Start:" + date);
		
		barM.setProgress(0.0);
		LabelRunningM.setText("Scenarios Run");
		
		//Select parameter
//		int[] varyL = {40,41,42,43,44,45,46,47,48,49,50}; //{40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55}; //{45}; //Parameter to vary
//		int[] varyM = {70,80,90,100,110,120,130,140,150,160,170,180,190,200}; // {50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200}; //{100}; 
//		int[] varyC = {Main.varPar1};//{16};//{6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};//{8};
//		double[] varyLmort = {0.03,0.04,0.05};
//		double[] varyMmort = {0.07,0.072,0.074,0.076,0.078,0.08,0.082,0.084,0.086,0.088,0.09};//{0.07,0.071,0.072,0.073,0.074};
//		double[] varyCmort = {Main.varPar2};//{0.26,0.27,0.28,0.29,0.3}; //0.27,0.28,0.29 //0.26, 0.3
//		double[] varyWinter = {0.3,0.31,0.32};
		double[] varypfMax = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0};//{0.0,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1};//{0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0};
		//double[] varybetaF = {30000}; //{1800,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,200000};
		boolean[] varyGrazer = {true, false};
		boolean[] varyeL = {false}; //{true, false};
		String scenarioName = ScenarioM.getText();
		EnterPath.setText(EnterPathM.getText());
		
		//System.out.println("varParam:" + varyC[0] + " " + varyCmort[0]);
		
		//SegNumb.setText("211");
		//SimTime.setText("480"); //40 Jahre
		//ManualLUT.setSelected(true);
		
//		for(int v1 = 0; v1 < varyL.length; v1++) {
//			for(int v2 = 0; v2 < varyM.length; v2++) {
//				for(int v3 = 0; v3 < varyC.length; v3++) {
//		for(int v4 = 0; v4 < varyLmort.length; v4++) {
//			for(int v5 = 0; v5 < varyMmort.length; v5++) {
//				for(int v6 = 0; v6 < varyCmort.length;v6++) {
//					for(int v7 = 0; v7 < varyWinter.length;v7++) {	
		for(int v1 = 0; v1 < varypfMax.length; v1++) {
			//for(int v2 = 0; v2 < varybetaF.length; v2++) {
			for(int v2 = 0; v2 < varyeL.length; v2++) {
				for(int v3 = 0; v3 < varyGrazer.length; v3++) {
					//Change parameter & adapt scenario
					//Scenario.setText(scenarioName + "_" + varyL[v1] + "_" + varyM[v2] + "_" + varyC[v3]+ "_"  + varyLmort[v4] + "_" + varyMmort[v5]+ "_" + varyCmort[v6] + "_" + varyWinter[v7]);
					if (varyeL[v2] == true) {
						if(varyGrazer[v3]==true) {
							Scenario.setText(scenarioName + "eL" + "_" + varypfMax[v1]);
							withGrazers.setSelected(true);
							changeToGrazers();
						}else {
							Scenario.setText(scenarioName + "eL" + "_wG" + "_" + varypfMax[v1]);
							withoutGrazers.setSelected(true);
							changeToNoGrazers();
						}
					}else {
						if(varyGrazer[v3]==true) {
							Scenario.setText(scenarioName + "_" + varypfMax[v1]);
							withGrazers.setSelected(true);
							changeToGrazers();
							
						}else {
							Scenario.setText(scenarioName + "_wG" + "_" + varypfMax[v1]);
							withoutGrazers.setSelected(true);
							changeToNoGrazers();
						}
					}
					
//					offspringL.setText(String.valueOf(varyL[v1]));
//					offspringM.setText(String.valueOf(varyM[v2]));
//					offspringC.setText(String.valueOf(varyC[v3]));
//					mortL.setText(String.valueOf(varyLmort[v4]));
//					mortM.setText(String.valueOf(varyMmort[v5]));
//					mortC.setText(String.valueOf(varyCmort[v6]));
//					scmortality.setText(String.valueOf(varyWinter[v7]));
					pfMax.setText(String.valueOf(varypfMax[v1]));
					//betaF.setText(String.valueOf(varybetaF[v2]));
					DepOnLight.setSelected(varyeL[v2]);
					
					
					
					//Save Parameter files
					onButtonClickInitialize();
				
//					int s = (v1+1)*(v2+1)*(v3+1)*(v4+1)*(v5+1)*(v6+1)*(v7+1);
					int s = (v1+1)*(v2+1);
//					double ssum = varyL.length *varyM.length *varyC.length*varyLmort.length *varyMmort.length *varyCmort.length*varyWinter.length;
					double ssum = varypfMax.length *varyeL.length;
					LabelRunningM.setText("Scenario " + s + " initialized");
				
					// Start simulation
					onButtonClickRun();
					LabelRunningM.setText("Scenario " + s + " finished");
					barM.setProgress(((double) s) / ssum);
				
					}
				}
			}
		
//			}
//		}}}}
		//LabelRunningM.setText("Finished simulation of scenarios!");
		java.util.Date date1 = new java.util.Date();
		System.out.println("End:" + date1);
		if(SimulationMain.autoRan) {Platform.exit();}
	}
	
	
	
}
