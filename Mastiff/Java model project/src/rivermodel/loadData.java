//*Class is loading data for rivermodel*

package rivermodel;

/**
 * @author Laura Meier
 * @version 05.07.21
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Pattern;
import java.io.File;

public class loadData {

	// *Methods*

	/**
	 * Load variables out of a file
	 * 
	 * @param file String with file name that consist parameter values
	 */
	public static void loadVariables(String file) {
		String csvFile = file;
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);

				// save parameters
				switch (data[0]) {
				case "SimTime":
					SimulationMain.simTime = Integer.parseInt(data[1]);
					break;
				case "Segments":
					SimulationMain.amountSegments = Integer.parseInt(data[1]);
					break;
				case "AirTemperature":					
					SimulationMain.airTemperatureMean = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.airTemperatureMean[t - 1] = Double.parseDouble(data[t]);
					}
					break;
				case "Shade":
					SimulationMain.shadeArray = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.shadeArray[t - 1] = Double.parseDouble(data[t]);
					}
					break;
				case "pFilamentousMax":
					SimulationMain.pFMax = Double.parseDouble(data[1]);
					break;
				case "pFilamentousThres":
					SimulationMain.betaF = Double.parseDouble(data[1]);
					break;
				case "stochLength":
					SimulationMain.stochLength = Boolean.parseBoolean(data[1]);
					break;
				case "DepOnLight":
					SimulationMain.DepOnLight = Boolean.parseBoolean(data[1]);
					break;					
				case "scaleMortality":
					SimulationMain.scMort = Double.parseDouble(data[1]);
					break;
				case "ddMortality":
					SimulationMain.ddM = Boolean.parseBoolean(data[1]);
					break;	
				case "mortalityChiro":
					//SimulationMain.mortalityRateC = Double.parseDouble(data[1]);
					Chironomid.setMort(Double.parseDouble(data[1]));
					break; 
				case "mortalityMayfly":
					//SimulationMain.mortalityRateM = Double.parseDouble(data[1]);
					Mayfly.setMort(Double.parseDouble(data[1]));
					break; 	
				case "mortalityLimpet":
					//SimulationMain.mortalityRateL = Double.parseDouble(data[1]);
					Limpet.setMort(Double.parseDouble(data[1]));
					break; 
				case "mortalityEggsFila":
					SimulationMain.mortalityEggsFila = Double.parseDouble(data[1]);
					break;				
				case "grazing":
					SimulationMain.grazingMethode = Double.parseDouble(data[1]);
					break;	
				case "grazingFila":
					SimulationMain.grazingFila = Double.parseDouble(data[1]);
					break;	
				case "compL":
					Limpet.compS = Double.parseDouble(data[1]);
					break;	
				case "compM":
					Mayfly.compS = Double.parseDouble(data[1]);
					break;	
				case "compC":
					Chironomid.compS = Double.parseDouble(data[1]);
					break;	
				case "GrowWinter":
					SimulationMain.growWinter = Boolean.parseBoolean(data[1]);
					break;		
				case "probstayM":
					Mayfly.setProbStay(Double.parseDouble(data[1]));
					break;
				case "probupM":
					Mayfly.setProbUp(Double.parseDouble(data[1]));
					break;	
				case "probdownM":
					Mayfly.setProbDown(Double.parseDouble(data[1]));
					break;	
				case "distmeanupM":
					Mayfly.setDistUp(Double.parseDouble(data[1]));
					break;
				case "distmeandownM":
					Mayfly.setDistDown(Double.parseDouble(data[1]));
					break;	
				case "probstayC":
					Chironomid.setProbStay(Double.parseDouble(data[1]));
					break;
				case "probupC":
					Chironomid.setProbUp(Double.parseDouble(data[1]));
					break;	
				case "probdownC":
					Chironomid.setProbDown(Double.parseDouble(data[1]));
					break;	
				case "distmeanupC":
					Chironomid.setDistUp(Double.parseDouble(data[1]));
					break;
				case "distmeandownC":
					Chironomid.setDistDown(Double.parseDouble(data[1]));
					break;	
				case "oviposition":
					SimulationMain.oviposition =  data[1];
					break;
				case "offspringL":
					Limpet.setOffspring(Double.parseDouble(data[1]));
					break;
				case "offspringM":
					Mayfly.setOffspring(Double.parseDouble(data[1]));
					break;
				case "offspringC":
					Chironomid.setOffspring(Double.parseDouble(data[1]));
					break;
				case "epsilon":
					SimulationMain.epsilonArray = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.epsilonArray[t - 1] = Double.parseDouble(data[t]);
					}
					break;
				case "mu":
					SimulationMain.muArray = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.muArray[t - 1] = Double.parseDouble(data[t]);
					}
					break;
				case "delta":
					SimulationMain.deltaArray = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.deltaArray[t - 1] = Double.parseDouble(data[t]);
					}
					break;
				case "nut":
					SimulationMain.nutArray = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.nutArray[t - 1] = Double.parseDouble(data[t]);
					}
					break;
				case "b":
					SimulationMain.b = Double.parseDouble(data[1]);
					break;
				case "B0":
					SimulationMain.B0 = Double.parseDouble(data[1]);
					break;
				case "B1":
					SimulationMain.B1 = Double.parseDouble(data[1]);
					break;
				case "beta0":
					SimulationMain.beta0 = Double.parseDouble(data[1]);
					break;	
				case "beta1":
					SimulationMain.beta1Array = new double[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.beta1Array[t - 1] = Double.parseDouble(data[t]);
					}
					break;				
				case "startMonth":
					SimulationMain.startMonth = Integer.parseInt(data[1]);
					break;
				case "Landusetype":
					SimulationMain.LUTArray = new String[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.LUTArray[t - 1] = data[t];
					}
					break;
				case "tempRise":
					SimulationMain.tempRise =  Double.parseDouble(data[1]);
					break;
				case "weeksOfHW":
					SimulationMain.weeksOfHWArray = new int[data.length - 1];
					for (int t = 1; t < data.length; t++) {
						SimulationMain.weeksOfHWArray[t - 1] = Integer.parseInt(data[t]);
					}
					break;
				case "constTemp":
					SimulationMain.tempConst = Boolean.parseBoolean(data[1]);
					break;

				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: Data file not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: Read file process!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception: Close file process!");
				}
			}
		}
	}

	/**
	 * Load initial species distribution
	 * 
	 * @param file String with file name that consist distribution of species
	 */
	public static void loadInitialSpecies(String file) {
		String csvFile = file;
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			int i = 0;
			while ((line = br.readLine()) != null) {
				if (Pattern.matches("#.*", line)) {
					continue;
				}
				String[] data = line.split(csvSplitBy);

				// save initial distribution per segment in matrix by converting string to int
				SimulationMain.InitialSpecies[i][0] = Double.parseDouble(data[0]); // inital Biofilm biomass 
				SimulationMain.InitialSpecies[i][1] = Double.parseDouble(data[1]); // Limpet
				SimulationMain.InitialSpecies[i][2] = Double.parseDouble(data[2]); // Limpet A/2
				SimulationMain.InitialSpecies[i][3] = Double.parseDouble(data[3]); // Mayfly
				SimulationMain.InitialSpecies[i][4] = Double.parseDouble(data[4]); // Chironomid
				SimulationMain.InitialSpecies[i][5] = Double.parseDouble(data[5]); // Stonefly
				SimulationMain.InitialSpecies[i][6] = Double.parseDouble(data[6]); // Stonefly 2
				SimulationMain.InitialSpecies[i][7] = Double.parseDouble(data[7]); // Stonefly 3

				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: Data file not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: Read file process!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception: Close file process!");
				}
			}
		}
	}

	/**
	 * Load stressors (pesticide)
	 * 
	 * @param file String with file name that consist concentration of pesticide
	 */
	public static double[][] loadStressors(String file) {
		String csvFile = file;
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		double[][] Stressors = new double[SimulationMain.amountSegments][SimulationMain.simTime + 1];

		try {
			br = new BufferedReader(new FileReader(csvFile));
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);

				// save initial distribution per segment in matrix by converting string to int
				for (int j = 0; j < data.length; j++) {
					Stressors[i][j] = Double.parseDouble(data[j]);
				}
				i++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: Data file not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: Read file process!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception: Close file process!");
				}
			}
		}

		return Stressors;

	}

	/**
	 * Load event stressors (pesticide)
	 * 
	 * @param file String with file name that consist parameters of pesticide event
	 */
	public static void loadStressorEvents(String file) {
		String csvFile = file;
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";
		
		String[][] StressorEvents = new String[filelength(file)-1][5];

		try {
			br = new BufferedReader(new FileReader(csvFile));
			int i = 0;
			while ((line = br.readLine()) != null) {
				if(Pattern.matches("#.*", line)) {continue;}
				String[] data = line.split(csvSplitBy);
				
				StressorEvents[i][0] = data[0]; //t - month
				StressorEvents[i][1] = data[1]; //segment
				StressorEvents[i][2] = data[2]; //pesticide
				StressorEvents[i][3] = data[3]; //intensity
				StressorEvents[i][4] = data[4]; //drift to other segments
				
				i++;
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: Data file not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: Read file process!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception: Close file process!");
				}
			}
		}
		
		
		//Set to zero (initialization)
		for(int time = 0; time < SimulationMain.simTime; time++) {
			for(int seg = 0; seg < SimulationMain.amountSegments; seg++) {
				SimulationMain.Stressor_Event_Insect[seg][time] = 0.0;
				SimulationMain.Stressor_Event_Herbi [seg][time] = 0.0;
			}
		}
		
		//Sorting
		for(int j = 0; j < StressorEvents.length; j++) {
			if(StressorEvents[j][1].equals("B*") || StressorEvents[j][1].equals("D") || StressorEvents[j][1].equals("E") || StressorEvents[j][1].equals("F")) { //LUT Stressor
				//Search for all segments with specific LUT
				String typS = StressorEvents[j][1];
				
				for(int k = 0; k < SimulationMain.LUTArray.length;k++) {
					if(SimulationMain.LUTArray[k].equals(typS)) {
						driftOfStressor(StressorEvents[j][2],Integer.parseInt(StressorEvents[j][0]), k , Double.parseDouble(StressorEvents[j][3]), Double.parseDouble(StressorEvents[j][4]));	
					}
				}
			}
			else { //Single Stressor
				driftOfStressor(StressorEvents[j][2],Integer.parseInt(StressorEvents[j][0]), Integer.parseInt(StressorEvents[j][1]), Double.parseDouble(StressorEvents[j][3]), Double.parseDouble(StressorEvents[j][4]));	
			}
		}
			
	}

	
	/**
	 * Fill up Event Stressors in SimulationMain
	 * @param type herbicide or insecticide
	 * @param t time step
	 * @param seg segment where the pesticide is spilled
	 * @param intialIntensity in first segment
	 * @param driftingRate k in e function
	 */
	private static void driftOfStressor(String type, int month, int seg, double intialIntensity, double driftingRate) {
		int t = 4*(month-1);
		if (type.equals("insecticide")) {
			for (int w = seg; w < SimulationMain.amountSegments; w++) {
				double newPesti = intialIntensity * Math.exp(-driftingRate * (w - seg));
				//SimulationMain.Stressor_Event_Insect[w][t] = (double) Math.round((1.0 - (1.0 - SimulationMain.Stressor_Event_Insect[w][t]) * (1.0 - newPesti))*100d)/100d;
				SimulationMain.Stressor_Event_Insect[w][t] = (double) SimulationMain.Stressor_Event_Insect[w][t] + newPesti;
				//System.out.println(SimulationMain.Stressor_Event_Insect[w][t]);
			}
		} else if (type.equals("herbicide")) {
			for (int w = seg; w < SimulationMain.amountSegments; w++) {
				double newPesti = intialIntensity * Math.exp(-driftingRate * (w - seg));
				//SimulationMain.Stressor_Event_Herbi[w][t] = (double) Math.round((1.0 - (1.0 - SimulationMain.Stressor_Event_Herbi[w][t]) * (1.0 - newPesti))*100d)/100d;
				SimulationMain.Stressor_Event_Herbi[w][t] = (double) SimulationMain.Stressor_Event_Herbi[w][t]+ newPesti;
			}
		} else System.out.println("Wired type of pesticide (driftOfStressor)");
	}

	
	/**
	 * Load the inflow for each month out of a file with different species
	 * 
	 * @param file name of the file
	 */
	public static void loadInflow(String file) {
		String csvFile = file;
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			int i = 0;
			while ((line = br.readLine()) != null) {
				if (Pattern.matches("#.*", line)) {
					continue;
				}
				String[] data = line.split(csvSplitBy);

				// save inflowing individuals in matrix by converting string to int per month i
				SimulationMain.artInflow[i][0] = Integer.parseInt(data[0]); // Limpet
				SimulationMain.artInflow[i][1] = Integer.parseInt(data[1]); // Limpet 2
				SimulationMain.artInflow[i][2] = Integer.parseInt(data[2]); // Mayfly
				SimulationMain.artInflow[i][3] = Integer.parseInt(data[3]); // Chironomid
				SimulationMain.artInflow[i][4] = Integer.parseInt(data[4]); // Stonefly
				SimulationMain.artInflow[i][5] = Integer.parseInt(data[5]); // Stonefly 2
				SimulationMain.artInflow[i][6] = Integer.parseInt(data[6]); // Stonefly 3

				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: Data file not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: Read file process!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception: Close file process!");
				}
			}
		}

	}

	/**
	 * Count lines in file
	 * 
	 * @param filename name of file
	 * @return count of lines
	 */
	public static int filelength(String filename) {
		try {
			File file = new File(filename);
			if (file.exists()) {
				FileReader fr = new FileReader(file);
				LineNumberReader lnr = new LineNumberReader(fr);

				int linenumber = 0;

				while (lnr.readLine() != null) {
					linenumber++;
				}

				lnr.close();

				return linenumber;
			} else {
				System.out.println("Could not count lines!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 1;
	}
	
	/**
	 * Load output parameters out of a file
	 * 
	 * @param file String with file name that consist output file parameters
	 */
	public static void loadOutFilesNames(String file) {
		String csvFile = file;
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				
				// save parameters
				switch (data[0]) {
				case "OutAir":
					SimulationMain.OutAir = Boolean.parseBoolean(data[1]);
					break;	
				case "OutWater":
					SimulationMain.OutWater = Boolean.parseBoolean(data[1]);
					break;		
				case "OutBiofilm":
					SimulationMain.OutBiofilm = Boolean.parseBoolean(data[1]);
					break;	
				case "OutCon":
					SimulationMain.OutCon = Boolean.parseBoolean(data[1]);
					break;	
				case "OutCon2":
					SimulationMain.OutCon2 = Boolean.parseBoolean(data[1]);
					break;
				case "OutPartEu":
					SimulationMain.OutPartEu = Boolean.parseBoolean(data[1]);
					break;	
				case "OutpF":
					SimulationMain.OutpF = Boolean.parseBoolean(data[1]);
					break;	
					
				case "OutAbun":
					SimulationMain.OutAbun = Boolean.parseBoolean(data[1]);
					break;	
				case "OutDemand":
					SimulationMain.OutDemand = Boolean.parseBoolean(data[1]);
					break;	
				case "OutFeed":
					SimulationMain.OutFeed = Boolean.parseBoolean(data[1]);
					break;	
				case "OutInfOutflow":
					SimulationMain.OutInfOutflow = Boolean.parseBoolean(data[1]);
					break;	
				case "OutDisLoss":
					SimulationMain.OutDisLoss = Boolean.parseBoolean(data[1]);
					break;	
					
				case "OutHerbi":
					SimulationMain.OutHerbi = Boolean.parseBoolean(data[1]);
					break;	
				case "OutInsecti":
					SimulationMain.OutInsecti = Boolean.parseBoolean(data[1]);
					break;	
				case "OutHerbiE":
					SimulationMain.OutHerbiE = Boolean.parseBoolean(data[1]);
					break;	
				case "OutInsectiE":
					SimulationMain.OutInsectiE = Boolean.parseBoolean(data[1]);
					break;	
				case "OutAbunBio":
					SimulationMain.OutAbunBio = Boolean.parseBoolean(data[1]);
					break;	
				case "ProofCoexist":
					SimulationMain.ProofCoexist = Boolean.parseBoolean(data[1]);
					break;
				case "ProofRange":
					SimulationMain.ProofRange = Boolean.parseBoolean(data[1]);
					break;
					
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: Data file not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: Read file process!");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception: Close file process!");
				}
			}
		}
	}
	
	

}
