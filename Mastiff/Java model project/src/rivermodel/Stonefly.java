/*
 * Species based on stonefly life cycle 
 */
package rivermodel;


/**
 *
 * @author Laura Meier
 * @version 05.07.21
 */

public class Stonefly {

	public static int codeSpecies = 4;
	public static int ovipositionM = 5;
	public static int hatchingM = 8;
	public static int adultM = 5;
	public static int deathM = 6; // maximal life span (from oviposition till death)
	public static int offspring = 1200;
	public static double mortalityRate = 0.05;
	public static double quality = 2;
	public static double[] bodymass = new double[3];
	public static double IGmax = 0.35;
	public static double Tref = 16.0;
	public static double sensititiy = 1;
	public static double probstay = 0.14;
	public static double probup = 0.81;
	public static double probdown = 1- probup;
	public static double distmeanup = 2;
	public static double distmeandown = 1;
	public static double[] tempThres = {18.0,-0.055,1.09}; //threshold,m,b <-----------not researched yet!! Need literature research!
	
	/**
	 * Set bodymass - growth
	 * @param year 1st,2nd or 3rd year of stonefly 
	 * @return new bodymass
	 */
	public static double setBodymass(int year) {
		double pronotumWidth; // cm
		if (year == 0) {
			switch ((SimulationMain.week + 48 - (hatchingM*4)) %48)  {
			case 0: // hatching (September)
			case 1: // hatching (September)
			case 2: // hatching (September)
			case 3: // hatching (September)
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 23: pronotumWidth = 0.1;  break; // February
			case 24: pronotumWidth = 0.1; break;
			case 25: pronotumWidth = 0.105; break;
			case 26: pronotumWidth = 0.110; break;
			case 27: pronotumWidth = 0.115; break;
			case 28: pronotumWidth = 0.12; break;
			case 29: pronotumWidth = 0.125; break;
			case 30: pronotumWidth = 0.13; break;
			case 31: pronotumWidth = 0.135; break;
			case 32: pronotumWidth = 0.14; break;
			case 33: pronotumWidth = 0.145; break;
			case 34: pronotumWidth = 0.15; break;
			case 35: pronotumWidth = 0.155; break;
			case 36: pronotumWidth = 0.16; break;
			case 37: pronotumWidth = 0.165; break;
			case 38: pronotumWidth = 0.17; break;
			case 39: pronotumWidth = 0.175; break;
			case 40: pronotumWidth = 0.18; break;
			case 41: pronotumWidth = 0.185; break;
			case 42: pronotumWidth = 0.19; break;
			case 43: pronotumWidth = 0.195; break;
			case 44: // August
			case 45: 
			case 46: 
			case 47: pronotumWidth = 0.2; break;
			default: pronotumWidth = 0.2; break;
			}
			bodymass[0] = Math.exp(-1.57 + 3.12 * Math.log(10.0 * pronotumWidth));
			return bodymass[0];
		} else if (year == 1) {
			switch ((SimulationMain.week + 48 - (hatchingM*4)) %48)  {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 23: pronotumWidth = 0.2; break; // February				
			case 24: pronotumWidth = 0.2; break;
			case 25: pronotumWidth = 0.205; break;
			case 26: pronotumWidth = 0.21; break;
			case 27: pronotumWidth = 0.215; break;
			case 28: pronotumWidth = 0.22; break;
			case 29: pronotumWidth = 0.225; break;
			case 30: pronotumWidth = 0.23; break;
			case 31: pronotumWidth = 0.235; break;
			case 32: pronotumWidth = 0.24; break;
			case 33: pronotumWidth = 0.245; break;
			case 34: pronotumWidth = 0.25; break;
			case 35: pronotumWidth = 0.255; break;
			case 36: pronotumWidth = 0.26; break;
			case 37: pronotumWidth = 0.265; break;
			case 38: pronotumWidth = 0.27; break;
			case 39: pronotumWidth = 0.275; break;
			case 40: pronotumWidth = 0.28; break;
			case 41: pronotumWidth = 0.285; break;
			case 42: pronotumWidth = 0.29; break;
			case 43: pronotumWidth = 0.295; break;
			case 44: pronotumWidth = 0.3; break; // August
			case 45: pronotumWidth = 0.3; break;
			case 46: pronotumWidth = 0.3; break;
			case 47: pronotumWidth = 0.3; break;
			default: pronotumWidth = 0.3; break;
			}

			bodymass[1] = Math.exp(-1.57 + 3.12 * Math.log(10.0 * pronotumWidth));

			return bodymass[1];
		} else {
			switch ((SimulationMain.week + 48 - (hatchingM*4)) %48)  {
			case 0:  
			case 1:
			case 2:
			case 3:
			case 4: 
			case 5:
			case 6:
			case 7:
			case 8: 
			case 9:
			case 10:
			case 11:
			case 12: 
			case 13:
			case 14:
			case 15: 
			case 16: 
			case 17:
			case 18:
			case 19:
			case 20: // February
			case 21:
			case 22:
			case 23: pronotumWidth = 0.3; break; 
			case 24: pronotumWidth = 0.3; break;
			case 25: pronotumWidth = 0.31; break;
			case 26: pronotumWidth = 0.32; break;
			case 27: pronotumWidth = 0.33; break;
			case 28: pronotumWidth = 0.34; break;
			case 29: pronotumWidth = 0.35; break;
			case 30: pronotumWidth = 0.36; break;
			case 31: pronotumWidth = 0.37; break;
			case 32: pronotumWidth = 0.38; break; // May
			case 33: pronotumWidth = 0.39; break;
			case 34: pronotumWidth = 0.40; break;
			case 35: pronotumWidth = 0.41; break;
			case 36: pronotumWidth = 0.42; break;
			case 37: pronotumWidth = 0.43; break;
			case 38: pronotumWidth = 0.44; break;
			case 39: pronotumWidth = 0.45; break;
			case 40: pronotumWidth = 0.46; break;
			case 41:
			case 42:
			case 43:
			case 44:
			default: pronotumWidth = 0.46; break;
			}

			bodymass[2] = Math.exp(-1.57 + 3.12 * Math.log(10.0 * pronotumWidth));

			return bodymass[2];
		}
	}

	
	/**
	 * Exponential decreasing disperal distance (upstream)
	 * @param x segment
	 * @return probability to start in segment x to fly upstream and land in current segment
	 */
	private static double fup(double x) {
		return 1/distmeanup*Math.exp(x/distmeanup);
	}
	
	
	/**
	 * Exponential decreasing disperal distance (downstream)
	 * @param x segment
	 * @return probability to start in segment x to fly downstream and land in current segment
	 */
	private static double fdown(double x) {
		return 1/distmeandown*Math.exp(-x/distmeandown);
	}
	
	
	/**
	 * Species distribution kernel for disperal of imagines
	 * @param Seg start segment of imagines
	 * @return probability to land in current segment
	 */
	public static double d(double Seg) {
		if(Seg > 0) return (1-probstay)*probdown*fdown(Seg);
		else if(Seg < 0) return (1-probstay)*probup*fup(Seg);
		else return probstay; //k == i --> Seg = 0
	}

}
