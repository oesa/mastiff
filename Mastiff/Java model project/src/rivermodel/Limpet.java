/*
 * Species based on limpet life cycle 
 */
package rivermodel;

/**
 *
 * @author Laura Meier
 * @version 05.07.21
 */
public class Limpet {

	public static int codeSpecies = 1;
	public static int ovipositionM = 4;  
	public static int hatchingM = 6;    
	public static int adultM = 4;        
	public static int deathM = 6;//7;
	public static double offspring;// = 50;
	public static double mortalityRate;// = 0.04;
	public static double quality = 2;
	public static double[] bodymass = new double[2];
	public static double IGmax = 3.27; //3.44;
	public static double Tref = 15.5;
	public static double sensitivity = 0.0;
	public static double compS; 		
	public static double delta = 1.0;
	public static double[] tempThres = {18.0,-0.055,1.09}; //threshold,m,b

	
	/**
	 * Set bodymass - growth
	 * @param year 1st or 2nd year limpet
	 * @return new bodymass
	 */
    public static double setBodymass(int year) {
    	double bodylength;
    	if(year == 0) {
    		switch ((SimulationMain.week + 48 - (hatchingM*4)) %48) {
    			case 0:  bodylength = 1.0; break;
    			case 1:  bodylength = 1.025; break;
    			case 2:  bodylength = 1.05; break;
    			case 3:  bodylength = 1.075; break;
    			case 4:  bodylength = 1.1; break;
    			case 5:  bodylength = 1.25; break;
    			case 6:  bodylength = 1.4; break;
    			case 7:  bodylength = 1.55; break;
    			case 8:  bodylength = 1.7; break;
    			case 9:  bodylength = 1.775; break;
    			case 10:  bodylength = 1.85; break;
    			case 11:  bodylength = 1.925; break;
    			case 12:  bodylength = 2.0; break;
    			case 13:  bodylength = 2.05; break;
    			case 14:  bodylength = 2.1; break;
    			case 15:  bodylength = 2.15; break;
    			case 16:  bodylength = 2.2; break;
    			case 17:  bodylength = 2.275; break;
    			case 18:  bodylength = 2.350; break;
    			case 19:  bodylength = 2.425; break;
    			case 20:  bodylength = 2.5; break;
    			case 21:  bodylength = 2.525; break;
    			case 22:  bodylength = 2.55; break;
    			case 23:  bodylength = 2.575; break;
    			case 24:  bodylength = 2.6; break;
    			case 25:  bodylength = 2.6; break;
    			case 26:  bodylength = 2.6; break;
    			case 27:  bodylength = 2.6; break;
    			case 28:  bodylength = 2.6; break;
    			case 29:  bodylength = 2.675; break;
    			case 30:  bodylength = 2.75; break;
    			case 31:  bodylength = 2.825; break;
    			case 32:  bodylength = 2.9; break;
    			case 33:  bodylength = 2.925; break;
    			case 34:  bodylength = 2.95; break;
    			case 35:  bodylength = 2.975; break;
    			case 36:  bodylength = 3.0; break;
    			case 37:  bodylength = 3.125; break;
    			case 38:  bodylength = 3.25; break;
    			case 39:  bodylength = 3.375; break;
    			case 40: bodylength = 3.5; break;
    			case 41: bodylength = 3.875; break;
    			case 42: bodylength = 4.25; break;
    			case 43: bodylength = 4.625; break;
    			case 44: bodylength = 5.0; break;
    			case 45: bodylength = 5.125; break;
    			case 46: bodylength = 5.25; break;
    			case 47: bodylength = 5.375; break;			
    			default: bodylength = 6.0; break;
    		}
    		bodymass[0] = Math.pow(10.0, (3.0*Math.log10(10.0*bodylength)-3.762))*(1.0/10.0); //formula by Calow 1975 	
        	return bodymass[0];
    	}
    	else {
    		switch ((SimulationMain.week + 48 - (hatchingM*4)) %48) {
    			case 0: bodylength = 5.5; break; 
    			case 1: bodylength = 5.635; break; 
    			case 2: bodylength = 5.750; break; 
    			case 3: bodylength = 5.875; break; 
    			case 4: bodylength = 6.0; break;
    			default: bodylength = 6.0; break;
    		}
    		bodymass[1] = Math.pow(10.0, (3.0*Math.log10(10.0*bodylength)-3.762))*(1.0/10.0); //formula by Calow 1975 	
        	return bodymass[1];
    	}	
    	
    }

    /**
     * Set mortality rate
     * @param newMort mortality rate
     */
    public static void setMort(double newMort) {
		mortalityRate = newMort;
	}
    
    /**
     * Set offspring
     * @param off amount offspring
     */
    public static void setOffspring(double off) {
		offspring = off;
	}
    
}
