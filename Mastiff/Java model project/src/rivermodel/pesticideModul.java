/*
 * Pesticide Module
 * --> not finished yet
 */

package rivermodel;

/**
 * @author Laura Meier
 * @version 21.10.19
 */


public class pesticideModul {

	//have to be fitted
	private static double para1H = 14;
	private static double para2H = 2;
	private static double para1I = 3;
	private static double para2I = 0.5;
	
	/**
	 * Give back effect with given concentration
	 * @param species
	 * @param concentration
	 * @return effect
	 */
	public static double pesticideEffect(int species, String pesti, double concentration) {
		//hier ggf noch Wirkungskurven fuer andere Arten einfuegen
		if(pesti.equals("Insecticide"))	return effectCurve(para1H,para2H,concentration);
		else return effectCurve(para1I,para2I,concentration);
	}
	
	/**
	 * Cumulative distribution function
	 * @param con concentration of pesticide
	 * @return effect of pesticide
	 */
	private static double effectCurve(double p1, double p2, double con) {
		if (con == 0) {return 0;}
		else {
			return 1/(1+Math.exp(-(con-p1)/p2));
		}
	}
	
	
	
}
