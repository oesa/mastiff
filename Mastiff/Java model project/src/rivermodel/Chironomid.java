/*
 * Species based on chironomid life cycle
 */
package rivermodel;


/**
 * 
 * @author Laura Meier
 * @version 05.07.21
 */

public class Chironomid{
	
	public static int codeSpecies = 3;
	public static int ovipositionM;  
	public static int hatchingM;    
	public static int adultM;        
	public static int deathM;       
	public static double offspring; // = 25;
	public static double mortalityRate; // = 0.28;//0.24; //0.2; //increased without predator //0.24
	public static double quality = 1;
	public static double bodymass;
	public static double IGmax = 42.89;
	public static double Tref = 14.5;
	public static double sensitivity = 1;
	public static double compS; 
	public static double probstay; // = 0.15;
	public static double probup; // = 0.6;
	public static double probdown; // = 1- probup;
	public static double distmeanup; // = 3;
	public static double distmeandown; // = 2;
	public static double[] tempThres = {13.0,-0.005,1.065}; //threshold,m,b
	
	//Chironomid specific parameters
	static int[] ovipositionMC = {10,4,6,8}; //{2,4,6,8,10};
	static int[] adultMC = {4,6,8,10}; // {4,6,8,10,2};
	static int[] deathMC = {5,7,9,2}; //{5,7,9,11,3};
	static int hatchingT = 1;
	
	
	/**
	 * Set parameter adultT, maxLife, ovipositionM by bornTS
	 */
	public static void setParam() {
		int bornTS = setChironomidBornTs();
		
		if (bornTS%12 == ovipositionMC[0]) {
			hatchingM = (bornTS + 5) %12;
			adultM =  adultMC[0];
			deathM = deathMC[0];
			ovipositionM = ovipositionMC[1];
		} else
		if (bornTS%12 == ovipositionMC[1]) {
			hatchingM = (bornTS + hatchingT) %12;
			adultM =  adultMC[1];
			deathM = deathMC[1];
			ovipositionM = ovipositionMC[2];
		} else
		if (bornTS%12 == ovipositionMC[2]) {
			hatchingM = (bornTS + hatchingT) %12;
			adultM =  adultMC[2];
			deathM = deathMC[2];
			ovipositionM = ovipositionMC[3];
		} else
		if (bornTS%12 == ovipositionMC[3]) {
			hatchingM = (bornTS + hatchingT) %12;
			adultM =  adultMC[3];
			deathM = deathMC[3];
			ovipositionM = ovipositionMC[0];
		} else {
			System.out.println("Chironomid does not fit to regular oviposition dates!");
			System.out.println(SimulationMain.month + " "+ bornTS + " ");
			
		}
		
	}
	
	/**
	 * Look for bornts for chironomid cohort
	 * @return time step in which the chironomid is born
	 */
	public static int setChironomidBornTs() {
		int ovipositionlast = 0;
		if(SimulationMain.month %12 >= 4 && SimulationMain.month %12 < 6) {
			ovipositionlast = 4;
		}else if(SimulationMain.month %12 >= 6 && SimulationMain.month %12 < 8) {
			ovipositionlast = 6;
		}else if(SimulationMain.month %12 >= 8 && SimulationMain.month %12 < 10) {
			ovipositionlast = 8;
		}else if(SimulationMain.month %12 >= 10 || SimulationMain.month %12 < 4) {
			ovipositionlast = 10;
		}else System.out.println("Something went wrong setChironomidBornTS: "+ SimulationMain.month%12);
		
		return ovipositionlast;

	}
	
	/**
	 * Set bodymass - growth
	 * @return new bodymass
	 */
    public static double setBodymass() {
    	switch ((SimulationMain.week + 48 - (hatchingM*4)) %48) {
    		case -1:
    		case 0:  bodymass = 0.051; break; 
    		case 1:  bodymass = 0.22; break;
    		case 2:  bodymass = 0.39; break;
    		case 3:  bodymass = 0.56; break;
    		case 4:  bodymass = 0.74; break;
    		default: bodymass = 0.74;
    	}
    	
    	return bodymass;
    }
	
	
	/**
	 * Exponential decreasing disperal distance (upstream)
	 * @param x segment
	 * @return probability to start in segment x to fly upstream and land in current segment
	 */
	private static double fup(double x) {
		return 1/distmeanup*Math.exp(x/distmeanup);
	}
	
	/**
	 * Exponential decreasing disperal distance (downstream)
	 * @param x segment
	 * @return probability to start in segment x to fly downstream and land in current segment
	 */
	private static double fdown(double x) {
		return 1/distmeandown*Math.exp(-x/distmeandown);
	}
	
	/**
	 * Species distribution kernel for disperal of imagines
	 * @param Seg start segment of imagines
	 * @return probability to land in current segment
	 */
	public static double d(double Seg) {
		if(Seg > 0) return (1-probstay)*probdown*fdown(Seg);
		else if(Seg < 0) return (1-probstay)*probup*fup(Seg);
		else return probstay; //k == i --> Seg = 0
	}
    
	
	/**
	 * Set mortality rate
	 * @param newMort new mortality rate
	 */
	public static void setMort(double newMort) {
		mortalityRate = newMort;
	}
	
	/**
	 * Set probability to stay in the same segment
	 * @param v probability to stay
	 */
	public static void setProbStay(double v) {
		probstay = v;
	}
	
	/**
	 * Set probability to fly upstream
	 * @param v probability to fly upstream
	 */
	public static void setProbUp(double v) {
		probup = v;
	}
	
	/**
	 * Set probability to fly downstream
	 * @param v probability to fly downstream
	 */
	public static void setProbDown(double v) {
		probdown = v;
	}
	
	/**
	 * Set mean distance for upstream flight
	 * @param v mean distance
	 */
	public static void setDistUp(double v) {
		distmeanup = v;
	}
	
	/**
	 * Set mean distance for downstream dlight
	 * @param v mean distance
	 */
	public static void setDistDown(double v) {
		distmeandown = v;
	}
	
	/**
	 * Set offspring
	 * @param off amount offspring
	 */
    public static void setOffspring(double off) {
		offspring = off;
	}
	
}
