/*
 * River segment objects
 */
package rivermodel;

import org.apache.commons.math3.distribution.*;

/**
 *
 * @author Laura Meier
 * @version 05.07.21
 */
public class Segments {

	// *Instance Variables*
	int SegNumb; // Segment ID
	int outSeg; // lower reach segment
	String LUT;

	// Biofilms
	double BiofilmF; // Biofilm, which could be overgrown by filamentous algae or thick biofilm
	double BiofilmN; // Biofilm, which could only be overgrown by thick biofilm (no filamentous algae possible)
	double BiofilmM; // Mirco niche - could not be overgrown by filamentous algae or thick biofilm
	double B0; // Biofilm: part non-edible
	double[] BiofilmRip; //Biofilm biomass rips off
	
	double b; // Biofilm: growth rate
	double d; // Biofilm: mortality rate
	double K; // Capacity for biofilmF and biofilmN (spatial)
	double KM; // Capacity for micro niche biofilmM
	double feed; // Feed for grazers
	double feedF;
	double feedN;
	double feedM;
	int[] biofilmCon; // Biofilm condition in the segment - F, N, M
	int maxLifeCon; // Life span of filamentous biofilm
	int ageOfEF; // Average duration in months of filamentous eutrophicated biofilm
	double epsilon; // Percentage covered by filamentous algae
	double mu; // micro niche
	double beta0; // Threshold: Handable
	double beta1; // Threshold: Tearing off

	// Organisms
	int numSpecies; // Number of species in the segment
	Species Limpets; // Egg, Larvae, Adult
	Species Mayflys; // Egg, Larvae, Imagines
	Species Chironomids; // Egg, Larvae, Imagines
	Species Stoneflys; // Egg, Larvae first year, second year, third year, Imagines
	double delta;
	double I0 = 16705; //kJ/week

	// Environment
	double[] tempWater; // Temperature over one year (seasonal variation)
	double scaleL = 6.3889; // Scale factor light
	double PAR = 0.5;
	double shadeFactor; // ShadeFactor
	double nutrients; // Nutrients

	// Stressors
	double[] StressorInsect; // Stressor: Insecticide
	double[] StressorHerbi; // Stressor: Insecticide

	/**
	 * Constructor
	 * 
	 * @param Seg Segment identification number 
	 * @param outflowSeg segment in which the current segment flows
	 * @param LUT land use type of the segment
	 * @param tempW temperature of water in the segment
	 * @param shade Shade in the segment specified by LUT
	 * @param eps epsilon - proportion of segment which can be overgrown by filamentous algae
	 * @param m my - proportion of segment which could never develop thick biofilm or filamentous algae
	 * @param del drift mortality to enter the segment for immigrants
	 * @param InitialSpecies ArrayList of initial species amount
	 * @param StressorI insecticide concentration (diffusive)
	 * @param StressorH herbicide concentration (diffusive)
	 * @param StressorIE insecticide concentration (event)
	 * @param StressorHE herbicide concentration (event)
	 * 
	 */
	public Segments(int Seg, int outflowSeg, String LUT, double[] tempW, double shade, double eps, double m, double del, double nut, double beta1,
			double[] InitialSpecies, double[] StressorsI, double[] StressorsH, double[] StressorIE, double[] StressorHE) {
		this.SegNumb = Seg;
		this.outSeg = outflowSeg;
		this.LUT = LUT;

		// Set environmental parameters
		this.tempWater = tempW;
		this.shadeFactor = shade;
		this.nutrients = nut;

		// Set biofilm condition
		this.B0 = SimulationMain.B0;
		this.epsilon = eps;
		this.mu = m;
		this.BiofilmF = InitialSpecies[0] * (1 - mu) * epsilon;
		this.BiofilmN = InitialSpecies[0] * (1 - mu) * (1 - epsilon);
		this.BiofilmM = InitialSpecies[0] * mu;
		this.BiofilmRip = new double[] {0, 0, 0};

		this.b = SimulationMain.b;//0.71;//42;
		this.d = 0.01; //4 //0.01
		this.K = 402000; //in gramm/600m^2
		
		this.biofilmCon = new int[] { 0, 0, 0 };
		this.maxLifeCon = 3*4;
		this.ageOfEF = 0;
		this.delta = del;
		this.beta0 = SimulationMain.beta0; //300000;
		this.beta1 = beta1;//350000;
		this.KM = beta0*mu; //15000; // < mu*beta0
		
		
		// Set Species condition
		this.numSpecies = InitialSpecies.length - 1;
		createIndividuals(InitialSpecies,SegNumb);

		// Set Stressors
		setPesticides(StressorsH, StressorHE, StressorsI, StressorIE);
		

	}

	// *********************************************Setup initial Species*********************************************//

	/**
	 * Create initial distribution of each individual each species
	 * 
	 * @param amountSp Amount of each species which should be created
	 * @param SegN segment number
	 */
	private void createIndividuals(double[] amountSp, int SegN) {

		// no eggs/no imagines
		double[] l = new double[] { 0, amountSp[1], amountSp[2] };
		Limpets = new Species(1, l, false,SegN);
		double[] m = new double[] { 0, amountSp[3], 0 };
		Mayflys = new Species(2, m, true,SegN);
		double[] c = new double[] { 0, amountSp[4], 0 };
		Chironomids = new Species(3, c, true,SegN);
		double[] s = new double[] { 0, amountSp[5], amountSp[6], amountSp[7], 0 };
		Stoneflys = new Species(4, s, true,SegN);

	}
	
	/**
	 * Calculates total pesticide concentation (herbicide, insecticide) for each segment out of diffusive and event-based pesticide values
	 * 
	 * @param HerbiCons herbicide concentration (diffusive)
	 * @param HerbiEvent herbicide concentration (events)
	 * @param InsectCons insecticide concentration (diffusive)
	 * @param InsectEvent insecticide concentration (events)
	 */
	
	private void setPesticides(double[] HerbiCons, double[] HerbiEvent, double[] InsectCons, double[] InsectEvent) {
		this.StressorInsect = new double[SimulationMain.simTime];
		this.StressorHerbi = new double[SimulationMain.simTime];
		for(int i = 0; i < SimulationMain.simTime; i++) {
			//this.StressorHerbi[i] = (double) Math.round((1.0 - (1.0 - HerbiCons[i])*(1.0 - HerbiEvent[i]))*100d)/100d;
			//this.StressorInsect[i] = (double) Math.round((1.0 - (1.0 - InsectCons[i])*(1.0 - InsectEvent[i]))*100d)/100d;
			this.StressorHerbi[i] = (double) HerbiCons[i] + HerbiEvent[i];
			this.StressorInsect[i] = (double) InsectCons[i] + InsectEvent[i];
		}
		
	}
			

	// ******************************************Biofilm growth******************************************************//

	/**
	 * Calculates biofilm growth
	 */
	public void biofilmGrowth() {
		if (b != d & K != 0 & KM != 0 & Kreff(K) != 0 & epsilon != 0) {
			
			BiofilmF = 1/(Math.exp(-reff())*1/BiofilmF + (1-Math.exp(-reff()))*1/(Kreff(K)*epsilon));
			BiofilmN = 1/(Math.exp(-reff())*1/BiofilmN + (1-Math.exp(-reff()))*1/(Kreff(K)*(1 - epsilon)));
			BiofilmM = 1/(Math.exp(-reff())*1/BiofilmM + (1-Math.exp(-reff()))*1/(Kreff(KM)));
						
			if(BiofilmF + BiofilmN + BiofilmM < B0 && biofilmCon[0] != 2) {
				System.out.println("Month " + SimulationMain.month +", New - BiofilmTotal: " + (BiofilmF + BiofilmN + BiofilmM));
				System.out.println("reff() " + reff());
			}
				
			//could not fall under B0 (result of herbicides)
//			if(BiofilmF < B0 * (1 - mu) * epsilon) BiofilmF = B0 * (1 - mu) * epsilon;
//			if(BiofilmN < B0 * (1 - mu) * (1 - epsilon)) BiofilmN = B0 * (1 - mu) * (1 - epsilon);
//			if(BiofilmM < B0 * mu) BiofilmM = B0 * mu;		
			
		} else {
			BiofilmF = 0;
			System.out.println("Kreff: "+Kreff(K) + "KM:" + KM);
			System.out.println("Division by zero (biofilmGrowth())");
		}

	}

	/**
	 * Calculates the biofilm growth depending on light and nutrients
	 * 
	 * @return biofilm growth rate
	 */
	private double beff() {
		return b * eL() * eN() * (1 - Stress("Herbicide"));
	}

	/**
	 * Calculates the biofilm growth depending on light and nutrients
	 * 
	 * @return biofilm growth rate
	 */
	private double reff() {
		return beff() - d;
	}

	/**
	 * Calculated the capacity
	 * 
	 * @param capacity
	 * @return effective capacity
	 */
	private double Kreff(double capacity) {
		if(beff() == 0) {
			System.out.println("beff() == 0 (Kreff method)");
			return 0;
		}
		else return (1 - (d / beff())) * capacity;
	}

	// **************************************************Life cycle**************************************************//

	/**
	 * Development processes referring to the species specific life cycles
	 */
	public void development() {
		// hatching
		if(biofilmCon[0] == 2 && SimulationMain.grazingFila == 1) { //hatching for filamentous conditions
			//1: only have eggs in mirco-niche, 2:die in non micro niche, 3: normal hatching and partly transferred to abundanceFila
			Limpets.hatching2(1,mu);
			Mayflys.hatching2(2,mu);
			Chironomids.hatching2(3,mu);
		}else { //normal hatching
			Limpets.hatching();
			Mayflys.hatching();
			Chironomids.hatching();
			Stoneflys.hatching();
		}

		// grow up/adult/emergence
		Limpets.developAdult();
		Mayflys.developAdult();
		Chironomids.developAdult();
		Stoneflys.developAdult();

		// growth for semivoltine species (more than 1 larval state)
		Limpets.growth();
		Mayflys.growth();
		Chironomids.growth();
		Stoneflys.growth();

		// oviposition
		Limpets.oviposition(Limpets.abundance[Limpets.abundance.length - 1]);

	}

	/**
	 * Mortality (natural and insecticids and temperature)
	 * @param sc scale factor reduces mortality in winter (inactivity)
	 */
	public void mortalitySpecies(double sc) {
		// Mortality
		Limpets.mortality(Stress("Insecticide"), Limpet.sensitivity, sc, SimulationMain.ddM,60000/2,true);
		Mayflys.mortality(Stress("Insecticide"), Mayfly.sensitivity, sc,SimulationMain.ddM,30000/2,false);
		Chironomids.mortality(Stress("Insecticide"), Chironomid.sensitivity, sc,SimulationMain.ddM,600000/2,false);
		Stoneflys.mortality(Stress("Insecticide"), Stonefly.sensititiy, sc, true,6000/2,false);
		
		Limpets.mortalityTemp(getTemperature(SimulationMain.ts), true);
		Mayflys.mortalityTemp(getTemperature(SimulationMain.ts), false);
		Chironomids.mortalityTemp(getTemperature(SimulationMain.ts), false);
		Stoneflys.mortalityTemp(getTemperature(SimulationMain.ts), false);
				
	}
	
	/**
	 * Drift mortality takes place
	 */
	public void mortalityOutflow() {
		Limpets.mortalityDrift(Limpet.delta);
		Mayflys.mortalityDrift(delta);
		Chironomids.mortalityDrift(delta);
		Stoneflys.mortalityDrift(delta);
	}
	
	
	/**
	 * Places for each landed imagines the eggs in the recent segment
	 * 
	 * @param numImaginesLand imagines distribution of species
	 */
	public void placeEggs(double[] numImaginesLand) {
			Mayflys.oviposition(numImaginesLand[0]);
			Chironomids.oviposition(numImaginesLand[1]);
			Stoneflys.oviposition(numImaginesLand[2]);
	}

	
	
	// *************************************Grazing and Settlement****************************************************//

	/**
	 * Calculation of feed for grazers in the segment is based on environmental
	 * parameters and herbicids
	 */
	public void calFeed() {
		// reset feed
		feed = 0;

		// feed of biofilmF
		if (biofilmCon[0] == 0 && BiofilmF > SimulationMain.B1 * epsilon * (1 - mu)) {
			feedF = BiofilmF - SimulationMain.B1 * epsilon * (1 - mu);
		} else {
			feedF = 0; // filamentous or thick biofilm
		}
		// feed of biofilmN
		if (biofilmCon[1] == 0 && BiofilmN > SimulationMain.B1 * (1 - epsilon) * (1 - mu)) {
			feedN = BiofilmN - SimulationMain.B1 * (1 - epsilon) * (1 - mu);

		} else {
			feedN = 0; // thick biofilm
		}
		// feed of biofilmM
		if (BiofilmM > SimulationMain.B1 * mu) {
			feedM = BiofilmM - SimulationMain.B1 * mu;
		} else {
			feedM = 0;
		}

		feed = feedF + feedN + feedM;

	}


	
	/**
	 * Calculate the food demand of the giving amount of individuals for a species
	 * in a specific stage (in g)
	 * @param species functional type of organism
	 * @param abun abundance of species
	 * @param larvalage
	 * @return food demand of the giving individuals
	 */
	public double calFoodDemand2(int species, double abun, int larvalage) {
		double totalDemand = 0;
		if (species == 1)
			totalDemand = Limpet.IGmax  * eT2(Limpet.Tref) * eB2(Limpet.setBodymass(larvalage)) *0.001 * abun;
		if (species == 2)
			totalDemand = Mayfly.IGmax  * eT2(Mayfly.Tref) * eB2(Mayfly.setBodymass()) * 0.001 * abun;
		if (species == 3)
			totalDemand = Chironomid.IGmax  * eT2(Chironomid.Tref) * eB2(Chironomid.setBodymass()) * 0.001 * abun;
		if (species == 4)
			totalDemand = Stonefly.IGmax  * eT2(Stonefly.Tref) * eB2(Stonefly.setBodymass(larvalage)) * 0.001 * abun;
		return totalDemand;
	}
	
	

	/**
	 * Calculate food demand for one individual of a species and a larval stage (in g)
	 * 
	 * @param species functional type of organism
	 * @param larvalage larval stage for perennial species
	 * @return food demand of one individuals of species
	 */
	public double k_i2(int species, int larvalage) {
		double k = 0;
		if (species == 1)
			k = Limpet.IGmax * eT2(Limpet.Tref) * eB2(Limpet.setBodymass(larvalage)) * 0.001;
		if (species == 2)
			k = Mayfly.IGmax * eT2(Mayfly.Tref) * eB2(Mayfly.setBodymass()) * 0.001;
		if (species == 3)
			k = Chironomid.IGmax * eT2(Chironomid.Tref) * eB2(Chironomid.setBodymass()) * 0.001;
		if (species == 4)
			k = Stonefly.IGmax * eT2(Stonefly.Tref) * eB2(Stonefly.setBodymass(larvalage)) * 0.001;

		return k;
	}

		
	
	/**
	 * Body mass response function (in g)
	 * 
	 * @param M Function of body mass over time in mg
	 * @return body mass response
	 * 
	 */
	public double eB(double M) {
		double y = 0.75; //allometric scaling constant
		return Math.pow(M/1000, y);
	}
	
	
	/**
	 * Body mass response function (in mg)
	 * 
	 * @param M Function of body mass over time in mg
	 * @return body mass response
	 * 
	 */
	public double eB2(double M) {
		double y = 0.66666; //allometric scaling constant
		return Math.pow(M, y);
	}

	
	/**
	 * Calculate total demand of inhabitant and immigrating species
	 * 
	 * @return demand of inhabitants and total demand of inhabitants and immigrants
	 */
	public double[] askForDemand() {
		double demandL1 = calFoodDemand2(1, Limpets.abundance[1], 0);
		double demandL2 = calFoodDemand2(1, Limpets.abundance[2], 1);
		double demandM = calFoodDemand2(2, Mayflys.abundance[1], 0);
		double demandC = calFoodDemand2(3, Chironomids.abundance[1], 0);
		double demandInhabit = demandL1 + demandL2 + demandM + demandC;
		
		double demandL1In = calFoodDemand2(1, Limpets.immigration[0], 0);
		double demandL2In = calFoodDemand2(1, Limpets.immigration[1], 1);
		double demandMIn = calFoodDemand2(2, Mayflys.immigration[0], 0);
		double demandCIn = calFoodDemand2(3, Chironomids.immigration[0], 0);
		double demandImmi = demandL1In + demandL2In + demandMIn + demandCIn;
		
		double demandLEgg = 0;
		double demandMEgg = 0;
		double demandCEgg = 0;
		if(Limpets.canHatch()&& SimulationMain.week%4 == 0) {
			demandLEgg = calFoodDemand2(1, Limpets.abundance[1], 0); //fresh hatched
		}
		if(Mayflys.canHatch()&& SimulationMain.week%4 == 0) {
			demandMEgg = calFoodDemand2(2, Mayflys.abundance[1], 0);
		}
		if(Chironomids.canHatch()&& SimulationMain.week%4 == 0) {
			demandCEgg = calFoodDemand2(3, Chironomids.abundance[1], 0);
		}
		double demandEgg = demandLEgg + demandMEgg + demandCEgg;
		
		double demandTotal = demandInhabit + demandImmi;
		
		return new double[] {demandInhabit, demandTotal,demandEgg};
		
	}
	
	
	/**
	 * Calculate total demand of inhabitant and immigrating species - species specific solution
	 * 
	 * @return demand of inhabitants and total demand of inhabitants and immigrants
	 */
	public double[] askForDemandSpecies() {
		double demandL1 = calFoodDemand2(1, Limpets.abundance[1], 0);
		double demandL2 = calFoodDemand2(1, Limpets.abundance[2], 1);
		double demandL = demandL1 + demandL2;
		double demandM = calFoodDemand2(2, Mayflys.abundance[1], 0);
		double demandC = calFoodDemand2(3, Chironomids.abundance[1], 0);
		double demandS1 = calFoodDemand2(4, Stoneflys.abundance[1], 0);
		double demandS2 = calFoodDemand2(4, Stoneflys.abundance[2], 1);
		double demandS3 = calFoodDemand2(4, Stoneflys.abundance[3], 2);
		double demandS = demandS1 + demandS2 + demandS3;
		
		double demandL1In = calFoodDemand2(1, Limpets.immigration[0], 0);
		double demandL2In = calFoodDemand2(1, Limpets.immigration[1], 1);
		double demandLIn = demandL1In + demandL2In;
		double demandMIn = calFoodDemand2(2, Mayflys.immigration[0], 0);
		double demandCIn = calFoodDemand2(3, Chironomids.immigration[0], 0);
		double demandS1In = calFoodDemand2(4, Stoneflys.immigration[0], 0);
		double demandS2In = calFoodDemand2(4, Stoneflys.immigration[1], 1);
		double demandS3In = calFoodDemand2(4, Stoneflys.immigration[2], 2);
		double demandSIn = demandS1In + demandS2In + demandS3In;
		
		double demandLEgg = 0;
		double demandMEgg = 0;
		double demandCEgg = 0;
		double demandSEgg = 0;
		if(Limpets.canHatch() && SimulationMain.week%4 == 0) {
			demandLEgg = calFoodDemand2(1, Limpets.abundance[1], 0); //fresh hatched
		}
		if(Mayflys.canHatch() && SimulationMain.week%4 == 0) {
			demandMEgg = calFoodDemand2(2, Mayflys.abundance[1], 0);
		}
		if(Chironomids.canHatch() && SimulationMain.week%4 == 0) {
			demandCEgg = calFoodDemand2(3, Chironomids.abundance[1], 0);
		}
		if(Stoneflys.canHatch() && SimulationMain.week%4 == 0) {
			demandSEgg = calFoodDemand2(4, Stoneflys.immigration[0], 0);
		}
		
		
		//Inhabit, Inflow, Egg
		return new double[] {demandL, demandM, demandC, demandS, demandLIn, demandMIn, demandCIn, demandSIn, demandLEgg, demandMEgg, demandCEgg, demandSEgg};
		
	}
	
	
	/**
	 * Decides if (and how many) inhabitants can still live in the habitat or, when
	 * the capacity is not fully reached, who can settle from the inflowing
	 * individuals
	 */
	public void grazing() {
		
		if(biofilmCon[0] == 2 && SimulationMain.grazingFila == 1) {
			Chironomids.abundanceFila = Chironomids.abundanceFila + Chironomids.immigration[0];
			Chironomids.immigration[0] = 0;
		}
		
		if(SimulationMain.grazingMethode == 2) { //(B) grazing method --> inhabit and inflow treated equally 
			//Each species inflow are added to inhabitants
			Limpets.abundance[1] += Limpets.immigration[0];
			Limpets.abundance[2] += Limpets.immigration[1];
			Limpets.immigration[0] = 0;
			Limpets.immigration[1] = 0;
			Mayflys.abundance[1] += Mayflys.immigration[0];
			Mayflys.immigration[0] = 0;
			Chironomids.abundance[1] += Chironomids.immigration[0];
			Chironomids.immigration[0] = 0;	
		}
		
		// biofilm demand of all grazers
		double demandL1 = calFoodDemand2(1, Limpets.abundance[1], 0);
		double demandL2 = calFoodDemand2(1, Limpets.abundance[2], 1);
		double demandM = calFoodDemand2(2, Mayflys.abundance[1], 0);
		double demandC = calFoodDemand2(3, Chironomids.abundance[1], 0);
		double demandInhabit = demandL1 + demandL2 + demandM + demandC;

		double demandL1In = calFoodDemand2(1, Limpets.immigration[0], 0);
		double demandL2In = calFoodDemand2(1, Limpets.immigration[1], 1);
		double demandMIn = calFoodDemand2(2, Mayflys.immigration[0], 0);
		double demandCIn = calFoodDemand2(3, Chironomids.immigration[0], 0);
		double demandImmi = demandL1In + demandL2In + demandMIn + demandCIn;

		double demandTotal = demandInhabit + demandImmi;
		

		// Check if every grazer could stay
		if (demandTotal <= feed) {
			// Every grazer could stay (inhabitants & immigrants)
			Limpets.abundance[1] += Limpets.immigration[0];
			Limpets.abundance[2] += Limpets.immigration[1];
			Limpets.immigration[0] = 0;
			Limpets.immigration[1] = 0;
			Mayflys.abundance[1] += Mayflys.immigration[0];
			Mayflys.immigration[0] = 0;
			Chironomids.abundance[1] += Chironomids.immigration[0];
			Chironomids.immigration[0] = 0;

			// Reduction of biomass case 2
			reductionBiofilm(2, demandTotal);
		} else if (demandInhabit <= feed) {
			// Inhabitants can stay

			// Reduction of feed for immigrants
			feed -= demandInhabit;

			// Competition of immigrants
			// Calculate available feed for each species
			double feedForSpecies[] = new double[4];

			if ((demandL1In + demandL2In + demandMIn + demandCIn) != 0) {
				feedForSpecies[0] = ((Limpet.compS * demandL1In) / ((Limpet.compS * demandL1In) + (Limpet.compS * demandL2In) + (Mayfly.compS * demandMIn) + (Chironomid.compS * demandCIn))) * feed;
				feedForSpecies[1] = ((Limpet.compS * demandL2In) / ((Limpet.compS * demandL1In) + (Limpet.compS * demandL2In) + (Mayfly.compS * demandMIn) + (Chironomid.compS * demandCIn))) * feed;
				feedForSpecies[2] = ((Mayfly.compS * demandMIn) / ((Limpet.compS * demandL1In) + (Limpet.compS * demandL2In) + (Mayfly.compS * demandMIn) + (Chironomid.compS * demandCIn))) * feed;
				feedForSpecies[3] = ((Chironomid.compS * demandCIn) / ((Limpet.compS * demandL1In) + (Limpet.compS * demandL2In) + (Mayfly.compS * demandMIn) + (Chironomid.compS * demandCIn))) * feed;

			} else {
				feedForSpecies[0] = 0;
				feedForSpecies[1] = 0;
				feedForSpecies[2] = 0;
				feedForSpecies[3] = 0;
			}

			// Calculation of sated amount of individuals of each species by given feed
			double[] satedByFeed = { 0.0, 0.0, 0.0, 0.0 };
			if (feedForSpecies[0] > 0) {
				satedByFeed[0] = feedForSpecies[0] / k_i2(1, 0);
			}
			if (feedForSpecies[1] > 0) {
				satedByFeed[1] = feedForSpecies[1] / k_i2(1, 1);
			}
			if (feedForSpecies[2] > 0) {
				satedByFeed[2] = feedForSpecies[2] / k_i2(2, 0);
			}
			if (feedForSpecies[3] > 0) {
				satedByFeed[3] = feedForSpecies[3] / k_i2(3, 0);
			}

			// Settlement of few immigrants
			Limpets.abundance[1] += satedByFeed[0];
			Limpets.immigration[0] -= satedByFeed[0];
			Limpets.abundance[2] += satedByFeed[1];
			Limpets.immigration[1] -= satedByFeed[1];

			Mayflys.abundance[1] += satedByFeed[2];
			Mayflys.immigration[0] -= satedByFeed[2];

			Chironomids.abundance[1] += satedByFeed[3];
			Chironomids.immigration[0] -= satedByFeed[3];

			// Drift of immigrants who can not be stated
			Limpets.emigration[0] += Limpets.immigration[0];
			Limpets.emigration[1] += Limpets.immigration[1];
			Limpets.immigration[0] = 0;
			Limpets.immigration[1] = 0;
			Mayflys.emigration[0] += Mayflys.immigration[0];
			Mayflys.immigration[0] = 0;
			Chironomids.emigration[0] += Chironomids.immigration[0];
			Chironomids.immigration[0] = 0;

			// Reduction of biomass case 1
			reductionBiofilm(1, 0);
		} else {
			// Competition of inhabitants
			// Calculate available feed for each species
			double feedForSpecies[] = new double[4];

			if ((demandL1 + demandL2 + demandM + demandC) != 0) {
				feedForSpecies[0] = ((Limpet.compS * demandL1) / ((Limpet.compS * demandL1) + (Limpet.compS * demandL2) + (Mayfly.compS * demandM) + (Chironomid.compS * demandC))) * feed;
				feedForSpecies[1] = ((Limpet.compS * demandL2) / ((Limpet.compS * demandL1) + (Limpet.compS * demandL2) + (Mayfly.compS * demandM) + (Chironomid.compS * demandC))) * feed;
				feedForSpecies[2] = ((Mayfly.compS * demandM) / ((Limpet.compS * demandL1) + (Limpet.compS * demandL2) + (Mayfly.compS * demandM) + (Chironomid.compS * demandC))) * feed;
				feedForSpecies[3] = ((Chironomid.compS * demandC) / ((Limpet.compS * demandL1) + (Limpet.compS * demandL2) + (Mayfly.compS * demandM) + (Chironomid.compS * demandC))) * feed;
				
			
			} else {
				feedForSpecies[0] = 0;
				feedForSpecies[1] = 0;
				feedForSpecies[2] = 0;
				feedForSpecies[3] = 0;
			}

			// Calculation of sated amount of individuals of each species by given feed
			double[] satedByFeed = { 0.0, 0.0, 0.0, 0.0 };
			if (feedForSpecies[0] > 0) {
				satedByFeed[0] = feedForSpecies[0] / k_i2(1, 0);
			}
			if (feedForSpecies[1] > 0) {
				satedByFeed[1] = feedForSpecies[1] / k_i2(1, 1);
			}
			if (feedForSpecies[2] > 0) {
				satedByFeed[2] = feedForSpecies[2] / k_i2(2, 0);
			}
			if (feedForSpecies[3] > 0) {
				satedByFeed[3] = feedForSpecies[3] / k_i2(3, 0);
			}

			// A few inhabitants could stay
			Limpets.emigration[0] += Limpets.abundance[1] - satedByFeed[0]; 
			Limpets.abundance[1] = satedByFeed[0];
			Limpets.emigration[1] += Limpets.abundance[2] - satedByFeed[1];
			Limpets.abundance[2] = satedByFeed[1];

			Mayflys.emigration[0] += Mayflys.abundance[1] - satedByFeed[2];
			Mayflys.abundance[1] = satedByFeed[2];

			Chironomids.emigration[0] += Chironomids.abundance[1] - satedByFeed[3];
			Chironomids.abundance[1] = satedByFeed[3];

			// all immigration drift
			Limpets.emigration[0] += Limpets.immigration[0];
			Limpets.emigration[1] += Limpets.immigration[1];
			Limpets.immigration[0] = 0; //limpets die, cannot drift
			Limpets.immigration[1] = 0;
			Mayflys.emigration[0] += Mayflys.immigration[0];
			Mayflys.immigration[0] = 0;
			Chironomids.emigration[0] += Chironomids.immigration[0];
			Chironomids.immigration[0] = 0;

			// Reduction of biomass case 1
			reductionBiofilm(1, 0);
		}

		if (Limpets.immigration[0] != 0 || Limpets.immigration[1] != 0 || Mayflys.immigration[0] != 0
				|| Chironomids.immigration[0] != 0) {
			System.out.println("Grazers still in immigration(grazing())!!!!!!!!!!!! Still in immigration - Limpets: "
					+ (Limpets.immigration[0] + Limpets.immigration[1]) + ", Mayflys: " + Mayflys.immigration[0]
					+ ", Chironomids: " + Chironomids.immigration[0]);

		}

	}

	/**
	 * Reduction of biofilm: case 1 - available feed is eaten up, case 2 -
	 * surplus/left over
	 * 
	 * @param c  1: enough grazer; 2: not enough grazers
	 * @param totaldem total biofilm demand of grazers
	 */
	public void reductionBiofilm(int c, double totaldem) {
		if (c == 1) {
			// enough/too many grazers --> eat up feed
			BiofilmF -= feedF;
			BiofilmN -= feedN;
			BiofilmM -= feedM;
		} else {
			// not enough grazers
			if (feed != 0) {
				double grazedBiomassF = (totaldem / feed) * feedF;
				BiofilmF -= grazedBiomassF;
				double grazedBiomassN = (totaldem / feed) * feedN;
				BiofilmN -= grazedBiomassN;
				double grazedBiomassM = (totaldem / feed) * feedM;
				BiofilmM -= grazedBiomassM;
			}
		}

	}


	// **********************************************Eutrophication**************************************************//

	/**
	 * Calculation: Probability to develop filamentous algae
	 * 
	 * @return probability
	 */
	public double calculatePF() {
		if (biofilmCon[0] == 1)
			return 0;
		if(d / reff() > 1) return 0;
		return 4 * (BiofilmF / (K * epsilon)) * (1 - (BiofilmF / (K * epsilon))) * (1 - d / reff())
				* SimulationMain.pFMax;
	}

	/**
	 * Calculation: Probability to develop filamentous algae (linear increase, starting at betaF and end at beta0 with pfMax)
	 * 
	 * @param pfmax maximal probability
	 * @param betaF threshold at which the probability increases
	 * @return probability
	 */
	public double calculatePF2(double pfmax, double betaF) {
		//check if threshold is reached or biofilm already eutrophicated by thick biofilm
		if(BiofilmF < betaF || biofilmCon[0] == 1) {
			return 0.0;
		}else {
			//calculate function
			double m = pfmax/((beta0 * epsilon * (1 - mu))-betaF);	//slope
			double b = - m*betaF;									//y axis intercept
			
			double pfFunction;
			if(SimulationMain.DepOnLight) { //depend on light and biofilm biomass
				pfFunction = eL()*m*BiofilmF+b;						//linear function
			}else { //only depent on biofilm biomass
				pfFunction = m*BiofilmF+b;						//linear function
			}
			
			return pfFunction;
		}	
	}
	
	/**
	 * Calculation: Constant pf 
	 * @param pf
	 * @return probability 
	 */
	public double calculatePF3(double pf) {
		//check if biofilm already eutrophicated by thick biofilm
		if(biofilmCon[0] == 1) {
			return 0.0;
		}else {
			return pf;
		}	
	}
	
	/**
	 * Sort organisms when filamentous events takes place the first time
	 */
	public void filaEvent() {
		//Limpets die in filamentous part, only in mirco niche survive
		Limpets.abundance[1] = mu* Limpets.abundance[1];
		Limpets.abundance[2] = mu* Limpets.abundance[2];
		//Mayfly in filamentous part drift into the next segment
		Mayflys.emigration[0] = (1-mu) * Mayflys.abundance[1];
		Mayflys.abundance[1] = mu * Mayflys.abundance[1];
		//Chiro stay and transferred to another counter for larvae grazing on filamentous algae
		Chironomids.abundanceFila = (1-mu) * Chironomids.abundance[1]; 
		Chironomids.abundance[1] = mu * Chironomids.abundance[1];
	}
	
	
	/**
	 * Method to proof is the threshold beta0 is exceeded
	 * 
	 * @param type of biofilm
	 * @return true exceeded threshold
	 */
	public boolean proofBeta0(String type) {
		if (type == "BiofilmF") {
			return BiofilmF > beta0 * epsilon * (1 - mu);}
		if (type == "BiofilmN") {	
			return BiofilmN > beta0 * (1 - epsilon) * (1 - mu);}
		return false;
	}

	/**
	 * Method to proof is the threshold beta1 is exceeded
	 * 
	 * @param type of biofilm
	 * @return true exceeded threshold
	 */
	public boolean proofBeta1(String type) {
		if (type == "BiofilmF")
			return BiofilmF > beta1 * epsilon * (1 - mu);
		if (type == "BiofilmN")
			return BiofilmN > beta1 * (1 - epsilon) * (1 - mu);
		return false;
	}

	/**
	 * Method that rips off biofilm
	 * 
	 * @param type of biofilm
	 */
	public void biofilmRipsOff(String type) {
		BiofilmRip[0] = 0;
		BiofilmRip[1] = 0;
		if (type == "BiofilmF") {
			if(SimulationMain.grazingFila == 1 & biofilmCon[0] == 2) { //if Chiros could eat from filamentous algae
				Chironomids.abundanceFila = 0; //chiros in filamentous part die when filamentous rip off
			}
			
			if(biofilmCon[0] == 2) {
				Mayflys.abundance[0] = Mayflys.abundance[0] - Mayflys.abundance[0]*(1-mu)*epsilon*SimulationMain.mortalityEggsFila;
				Chironomids.abundance[0] = Chironomids.abundance[0] - Chironomids.abundance[0]*(1-mu)*epsilon*SimulationMain.mortalityEggsFila;
				
			}
			
			biofilmCon[0] = 0;
			BiofilmRip[0] = BiofilmF - (B0 * (1 - mu) * epsilon);
			BiofilmF = B0 * (1 - mu) * epsilon;
			
			
		}
		if (type == "BiofilmN") {
			biofilmCon[1] = 0;
			BiofilmRip[1] = BiofilmN - (B0 * (1 - mu) * (1 - epsilon));
			BiofilmN = B0 * (1 - mu) * (1 - epsilon);
		}

	}



	// *****************************************Stressors and their effect****************************************//
	
	/**
	 * Returns the TU of pesticide
	 * 
	 * @param s type of pesticide (herbicide/insecticide)
	 * @return stress/effect by pesticide
	 * 
	 */
	public double Stress(String s) {
		if (s.equals("Insecticide"))
			//if(SimulationMain.w % 4 == 0) 
				return pesticideModul.pesticideEffect(0,"Insecticide",StressorInsect[SimulationMain.month - SimulationMain.startMonth]);
			//else return 0.0;
		if (s.equals("Herbicide"))
			//if(SimulationMain.w % 4 == 0) 
			return pesticideModul.pesticideEffect(0,"Herbicide",StressorHerbi[SimulationMain.month - SimulationMain.startMonth]);
			//else return 0.0;
		return 0;
	}

	
	// *****************************************Setup Environmental Parameter****************************************//

	/**
	 * Get a temperature for a specific week
	 * 
	 * @param w week
	 * @return temperature in a certain week
	 */
	public double getTemperature(int w) {
		return tempWater[w];
	}

	/**
	 * Van't hoffschen rule - 10 degrees warmer double reaction speed
	 * 
	 * @return effect of biomass of species
	 */
	public double eT() {
		double Q10 = 2;
		double eT = Math.pow(Q10, (getTemperature(SimulationMain.ts) - 10) / 10);
		return eT;
	}

	/**
	 * Arrhenius function
	 * 
	 * @return effect of biomass of species
	 */
	public double eT2(double tref) {
		double kappa = 8.62 * Math.pow(10, -5); //Koltzmann's constant
		double E = 0.79; //Dillon 2010
		double Tref = tref;//11.6; //mean of calculate temperature
		return Math.exp((-E / kappa) * (1/(getTemperature(SimulationMain.ts) + 273.15) - (1/(Tref+273.15))));
	}

	/**
	 * Calculate the solar radiation in a specific month
	 * 
	 * @param month month
	 * @return solar radiation in the given month
	 * 
	 */
	private double solarRadiation(int week) {
		return 87.9 - 78.8 * Math.cos(2 * Math.PI * ((double) week + 1.4) / 48);
	}

	
	/**
	 * Return seasonal shading
	 * @param s maximal shading
	 * @param month month
	 * @return shade in the segment in a specific month
	 */
	public static double seasonalShade(double s, int week) {
		double X = (week%48)*(365/48); //Conversion from day to month
		if(-7.87 + 0.12*X - 0.00027*Math.pow(X, 2) < 0) {
			return 0.0;
		}
		else {
			return s*(-7.87 + 0.12*X - 0.00027*Math.pow(X, 2))/5.46333;
		}
	}
	
	
	/**
	 * Calculate the response curve to a solar radiation
	 * 
	 * @return light response
	 */
	public double eL() {
		double radiationM = solarRadiation(SimulationMain.week); // in kWh/m^2 per month
		double PFD = radiationM * scaleL * PAR * (1-seasonalShade(shadeFactor, SimulationMain.week));

		LogNormalDistribution L = new LogNormalDistribution(5, 1);
		return L.cumulativeProbability(PFD);
	}

	/**
	 * Response to nutrients
	 * 
	 * @return nutrient response
	 */
	public double eN() {
		return nutrients;
	}
	
	
	//-------Analysis
	/**
	 * Calculates eutrophic part of segment
	 * @return part eutrophic
	 */
	public double partE() {
		double part = 0;
		if (biofilmCon[0] == 1 || biofilmCon[0] == 2) {
			part += (1-mu)*epsilon;
		}
		if (biofilmCon[1] == 1) {
			part += (1-mu)*(1-epsilon);
		}
		
		return part;
	}

	/**
	 * Set abundances to zero if they become to small
	 */
	public void killMinimalSpecies() {
		double thresKill = 0.0001;
		if (Limpets.abundance[0] < thresKill) {
			Limpets.abundance[0] = 0.0;
		}
		if (Limpets.abundance[1] < thresKill) {
			Limpets.abundance[1] = 0.0;
		}
		if (Limpets.abundance[2] < thresKill) {
			Limpets.abundance[2] = 0.0;
		}

		
		if (Mayflys.abundance[0] < thresKill) {
			Mayflys.abundance[0] = 0.0;
		}
		if (Mayflys.abundance[1] < thresKill) {
			Mayflys.abundance[1] = 0.0;
		}
		if (Mayflys.abundance[2] < thresKill) {
			Mayflys.abundance[2] = 0.0;
		}
		
		if (Chironomids.abundance[0] < thresKill) {
			Chironomids.abundance[0] = 0.0;
		}
		if (Chironomids.abundance[1] < thresKill) {
			Chironomids.abundance[1] = 0.0;
		}
		if (Chironomids.abundance[2] < thresKill) {
			Chironomids.abundance[2] = 0.0;
		}
		
		if (Stoneflys.abundance[0] < thresKill) {
			Stoneflys.abundance[0] = 0.0;
		}
		if (Stoneflys.abundance[1] < thresKill) {
			Stoneflys.abundance[1] = 0.0;
		}
		if (Stoneflys.abundance[2] < thresKill) {
			Stoneflys.abundance[2] = 0.0;
		}
		if (Stoneflys.abundance[3] < thresKill) {
			Stoneflys.abundance[3] = 0.0;
		}
		
	}
	
	
	
}
