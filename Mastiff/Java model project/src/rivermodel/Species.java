/*
 * Species class
 */
package rivermodel;


/**
 *
 * @author Laura Meier
 * @version 05.07.21
 */
public class Species {
    
    int codeSpecies;		//species code
    double[] abundance; 	//Abundance of life stages
    double abundanceFila; 	//Abundance of larvae grazing on filamentous algae
	int ovipositionM;   	//month when the eggs were usually set
    int hatchingM;   		//time span between oviposition and hatching
    int adultM;      		//time span after the species is able to reproduce (emergence) between hatching and being adult
    int deathM;       		//month in which the maximal time span of individual in last age class is reached
    double offspring;		//amount of offspring
    double mortalityRate;	//mortality rate (natural)
    double quality;			//quality (conversion rate)
    double[] bodymass;		//bodymass
    double IGmax;			//ingestion rate
    double[] tempThres;		//temperature threshold at which letal effects occur, m, b for function
    double sensitivity;		//sensitivity towards insecticide
    double compS; 			//Competition strength
    
    double[]emigration;		//leave population - outflowing/emigrants
    double[]immigration;	//would like to join population - inflowing/immigrants
    
    /**
     * Constructor
     * 
     * @param code species identification code
     * @param abun abundances for each lifes stage
     * @param airworthy true if species has an airworthy stage
     * @param SegN
     */
    public Species(int code, double[] abun, boolean airworthly, int SegN){
    	codeSpecies = code;
    	abundance = abun;
    	if(airworthly) { // not an airworthly status - eat lifelong on biofilm
    		emigration = new double[abun.length-2];
    		immigration = new double[abun.length-2];
    		bodymass = new double[abundance.length-2];
    	}
    	else {
    		emigration = new double[abun.length-1];
    		immigration = new double[abun.length-1];
    		bodymass = new double[abundance.length-1];
    	}
    	setPopulationSpecificParameters(code, SegN);
    }
    
    
    /**
     * Set Population specific parameter
     * @param c species code
     * @param SegN segment number
     */
    private void setPopulationSpecificParameters(int c, int SegN) {
    	if (c == Limpet.codeSpecies) {//Limpet
    		ovipositionM = Limpet.ovipositionM;  
    	    hatchingM = Limpet.hatchingM;     
    	    adultM = Limpet.adultM;       
    	    deathM = Limpet.deathM;    
    	    offspring = Limpet.offspring;
    	    mortalityRate = Limpet.mortalityRate;
    	    quality = Limpet.quality;
    	    bodymass[0] = Limpet.setBodymass(0);
    	    bodymass[1] = Limpet.setBodymass(1);
    	    IGmax = Limpet.IGmax;
    	    tempThres = Limpet.tempThres;
    	    compS = Limpet.compS;
    	    emigration[0] = 0;
    	    emigration[1] = 0;
    	    immigration[0] = 0;
    	    immigration[1] = 0;
    	    if(SegN == 0 && abundance[1]!=0 && SimulationMain.startMonth > adultM && SimulationMain.startMonth < hatchingM) {
    	    	System.out.println("Unsual limpet characteristics - expected no limpet in abundance first grazing stage (setPopulationSpecificParameters)");
    	    }
    	    if(SegN == 0 && abundance[2]!=0 && (SimulationMain.startMonth < adultM || SimulationMain.startMonth > deathM)) {
    	    	System.out.println("Unsual limpet characteristics - expected no limpet in abundance last grazing stage (setPopulationSpecificParameters)");
    	    }
    	}
    	if (c == Mayfly.codeSpecies) {//Mayfly
    		 ovipositionM = Mayfly.ovipositionM;  
    	     hatchingM = Mayfly.hatchingM;     
    	     adultM = Mayfly.adultM;       
    	     deathM = Mayfly.deathM;    
    	     offspring = Mayfly.offspring;
     	     mortalityRate = Mayfly.mortalityRate;
     	     quality = Mayfly.quality;
     	     bodymass[0] = Mayfly.setBodymass();
     	     IGmax = Mayfly.IGmax;
     	     tempThres = Mayfly.tempThres;
     	     compS = Mayfly.compS;
     	     emigration[0] = 0;
     	     immigration[0] = 0;
     	    if(abundance[1]!=0 && SimulationMain.startMonth > adultM && SimulationMain.startMonth < hatchingM) {
    	    	if (SegN == 0) {
    	    		System.out.println("Unsual mayfly characteristics - expected no mayfly in abundance larval stage (setPopulationSpecificParameters)");
    	    		System.out.println("Larvae assigned to eggs - Mayfly");
    	    	}
    	    	
    	    	abundance[0] = abundance[1];
    	    	abundance[1] = 0;
    	    }
    	}
    	if (c == Chironomid.codeSpecies) {//Chironomid
    		Chironomid.setParam();
    		ovipositionM = Chironomid.ovipositionM;  
   	     	hatchingM = Chironomid.hatchingM;     
   	     	adultM = Chironomid.adultM;       
   	        deathM = Chironomid.deathM;    
   	     	offspring = Chironomid.offspring;
    	    mortalityRate = Chironomid.mortalityRate; 
    	    quality = Chironomid.quality;
    	    bodymass[0] = Chironomid.setBodymass();
    	    IGmax = Chironomid.IGmax;
    	    tempThres = Chironomid.tempThres;
    	    compS = Chironomid.compS;
    	    emigration[0] = 0;
    	    immigration[0] = 0;
    	    if(abundance[1]!=0 && (SimulationMain.startMonth == 0 || SimulationMain.startMonth == 1 || SimulationMain.startMonth == 2 || SimulationMain.startMonth == 11)) {
    	    	if(SegN == 0) {
    	    		System.out.println("Unsual chiro characteristics - expected no chiro in abundance larval stage (setPopulationSpecificParameters)");
    	    		System.out.println("Larvae assigned to eggs - Chiro");
    	    	}
    	    	abundance[0] = abundance[1];
    	    	abundance[1] = 0;
    	    }
    	}
    	if (c == Stonefly.codeSpecies) {//Stonefly
    		ovipositionM = Stonefly.ovipositionM;  
   	     	hatchingM = Stonefly.hatchingM;     
   	     	adultM = Stonefly.adultM;       
   	        deathM = Stonefly.deathM;    
   	     	offspring = Stonefly.offspring;
    	    mortalityRate = Stonefly.mortalityRate;
    	    quality = Stonefly.quality;
    	    bodymass[0] = Stonefly.setBodymass(0);
    	    bodymass[1] = Stonefly.setBodymass(1);
    	    bodymass[2] = Stonefly.setBodymass(2);
    	    IGmax = Stonefly.IGmax;
    	    tempThres = Stonefly.tempThres;
    	    emigration[0] = 0;
    	    emigration[1] = 0;
    	    emigration[2] = 0;
    	    immigration[0] = 0;
    	    immigration[1] = 0;
    	    immigration[2] = 0;
    	    if(SegN == 0 && abundance[1]!=0 && SimulationMain.startMonth > ovipositionM && SimulationMain.startMonth < hatchingM) {
    	    	System.out.println("Unsual stonefly characteristics - expected no stonefly in abundance first larval stage (setPopulationSpecificParameters)");
    	    }    
    	}  	
    	
	}

    
    /**
     * Proofs if the individual (eggs state) can hatch
     * @return true if hatching month is reached
     */
    public boolean canHatch() {
    	if(SimulationMain.month % 12 == hatchingM) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * Method for eggs to hatch to larvae
     */
    public void hatching() {
    	if(canHatch()) {
    		if(SimulationMain.grazingMethode != 1) { //grazing method A & C
    			abundance[1] = abundance[1] + abundance[0]; //assigned to inhabit
				abundance[0] = 0;
    		}
			else { //grazing method B
				immigration[0] = immigration[0] + abundance[0]; //assigned to inflow
				abundance[0] = 0;
			}
		}
    }
    
    /**
     * Method for eggs to hatch to larvae under filamentous conditions when chironomids could also eat/hatch/live in filamentous part
     * @param sp species behavior - 1: only have eggs in mirco-niche, 2:die in non micro niche, 3: normal hatching and partly transferred to abundanceFila
     * @param muSeg mu value of the segment (micro niche size)
     */
    public void hatching2(int sp, double muSeg) {
    	if(canHatch()) {
    		if(SimulationMain.grazingMethode != 1) { //grazing method A & C
    							
				if(sp == 1) {
					abundance[1] = abundance[1] + abundance[0]; //assigned to inhabit
					abundance[0] = 0;
				}
				if(sp == 2) {
					abundance[1] = abundance[1] + (abundance[0]*muSeg); //part assgined to inhabit, rest die
					abundance[0] = 0;
				}
				if(sp == 3) {
					abundance[1] = abundance[1] + (abundance[0]*muSeg); //part assgined to inhabit of BM, rest assigned sated to BF
					abundanceFila = abundanceFila + abundance[0]*(1-muSeg);
					abundance[0] = 0;
				}
    		}
			else { //grazing method B
				immigration[0] = immigration[0] + abundance[0]; //assigned to inflow
				abundance[0] = 0;
				System.out.println("hatching 2: situation not specifically implemented (grazing filamentous algae by chiros + grazing methond B)");
				
			}
		}
    }
    
    
    /**
     * Method to switch between larvae age classes
     */
    public void growth() {
    	//Chance growth classes - for semivoltine species
    	if(SimulationMain.month % 12 == adultM) {
    		for(int i = abundance.length-2; i >= 2; i--) {
    			abundance[i] = abundance[i-1];
    			abundance[i-1] = 0;
    		}
    	}    	
    }
    
    /**
     * Check if the individuals is fully grown up and can reproduce
     * @return true if adult age is reached
     * @return false if adult age is not reached
     */
    private boolean isadult(){
    	if(SimulationMain.month % 12 == adultM) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * Development to large class (emergence/fertile adults)
     */
    public void developAdult() {
    	if(isadult()) {
    		abundance[abundance.length-1] += abundance[abundance.length-2];
    		abundance[abundance.length-2] = 0;
    		abundance[abundance.length-1] += immigration[immigration.length-1]; //last laval state add to abundance adults (important for Grazing B)
    		immigration[immigration.length-1] = 0;
    	}
    }
    
    /**
     * Oviposition/lay eggs in same segment
     */
    public void oviposition(double fertilI) {
    	if(SimulationMain.month % 12 == ovipositionM) {
    		abundance[0]= fertilI*offspring;
    	}
    	//for mulitvoltine species
    	if (codeSpecies == Chironomid.codeSpecies) {
    		setNextOvi();
    	}
    }
    
    /**
     * Only for Chironomids
     */
    private void setNextOvi() {
    	//Chironomid
		Chironomid.setParam();
		ovipositionM = Chironomid.ovipositionM;
		hatchingM = Chironomid.hatchingM;
		adultM = Chironomid.adultM;
		deathM = Chironomid.deathM;
    }
    
    
    
    /**
     * Mortality if species drift into the next segment
     * @param delta drift mortality
     */
    public void mortalityDrift(double delta) {
    	//immigration
    	for(int i = 0; i < emigration.length; i++) {
    		emigration[i] = emigration[i]*(1-delta);
    	}
    }
    
    /**
     * Temperature dependent mortality
     * @param adultlarvae
     */
    public void mortalityTemp(double temperature, boolean adultlarvae) {
		for(int i = 1; i < abundance.length-1; i++) {
    		abundance[i] = abundance[i]*mortTempFunction(temperature,tempThres[0],tempThres[1],tempThres[2]);
    		if (abundance[i] <0) throw new IllegalArgumentException("Negative abundance");    		
    	}
		
		if(adultlarvae) {
    		abundance[abundance.length-1] = abundance[abundance.length-1]*mortTempFunction(temperature,tempThres[0],tempThres[1],tempThres[2]);
    		if (abundance[abundance.length-1] <0) throw new IllegalArgumentException("Negative abundance");
    	}
    }
    
    /**
     * Survival function for water temperature 
     * @param temp recent water temperature
     * @param threshold threshold at which temperature mortality takes place
     * @param m slope
     * @param b y-axis section
     * @return survival at a certain temperature
     */
    private double mortTempFunction(double temp, double threshold, double m, double b) {
    	if(temp < threshold) return 1.0;
    	else return m*temp+b;
    }
    
    /**
     * Mortality for organisms
     * @param stressMortality mortality by stressor eg. insecticides
     * @param senSpecies sensitivity of species towards insecticide
     * @param sc scale factor reduces mortality in winter (inactivity)
     * @param alpha true if highest orders of predation (realistic alpha = 2)
     * @param scAbun
     * @param adultlarvae true if adult state is also influenced by natural mortality
     */
    public void mortality(double stressMortality, double sensSpecies, double sc, boolean alpha, double scAbun, boolean adultlarvae) {
    	//inhabitants
    	if(alpha == false) {
    		for(int i = 1; i < abundance.length-1; i++) {
        		abundance[i] = abundance[i]*(1-sc*mortalityRate)*(1-stressMortality*sensSpecies);
        		if (abundance[i] <0) throw new IllegalArgumentException("Negative abundance");     		
        	}
    		
    		if(adultlarvae) {
        		abundance[abundance.length-1] = abundance[abundance.length-1]*(1-sc*mortalityRate)*(1-stressMortality*sensSpecies);
        		if (abundance[abundance.length-1] <0) throw new IllegalArgumentException("Negative abundance");
        	}
    		
    		if(SimulationMain.grazingFila == 1) {
    			abundanceFila = abundanceFila*(1-sc*mortalityRate)*(1-stressMortality*sensSpecies);
    			if (abundanceFila <0) throw new IllegalArgumentException("Negative abundance (abundanceFila)");
    		}
    		
    	}else {
    		for(int i = 1; i < abundance.length; i++) {
    			abundance[i] = abundance[i]*(1-(mortalityRate*sc*abundance[i]/scAbun))*(1-stressMortality*sensSpecies); 
    			if (abundance[i] <0) throw new IllegalArgumentException("Negative abundance");
    		}
    		
    		if(adultlarvae) {
    			abundance[abundance.length-1] = abundance[abundance.length-1]*(1-(mortalityRate*sc*abundance[abundance.length-1]/scAbun))*(1-stressMortality*sensSpecies); 
        		if (abundance[abundance.length-1] <0) throw new IllegalArgumentException("Negative abundance");
        	}	
    		
    	}
    	
    	
    	//Maximal lifetime reached
    	if(reachedMaxLife()) {
    		abundance[abundance.length-1] = 0;
    	}
    	
    	//Immigration by drift through
    	for(int i = 0; i < immigration.length; i++) {
    		immigration[i] = immigration[i]*(1-stressMortality*sensSpecies);					//only stress mortality
    	}	
    }
    
    

    /**
     * Check if the individual reached maximal life
     * @return true if maximal life is reach, if not false
     */
    private boolean reachedMaxLife(){
        if(SimulationMain.month % 12 == deathM){
            return true;
        }  
        return false;
    }
     
    
}
