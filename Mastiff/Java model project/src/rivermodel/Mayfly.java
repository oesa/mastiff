/*
 * Species based on limpet life cycle 
 */
package rivermodel;


/**
 *
 * @author Laura Meier
 * @version 05.07.21
 */
public class Mayfly {

	public static int codeSpecies = 2;
	public static int ovipositionM = 4;
	public static int hatchingM = 7;
	public static int adultM = 4;
	public static int deathM = 5;
	public static double offspring; //= 200;
	public static double mortalityRate; // = 0.07; // default 0.07;
	public static double quality = 2;
	public static double bodymass;
	public static double IGmax = 5.77;
	public static double Tref = 14.5;
	public static double sensitivity = 1;
	public static double compS; 
	public static double probstay;// = 0.04;
	public static double probup;// = 0.9;
	public static double probdown;// = 1- probup;
	public static double distmeanup;// = 17;
	public static double distmeandown;// = 2;
	public static double[] tempThres = {16.0,-0.055,1.88}; //threshold,m,b

	/**
	 * Set bodymass - growth
	 * @return new bodymass
	 */
	public static double setBodymass() {
		switch ((SimulationMain.week + 48 - (hatchingM*4)) %48) {
		case 0: bodymass = 0.01; break;
		case 1: bodymass = 0.0125; break;
		case 2: bodymass = 0.015; break;
		case 3: bodymass = 0.0175; break;
		case 4: bodymass = 0.02; break;
		case 5: bodymass = 0.0275; break;
		case 6: bodymass = 0.035; break;
		case 7: bodymass = 0.0425; break;
		case 8: bodymass = 0.05; break;
		case 9: bodymass = 0.0625; break;
		case 10: bodymass = 0.075; break;
		case 11: bodymass = 0.0875; break;
		case 12: bodymass = 0.1; break;
		case 13: bodymass = 0.1125; break;
		case 14: bodymass = 0.125; break;
		case 15: bodymass = 0.1375; break;
		case 16: bodymass = 0.15; break;
		case 17: bodymass = 0.1625; break;
		case 18: bodymass = 0.175; break;
		case 19: bodymass = 0.1875; break;
		case 20: bodymass = 0.2; break;
		case 21: bodymass = 0.275; break;
		case 22: bodymass = 0.35; break;
		case 23: bodymass = 0.425; break;
		case 24: bodymass = 0.5; break;
		case 25: bodymass = 0.625; break;
		case 26: bodymass = 0.75; break;
		case 27: bodymass = 0.875; break;
		case 28: bodymass = 1.0; break;
		case 29: bodymass = 1.25; break;
		case 30: bodymass = 1.5; break;
		case 31: bodymass = 1.75; break;
		case 32: bodymass = 2.0; break;
		case 33: bodymass = 2.25; break;
		case 34: bodymass = 2.5; break;
		case 35: bodymass = 2.75; break;
		case 36: bodymass = 3.0; break;
		case 37: bodymass = 3.25; break;
		case 38: bodymass = 3.5; break;
		case 39: bodymass = 3.75; break;
		case 40: bodymass = 4.0; break;
		case 41: bodymass = 4.0; break;
		case 42: bodymass = 4.0; break;
		case 43: bodymass = 4.0; break;
		case 44: bodymass = 4.0; break;
		default: bodymass = 4.0; break;
		}
		return bodymass;
	}


	/**
	 * Exponential decreasing disperal distance (upstream)
	 * @param x segment
	 * @return probability to start in segment x to fly upstream and land in current segment
	 */
	private static double fup(double x) {
		return 1/distmeanup*Math.exp(x/distmeanup);
	}
	
	
	/**
	 * Exponential decreasing disperal distance (downstream)
	 * @param x segment
	 * @return probability to start in segment x to fly downstream and land in current segment
	 */
	private static double fdown(double x) {
		return 1/distmeandown*Math.exp(-x/distmeandown);
	}
	
	
	/**
	 * Species distribution kernel for disperal of imagines
	 * @param Seg Seg start segment of imagines
	 * @return probability to land in current segment
	 */
	public static double d(double Seg) {
		if(Seg > 0) return (1-probstay)*probdown*fdown(Seg);
		else if(Seg < 0) return (1-probstay)*probup*fup(Seg);
		else return probstay; //k == i --> Seg = 0
	}
	
	/** 
	 * Set mortality rate
	 * @param newMort mortality rate
	 */
	public static void setMort(double newMort) {
		mortalityRate = newMort;
	}
	
	/**
	 * Set probability to stay in the same segment
	 * @param v probability to stay
	 */
	public static void setProbStay(double v) {
		probstay = v;
	}
	
	/**
	 * Set probability to fly upstream
	 * @param v probability to fly upstream
	 */
	public static void setProbUp(double v) {
		probup = v;
	}
	
	/**
	 * Set probability to fly downstream
	 * @param v probability to fly downstream
	 */
	public static void setProbDown(double v) {
		probdown = v;
	}
	
	/**
	 * Set mean distance for upstream flight
	 * @param v mean distance
	 */
	public static void setDistUp(double v) {
		distmeanup = v;
	}
	
	/**
	 * Set mean distance for downstream flight
	 * @param v mean distance
	 */
	public static void setDistDown(double v) {
		distmeandown = v;
	}
	
	/**
	 * Set offspring
	 * @param off amount offspring
	 */
    public static void setOffspring(double off) {
		offspring = off;
	}
	
}
