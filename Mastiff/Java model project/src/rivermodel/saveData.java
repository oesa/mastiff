//*Class is saving data in files*

package rivermodel;

/**
 * @author Laura Meier
 * @version 05.07.21
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class saveData {

	private static String scenario = "test";
	private static int run = 1;
	private static String path = "";
	//+"_"+scenario+"_"+run
	
	// *Methods*

	/**
	 * Saves the biofilm data in a file
	 * 
	 * @param data
	 */
	public static void saveDataBiofilmC(int[][][] data, int fName) {


		final String filename;
		if (fName == 0) {
			filename = SimulationMain.pathString + SimulationMain.pathOutput + path + "/Data_biofilmC"+"_"+scenario+"_"+run+".csv";
		}else {
			filename = SimulationMain.pathString + SimulationMain.pathOutput + path + "/Data_biofilmC2"+"_"+scenario+"_"+run+".csv";
		}

		
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw.write(",");
					for (int y = 0; y < data[k][t].length; y++) {
						if (y != 0)
							bw.write(" ");
						bw.write(Integer.toString(data[k][t][y]));
					}
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Saves the biomass of the biofilm in a file
	 * 
	 * @param data
	 */
	public static void saveDataBiofilm(double[][][] data) {

		for (int y = 0; y < 3; y++) {
			final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path + "/Data_biofilm"+ y +"_"+scenario+"_"+run+".csv";

			BufferedWriter bw = null;
			FileWriter fw = null;

			try {
				fw = new FileWriter(filename);
				bw = new BufferedWriter(fw);

				// Write into file

				for (int k = 0; k < data.length; k++) {
					for (int t = 0; t < data[k].length; t++) {
						if (t != 0)
							bw.write(",");
						bw.write(Double.toString(data[k][t][y]));

					}
					bw.newLine();
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (bw != null)
						bw.close();
					if (fw != null)
						fw.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Saves the biomass of the biofilm in a file that flow down the river after a rip off
	 * 
	 * @param data
	 */
	public static void saveDataBiofilmRipsOff(double[][][] data) {

		for (int y = 0; y < 3; y++) {
			final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path + "/Data_biofilmRipOff"+ y +"_"+scenario+"_"+run+".csv";

			BufferedWriter bw = null;
			FileWriter fw = null;

			try {
				fw = new FileWriter(filename);
				bw = new BufferedWriter(fw);

				// Write into file

				for (int k = 0; k < data.length; k++) {
					for (int t = 0; t < data[k].length; t++) {
						if (t != 0)
							bw.write(",");
						bw.write(Double.toString(data[k][t][y]));

					}
					bw.newLine();
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (bw != null)
						bw.close();
					if (fw != null)
						fw.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	
	/**
	 * Saves the feed in a file
	 * 
	 * @param data
	 */
	public static void saveDataFeed(double[][][] data) {

		final String filename = SimulationMain.pathString +SimulationMain.pathOutput + path +"/Data_Feed"+"_"+scenario+"_"+run+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t][0]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		final String filename1 = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_FeedAfter"+"_"+scenario+"_"+run+".csv";

		BufferedWriter bw1 = null;
		FileWriter fw1 = null;

		try {
			fw1 = new FileWriter(filename1);
			bw1 = new BufferedWriter(fw1);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw1.write(",");
					bw1.write(Double.toString(data[k][t][1]));
				}
				bw1.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw1 != null)
					bw1.close();
				if (fw1 != null)
					fw1.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
	}
	
	/**
	 * Saves the eutroph part in a segment in a file
	 * 
	 * @param data
	 */
	public static void saveDataPartEutroph(double[][] data) {

		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_PartEutroph"+"_"+scenario+"_"+run+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	

	/**
	 * Saves the demand in a file
	 * 
	 * @param data
	 */
	public static void saveDataDemand(double[][][] data) {

		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_DemandInhabit"+"_"+scenario+"_"+run+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t][0]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		final String filename1 = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_DemandTotal"+"_"+scenario+"_"+run+".csv";

		BufferedWriter bw1 = null;
		FileWriter fw1 = null;

		try {
			fw1 = new FileWriter(filename1);
			bw1 = new BufferedWriter(fw1);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw1.write(",");
					bw1.write(Double.toString(data[k][t][1]));
				}
				bw1.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw1 != null)
					bw1.close();
				if (fw1 != null)
					fw1.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		final String filename2 = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_DemandHatch"+"_"+scenario+"_"+run+".csv";

		BufferedWriter bw2 = null;
		FileWriter fw2 = null;

		try {
			fw2 = new FileWriter(filename2);
			bw2 = new BufferedWriter(fw2);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw2.write(",");
					bw2.write(Double.toString(data[k][t][2]));
				}
				bw2.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw2 != null)
					bw2.close();
				if (fw2 != null)
					fw2.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}			
	}
	
	
	/**
	 * Save a specific species in a file
	 * 
	 * @param data
	 *            with segment, time, species, life form
	 * @param speciescode
	 *            species you would like to save
	 */
	public static void saveDataDemandSpecies(double[][][][] data) {
		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_DemandInhabitS_" + scenario+"_"+run+ ".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw.write(",");
					for (int y = 0; y < data[k][t][0].length; y++) {
						if (y != 0)
							bw.write(" ");
						bw.write(Double.toString(data[k][t][0][y]));
					}
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		final String filename1 = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_DemandImmiS_" + scenario+"_"+run+ ".csv";

		BufferedWriter bw1 = null;
		FileWriter fw1 = null;

		try {
			fw1 = new FileWriter(filename1);
			bw1 = new BufferedWriter(fw1);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw1.write(",");
					for (int y = 0; y < data[k][t][1].length; y++) {
						if (y != 0)
							bw1.write(" ");
						bw1.write(Double.toString(data[k][t][1][y]));
					}
				}
				bw1.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw1 != null)
					bw1.close();
				if (fw1 != null)
					fw1.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		
		final String filename2 = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_DemandHatchS_" + scenario+"_"+run+ ".csv";

		BufferedWriter bw2 = null;
		FileWriter fw2 = null;

		try {
			fw2 = new FileWriter(filename2);
			bw2 = new BufferedWriter(fw2);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw2.write(",");
					for (int y = 0; y < data[k][t][2].length; y++) {
						if (y != 0)
							bw2.write(" ");
						bw2.write(Double.toString(data[k][t][2][y]));
					}
				}
				bw2.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw2 != null)
					bw2.close();
				if (fw2 != null)
					fw2.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	
	
	/**
	 * Saves species in file
	 * 
	 * @param data
	 *            with segment, time, species, life form
	 */
	public static void saveDataSpecies(double[][][][] data) {
		saveDataS(data, 1);
		saveDataS(data, 2);
		saveDataS(data, 3);
		saveDataS(data, 4);
	}

	/**
	 * Saves species biomass in file (only larval biomass)
	 * 
	 * @param data
	 *            with segment, time, species
	 */
	public static void saveDataSpeciesBio(double[][][] data) {
		saveDataSBio(data, 1);
		saveDataSBio(data, 2);
		saveDataSBio(data, 3);
		saveDataSBio(data, 4);
	}
	
	
	/**
	 * Save a specific species in a file
	 * 
	 * @param data
	 *            with segment, time, species, life form
	 * @param speciescode
	 *            species you would like to save
	 */
	private static void saveDataS(double[][][][] data, int speciescode) {
		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_species_" + speciescode +"_"+scenario+"_"+run+ ".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw.write(",");
					for (int y = 0; y < data[k][t][speciescode - 1].length; y++) {
						if (y != 0)
							bw.write(" ");
						bw.write(Double.toString(data[k][t][speciescode - 1][y]));
					}
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
	
	/**
	 * Save a specific species biomass in a file
	 * 
	 * @param data
	 *            with segment, time, species
	 * @param speciescode
	 *            species you would like to save
	 */
	private static void saveDataSBio(double[][][] data, int speciescode) {
		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_species_" + speciescode +"_Biomasse_"+scenario+"_"+run+ ".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
				
					bw.write(Double.toString(data[k][t][speciescode - 1]));
					
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
	
	
	/**
	 * Save a dispersing imagines
	 * 
	 * @param data
	 *            with time, species, before/after dispersal
	 */
	public static void saveDataImagines(double[][][] data) {
		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_ImaginesLoss_"+scenario+"_"+run+ ".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw.write(",");
					for (int y = 0; y < data[k][t].length; y++) {
						if (y != 0)
							bw.write(" ");
						bw.write(Double.toString(data[k][t][y]));
					}
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
	
	
	
	/**
	 * Save a specific inflowing/outflowing species in a file
	 * 
	 * @param data
	 *            with segment, time, inflow/outflow, species
	 * @param flowcode
	 *            inflow/outflow you would like to save
	 */
	public static void saveDataInf(double[][][][] data, String flow, String bio) {
		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_species_" + flow + bio +"_"+scenario+"_"+run+ ".csv";
		int code;
		
		if(flow.equals("Inflow")) code = 0; //Inflow
		else code = 1; //Outflow
		
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0)
						bw.write(",");
					for (int y = 0; y < data[k][t][code].length; y++) {
						if (y != 0)
							bw.write(" ");
						bw.write(Double.toString(data[k][t][code][y]));
					}
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
	
	
	/**
	 * Saves the water temperature
	 * 
	 * @param data water temperature
	 */
	public static void saveWaterTemp(double[][] data) {

		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_WaterTemp"+"_"+scenario+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Saves the air temperature
	 * 
	 * @param data air temperature
	 */
	public static void saveAirTemp(int numseg) {
		
		double[][] data = new double[numseg][SimulationMain.simTime * 4];

		int count = 0;

		for (int i = 0; i < SimulationMain.simTime * 4; i++) {
			for (int j = 0; j < numseg; j++) {
				if (count < SimulationMain.weeksOfHWArray.length && i == SimulationMain.weeksOfHWArray[count]) {
					data[j][i] = WaterTemperature.airTemp(j, (i + SimulationMain.startMonth*4),
							SimulationMain.tempRise);

				} else {
					data[j][i] = WaterTemperature.airTemp(j, (i + SimulationMain.startMonth*4), 0);
				}
			}
			if (count < SimulationMain.weeksOfHWArray.length && i == SimulationMain.weeksOfHWArray[count]) {
				count += 1;
			}
		}
	

		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_AirTemp"+"_"+scenario+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}



	
	/**
	 * Saves the stressor
	 * 
	 * @param data stressor
	 */
	public static void saveStressor(String pesticide, double[][] data) {

		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_"+ pesticide +"_"+scenario+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Get scenario name
	 * @return name of scenario
	 */
	public static String getScenario() {
		return scenario;
	}

	/**
	 * Set scenario name
	 * @param scenario name of scenario
	 */
	public static void setScenario(String scenario) {
		saveData.scenario = scenario;
	}

	/**
	 * Get run of scenario
	 * @return run number
	 */
	public static int getRun() {
		return run;
	}

	/**
	 * Set Run number of scenario
	 * @param run number 
	 */
	public static void setRun(int run) {
		saveData.run = run;
	}

	/**
	 * Get path in which all 
	 * @return path
	 */
	public static String getPath() {
		return path;
	}
	
	public static void setPath(String p) {
		saveData.path = p;
	}
	
	
	/** 
	 * Save the probability of filamentous algae developping
	 */
	public static void saveDataPf(double[][] data) {

		final String filename = SimulationMain.pathString + SimulationMain.pathOutput + path +"/Data_ProbabilityPF_"+scenario+"_"+run+".csv";

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);

			// Write into file

			for (int k = 0; k < data.length; k++) {
				for (int t = 0; t < data[k].length; t++) {
					if (t != 0) bw.write(",");
					bw.write(Double.toString(data[k][t]));
				}
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}


	/** 
	 * Save parameter combination (with coexistence)
	 */
	public static void saveParameterSz(String scenario, String filenameOut) {
		
		BufferedWriter bwOut = null;
		FileWriter fwOut = null;
		
		
		try {
			fwOut = new FileWriter(filenameOut,true);
			bwOut = new BufferedWriter(fwOut);

			// Write into file

			bwOut.write(scenario);
			bwOut.newLine();
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bwOut != null)
					bwOut.close();
				if (fwOut != null)
					fwOut.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	

}
