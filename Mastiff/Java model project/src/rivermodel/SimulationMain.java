/*
 * Main method to start the simulation.
 * First initialize the simulation by creating segments and species. After, the 
 * simulations starts over 'SimTime' months.
 * --> Use Main in application for GUI interface
 */
package rivermodel;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Laura Meier
 * @version 05.07.21
 */

public class SimulationMain {

	// *Declaration of variables*
	
	//local
//	static File pfad = new File("");
//	public static String pathString = pfad.getAbsolutePath();
//	public static String pathParam = "/data/Parameter/"; 
//	public static String pathOutput = "/data/Results/";	
//	public static boolean autoRan = false;
	
	
	//as jar
	static File pfad = new File("");
	public static String pathString = pfad.getAbsolutePath();
	public static String pathParam = "/Data/"; 
	public static String pathOutput = "/Data/Results/";	
	public static boolean autoRan = false;
	
	
	// Initialization
	public static int simTime; // Simulation time in months
	public static int amountSegments; // Number of segments
	public static double airTemperatureMean[]; //Mean air temperature [segment]
	public static int[] weeksOfHWArray; //Weeks in which a heatwave takes place
	public static double tempRise; //rise of air temperature in weeks of heatwave
	public static boolean tempConst; //true, if water temperature is constant along the river
	public static double[][] waterTemperature; // water temperature [segment][month]
	public static double[] shadeArray; // shade in each segment
	public static double[][] InitialSpecies; // initial distribution of species [segment][species]
	public static int startMonth; // Month in which the simulation starts
	public static int month = 0;
	public static int week = 0;
	public static int ts = 0;
	public static double pFMax; // Maximal probability of a filamentous event
	public static double betaF; // Biofilm biomass threshold at which filamentous event could take place
	public static boolean stochLength; //true, if length of filamentous event should be stochastic chosen
	public static boolean DepOnLight; //true if pf depend on light conditions
	public static double scMort; //scale of mortality in winter
	public static boolean ddM; //density dependent mortality
	public static double mortalityEggsFila; //part egg mortality landing on filamentous algae
	public static double grazingMethode; //(A) hatched --> inhabit; (B) hatched --> inflow; (C) all equal
	public static double grazingFila; //0: , 1: filamentous eaten by Chiros
	public static boolean growWinter;
	public static String oviposition; //Oviposition method (constant/dynamic)
	public static double b; //Biofilm growth rate
	public static double beta0; //Handling threshold
	public static double B0; //minimum biofilm biomass
	public static double B1;
	public static double[] epsilonArray; // Spatial extent when a filamentous event takes place
	public static double[] muArray; // Spatial extent of micro niches
	public static double[] deltaArray; // Drift mortality for each segment
	public static double[] nutArray; //Nutrients for each segment
	public static String[] LUTArray; //land use type for each segment
	public static double[] beta1Array; //beta1 for each segment

	// Simulation
	public static Segments[] RiverSeg; // Segment objects of the river
	public static double[][] artInflow; // artificial inflow [time][species]
	public static double[][] riverOutflow; // Ouflow of individuals out of the system

	// Stressors
	public static double[][] Stressors_Insect;
	public static double[][] Stressors_Herbi;
	public static double[][] Stressor_Event_Insect;
	public static double[][] Stressor_Event_Herbi;

	// Save for analysis
	private static int[][][] saveBiofilmCon; // Biofilm condition [segment][time][compartment]
	private static double[][] savePartEutroph; // Part of segment eutrophic [segment][time]
	private static int[][][] saveBiofilmCon2; // Biofilm condition/type [segment][time][type/event apperance]
	private static double[][][] saveBiofilm; // Biofilm biomass [segment][time][type]
	private static double[][][] saveBiofilmRipOff; // Biofilm biomass [segment][time][type]
	private static double[][][] saveFeed; // Feed[segment][time][beforegrazing/aftergrazing]
	private static double[][][] saveDemand; // Demand[segment][time][inhabit/total/egg]
	private static double[][][][] saveDemandSpecies; // Demand[segment][time][inhabit/total/egg][Species]
	private static double[][][][] saveFlow; //Flow[segment][time][flow][species]
	private static double[][][][] saveFlowBiomass; //Flow[segment][time][flow][species]
	private static double[][][][] saveSpecies; // Species [segment][time][species][life form]
	private static double[][][] saveDispersalLoss; //Dispersal loss by imagines before and after dispersal [time][species][before/after dispersal]
	private static double[][][] saveSpeciesBiomass; // Species [segment][time][species] in mg
	private static double[][] savePf; //probability to develop filamentous algae [segment][month]
	
	
	//Output files
	static boolean OutAir, OutWater, OutBiofilm, OutCon, OutCon2, OutPartEu, OutpF = false;
	static boolean OutAbun, OutDemand, OutFeed, OutInfOutflow, OutDisLoss = false; 
	static boolean OutHerbi, OutInsecti, OutHerbiE, OutInsectiE, OutAbunBio = false;
	
	//
	static boolean ProofCoexist;
	static boolean ProofRange;
	static boolean coExist = true;
	
	/**
	 * Method to start simulation
	 */
	public static void main(String[] args) {
		if(args.length == 0) {
			args = new String[3];
			args[0]="";
			args[1]="1";
			args[2]="";
		}
		
		for (int run = 0; run < Integer.parseInt(args[1]); run++) {
			//System.out.println("Run: " + run);
						
			// Model initialization
			modelInit(args[0]);

			// -----------------------------------------Start_Simulation----------------------------------------------
			System.out.println("\n#############Start simulation: " + args[0] + " #############");
			while (((double) ts / 4.0) < simTime) {
				//System.out.println("**** Time step: " + ts + ", Month: "+ month + ", week: " + week + " ****");

			
				// ---------------Go through segments----------------------
				for (int k = 0; k < amountSegments; k++) {
						
					// Check for winter break
					if (month % 12 == 11 || month % 12 == 0 || month % 12 == 1) {
						winterbreak(k);
						
					} else {
						
						//System.out.println( "w=" + w + ", t="+ t);
						//System.out.println("---Segment nr." + k + "---");
					
						
						//save Inflow - includes already mortality by drift
						saveFlow[k][ts][0][0]=RiverSeg[k].Limpets.immigration[0]+RiverSeg[k].Limpets.immigration[1];
						saveFlow[k][ts][0][1]=RiverSeg[k].Mayflys.immigration[0];
						saveFlow[k][ts][0][2]=RiverSeg[k].Chironomids.immigration[0];
						saveFlow[k][ts][0][3]=RiverSeg[k].Stoneflys.immigration[0]+RiverSeg[k].Stoneflys.immigration[1]+RiverSeg[k].Stoneflys.immigration[2];
						
						saveFlowBiomass[k][ts][0][0]=RiverSeg[k].Limpets.immigration[0]*Limpet.setBodymass(0)+RiverSeg[k].Limpets.immigration[1]*Limpet.setBodymass(1);
						saveFlowBiomass[k][ts][0][1]=RiverSeg[k].Mayflys.immigration[0]*Mayfly.setBodymass();
						saveFlowBiomass[k][ts][0][2]=RiverSeg[k].Chironomids.immigration[0]*Chironomid.setBodymass();
						saveFlowBiomass[k][ts][0][3]=RiverSeg[k].Stoneflys.immigration[0]*Stonefly.setBodymass(0)+RiverSeg[k].Stoneflys.immigration[1]*Stonefly.setBodymass(1)+RiverSeg[k].Stoneflys.immigration[2]*Stonefly.setBodymass(2);
						
										
						
						//Filamentous event?
						savePf[k][month-startMonth] = RiverSeg[k].calculatePF2(pFMax,betaF);
						// Filamentous segment developing?
						if (Math.random() < RiverSeg[k].calculatePF2(pFMax,betaF)) {
							RiverSeg[k].biofilmCon[0] = 2; // filamentous alarm
							RiverSeg[k].BiofilmF = 0;
							if(SimulationMain.stochLength) {
								int randomNum = ThreadLocalRandom.current().nextInt(8, 12 + 1);
								RiverSeg[k].maxLifeCon = randomNum;	
							}else {
								RiverSeg[k].maxLifeCon = 12;
							}

							saveBiofilmCon2[k][ts][1] = 1; //track first time event
							if(grazingFila == 1) {
								RiverSeg[k].filaEvent();
							}
						}else {
							saveBiofilmCon2[k][ts][1] = 0;
						}
							
						// Life cycle issus
						if (week % 4 == 0) {
							RiverSeg[k].development();
						}
						
						
						// Mortality
						RiverSeg[k].mortalitySpecies(1); //full mortality
						RiverSeg[k].killMinimalSpecies();
																
						
						// Check if a biofilm rips off
						RiverSeg[k].BiofilmRip[0] = 0;
						RiverSeg[k].BiofilmRip[1] = 0;
						RiverSeg[k].BiofilmRip[2] = 0;
						if (RiverSeg[k].proofBeta1("BiofilmF") && RiverSeg[k].biofilmCon[0] != 2) {
							RiverSeg[k].biofilmRipsOff("BiofilmF");
						}
						if (RiverSeg[k].proofBeta1("BiofilmN")) {
							RiverSeg[k].biofilmRipsOff("BiofilmN");
						}

						if (RiverSeg[k].biofilmCon[0] == 2) {
							if (RiverSeg[k].ageOfEF < RiverSeg[k].maxLifeCon) {
								RiverSeg[k].ageOfEF++;
								RiverSeg[k].BiofilmF = 0;
							} else {
								RiverSeg[k].ageOfEF = 0;
								RiverSeg[k].biofilmRipsOff("BiofilmF");
							}
						}
						

						// Biofilm growth
						RiverSeg[k].biofilmGrowth();


						// Check for thick biofilm - handling of grazers
						if (RiverSeg[k].proofBeta0("BiofilmF") && RiverSeg[k].biofilmCon[0] != 2) {
							// *Biofilm could not be handled anymore*
							RiverSeg[k].biofilmCon[0] = 1;
							//System.out.println("Extensiv biofilm growth in Segment " + k + " in part BF");
						}
						if (RiverSeg[k].proofBeta0("BiofilmN")) {
							// *Biofilm could not be handled anymore*
							RiverSeg[k].biofilmCon[1] = 1;
							// RiverSeg[k].eutrophSeg();
							//System.out.println("Extensiv biofilm growth in Segment " + k + " in part BN");
						}
						
						
						// Calculate feed of the segment
						RiverSeg[k].calFeed();
						saveFeed[k][ts][0] = RiverSeg[k].feed;
						saveDemand[k][ts][0] = RiverSeg[k].askForDemand()[0];
						saveDemand[k][ts][1] = RiverSeg[k].askForDemand()[1];
						saveDemand[k][ts][2] = RiverSeg[k].askForDemand()[2];
						saveDemandSpecies[k][ts][0][0] = RiverSeg[k].askForDemandSpecies()[0];
						saveDemandSpecies[k][ts][0][1] = RiverSeg[k].askForDemandSpecies()[1];
						saveDemandSpecies[k][ts][0][2] = RiverSeg[k].askForDemandSpecies()[2];
						saveDemandSpecies[k][ts][0][3] = RiverSeg[k].askForDemandSpecies()[3];
						
						saveDemandSpecies[k][ts][1][0] = RiverSeg[k].askForDemandSpecies()[4];
						saveDemandSpecies[k][ts][1][1] = RiverSeg[k].askForDemandSpecies()[5];
						saveDemandSpecies[k][ts][1][2] = RiverSeg[k].askForDemandSpecies()[6];
						saveDemandSpecies[k][ts][1][3] = RiverSeg[k].askForDemandSpecies()[7];
						
						saveDemandSpecies[k][ts][2][0] = RiverSeg[k].askForDemandSpecies()[8];
						saveDemandSpecies[k][ts][2][1] = RiverSeg[k].askForDemandSpecies()[9];
						saveDemandSpecies[k][ts][2][2] = RiverSeg[k].askForDemandSpecies()[10];
						saveDemandSpecies[k][ts][2][3] = RiverSeg[k].askForDemandSpecies()[11];
						
						
						// Grazing
						RiverSeg[k].grazing();
						RiverSeg[k].calFeed();
						saveFeed[k][ts][1] = RiverSeg[k].feed;
						
						
						// Settlement of predator (--> possible implementation of predator)


						// Save data per time step
						saveBiofilmCon[k][ts][0] = RiverSeg[k].biofilmCon[0];
						saveBiofilmCon[k][ts][1] = RiverSeg[k].biofilmCon[1];
						saveBiofilmCon[k][ts][2] = RiverSeg[k].biofilmCon[2];
						saveBiofilmCon2[k][ts][0] = RiverSeg[k].biofilmCon[0];
						savePartEutroph[k][ts] = RiverSeg[k].partE();
						saveBiofilm[k][ts][0] = RiverSeg[k].BiofilmF;
						saveBiofilm[k][ts][1] = RiverSeg[k].BiofilmN;
						saveBiofilm[k][ts][2] = RiverSeg[k].BiofilmM;
						saveBiofilmRipOff[k][ts][0] = RiverSeg[k].BiofilmRip[0];
						saveBiofilmRipOff[k][ts][1] = RiverSeg[k].BiofilmRip[1];
						saveBiofilmRipOff[k][ts][2] = RiverSeg[k].BiofilmRip[2];

						saveSpecies[k][ts][0][0] = RiverSeg[k].Limpets.abundance[0];
						saveSpecies[k][ts][0][1] = RiverSeg[k].Limpets.abundance[1];
						saveSpecies[k][ts][0][2] = RiverSeg[k].Limpets.abundance[2];

						saveSpecies[k][ts][1][1] = RiverSeg[k].Mayflys.abundance[1];
						saveSpecies[k][ts][1][2] = RiverSeg[k].Mayflys.abundance[2];

						saveSpecies[k][ts][2][1] = RiverSeg[k].Chironomids.abundance[1];
						saveSpecies[k][ts][2][2] = RiverSeg[k].Chironomids.abundance[2];

						saveSpecies[k][ts][3][1] = RiverSeg[k].Stoneflys.abundance[1]
								+ RiverSeg[k].Stoneflys.abundance[2] + RiverSeg[k].Stoneflys.abundance[3];
						saveSpecies[k][ts][3][2] = RiverSeg[k].Stoneflys.abundance[4];
							
						//Save larvae as biomass
						saveSpeciesBiomass[k][ts][0] = RiverSeg[k].Limpets.abundance[1]*Limpet.setBodymass(0) + RiverSeg[k].Limpets.abundance[2]*Limpet.setBodymass(1);
						
						saveSpeciesBiomass[k][ts][1] = RiverSeg[k].Mayflys.abundance[1]*Mayfly.setBodymass();
						saveSpeciesBiomass[k][ts][2] = RiverSeg[k].Chironomids.abundance[1]*Chironomid.setBodymass();
						saveSpeciesBiomass[k][ts][3] = RiverSeg[k].Stoneflys.abundance[1]*Stonefly.setBodymass(0) + RiverSeg[k].Stoneflys.abundance[2]*Stonefly.setBodymass(1) + RiverSeg[k].Stoneflys.abundance[3]*Stonefly.setBodymass(2);
						
						//Save Outflowing/Emigrants
						saveFlow[k][ts][1][0]=RiverSeg[k].Limpets.emigration[0]+RiverSeg[k].Limpets.emigration[1];
						saveFlow[k][ts][1][1]=RiverSeg[k].Mayflys.emigration[0];
						saveFlow[k][ts][1][2]=RiverSeg[k].Chironomids.emigration[0];
						saveFlow[k][ts][1][3]=RiverSeg[k].Stoneflys.emigration[0]+RiverSeg[k].Stoneflys.emigration[1]+RiverSeg[k].Stoneflys.emigration[2];
						//System.out.println("und emi: " + saveFlow[k][w][1][3]);
						
						saveFlowBiomass[k][ts][1][0]=RiverSeg[k].Limpets.emigration[0]*Limpet.setBodymass(0)+RiverSeg[k].Limpets.emigration[1]*Limpet.setBodymass(1);
						saveFlowBiomass[k][ts][1][1]=RiverSeg[k].Mayflys.emigration[0]*Mayfly.setBodymass();
						saveFlowBiomass[k][ts][1][2]=RiverSeg[k].Chironomids.emigration[0]*Chironomid.setBodymass();
						saveFlowBiomass[k][ts][1][3]=RiverSeg[k].Stoneflys.emigration[0]*Stonefly.setBodymass(0)+RiverSeg[k].Stoneflys.emigration[1]*Stonefly.setBodymass(1)+RiverSeg[k].Stoneflys.emigration[2]*Stonefly.setBodymass(2);
						
						
						
						//Mortality of immigrating species
						RiverSeg[k].mortalityOutflow();
						
						//asychron Drift
						createDriftSeg(k);
					}
				
				}
				
				
				// synchron Drift of all segments (if necessary use data from loadInflow)
				// createDrift();

				//System.out.println("----Start dispersal----" + t);
				if (week % 4 == 0) {
					if(oviposition.equals("constant")) {
						dispersalImaginesConstant();
					}else if (oviposition.equals("dynamic")) {
						dispersalImagines();
					}else if(oviposition.equals("sum")) {
						dispersalImaginesSum();
					}else {
						System.out.println ("Strange ovipostion method (SimulationMain/main): " + oviposition);
					}
				}

				for (int k = 0; k < amountSegments; k++) {
					// save eggs
					saveSpecies[k][ts][1][0] = RiverSeg[k].Mayflys.abundance[0];
					saveSpecies[k][ts][2][0] = RiverSeg[k].Chironomids.abundance[0];
					saveSpecies[k][ts][3][0] = RiverSeg[k].Stoneflys.abundance[0];
				}

				
				//Increase time step
				week++;
				ts++;
				if (week % 4 == 0)
					month++;

				//Proof if a species already died out
				if(ProofCoexist) proofCoexistence(args[0]);
				
			}
			// -----------------------------------------End_of_simulation---------------------------------------------//

			//Proof if the abundance of the last simulated year are in the preferred range
			if(ProofRange) proofAbunRange(args[0]);
			
     		String filename = pathString + pathOutput + args[2] + "/Data_OutputSzenarios_"+run+".csv";

			if (coExist) saveData.saveParameterSz(args[0],filename);
			
			// Save Data in file at the end of simulation
			saveData.setScenario(args[0]);
			saveData.setRun(run);
			saveData.setPath(args[2]);
			if(OutCon) saveData.saveDataBiofilmC(saveBiofilmCon,0);
			if(OutCon2) saveData.saveDataBiofilmC(saveBiofilmCon2,2);
			if(OutPartEu) saveData.saveDataPartEutroph(savePartEutroph);
			if(OutBiofilm) saveData.saveDataBiofilm(saveBiofilm);
			if(OutBiofilm) saveData.saveDataBiofilmRipsOff(saveBiofilmRipOff);
			if(OutAbun) saveData.saveDataSpecies(saveSpecies);
			if(OutAbunBio) saveData.saveDataSpeciesBio(saveSpeciesBiomass);
			if(OutInfOutflow) saveData.saveDataInf(saveFlow,"Inflow","");
			if(OutInfOutflow) saveData.saveDataInf(saveFlow,"Outflow","");
			if(OutAbunBio) saveData.saveDataInf(saveFlowBiomass,"Inflow","Biomass");
			if(OutAbunBio) saveData.saveDataInf(saveFlowBiomass,"Outflow","Biomass");
			if(OutFeed) saveData.saveDataFeed(saveFeed);
			if(OutDemand) saveData.saveDataDemand(saveDemand);
			if(OutDemand) saveData.saveDataDemandSpecies(saveDemandSpecies);
			if(OutpF) saveData.saveDataPf(savePf);
			if(OutDisLoss) saveData.saveDataImagines(saveDispersalLoss);
		}
		if(OutWater) saveData.saveWaterTemp(waterTemperature);
		if(OutAir) saveData.saveAirTemp(amountSegments);
		if(OutHerbi) saveData.saveStressor("Herbicide", Stressors_Herbi);
		if(OutInsecti) saveData.saveStressor("Insecticide", Stressors_Insect);
		if(OutHerbiE) saveData.saveStressor("InsecticideEvent", Stressor_Event_Insect);
		if(OutInsectiE) saveData.saveStressor("HerbicideEvent", Stressor_Event_Herbi);
		
		System.out.println("\n#############End simulation: " + args[0] + " ###############");
		
	}


	// ***********************************************Model_initialisation****************************************//

	/**
	 * Model initialisation
	 * Load Parameters, intial conditions, Stressors, create segments
	 * @param sc scenario name
	 * 
	 */
	public static void modelInit(String sc) {
		// Load variables
		loadData.loadVariables(pathString + pathParam + "Parameters_"+ sc + ".csv");
		loadData.loadVariables(pathString  + pathParam + "ParametersSeg_"+ sc + ".csv");

		waterTemperature = WaterTemperature.setWaterTemperature2(airTemperatureMean,shadeArray,startMonth);
		if (tempConst) { //set constant water temperature over segments 
			System.out.println("tempConst:" + tempConst);
			waterTemperature = WaterTemperature.setWaterTemperatureConst(waterTemperature);
		}
		
		
		// Set start month
		month = startMonth;
		week = startMonth*4;
		ts = 0;

		coExist = true;

		// Load initial species
		InitialSpecies = new double[amountSegments][8];
		loadData.loadInitialSpecies(pathString + pathParam + "SpeciesTab_"+ sc + ".csv");

		// Load Stressors
		// Stressors_Insect = new double[amountSegments][simTime + 1];
		Stressors_Insect = loadData.loadStressors(
				pathString  + pathParam + "Stressors_Insecticide_"+ sc + ".csv");
		//saveData.saveStressor("Insecticide", Stressors_Insect);
		
		// Stressors_Herbi = new double[amountSegments][simTime + 1];
		Stressors_Herbi = loadData.loadStressors(
				pathString  + pathParam + "Stressors_Herbicide_"+ sc + ".csv");
		//saveData.saveStressor("Herbicide", Stressors_Herbi);
		

		// Create Segments with loaded environmental parameters and initial species numbers
		Stressor_Event_Insect = new double[amountSegments][simTime*4];
		Stressor_Event_Herbi = new double[amountSegments][simTime*4];
		loadData.loadStressorEvents(
				pathString  + pathParam + "Stressor_Events_"+ sc + ".csv");
		//saveData.saveStressor("InsecticideEvent", Stressor_Event_Insect);
		//saveData.saveStressor("HerbicideEvent", Stressor_Event_Herbi);
		
		loadData.loadOutFilesNames(pathString + pathParam + "OutputFiles_"+ sc + ".csv");
		
		createSegments();

		// Connect segments with in- and outflow or create artificial inflow
		if (amountSegments > 1) { // More than one segment
			// Cascade segments
			// createDrift(); // --> in a dentric system are here the upstream segments defined
		} else if (amountSegments == 1) { // Segment = 1
			// Load inflow
			artInflow = new double[simTime * 4 + 1][7]; // initial inflow(0) + 'simTim'-months with different species
			loadData.loadInflow(pathString  + pathParam + "Inflow_"+ sc + ".csv");
		} else {
			System.out.println("Amount of Segments is not valid- Connection of segments failed!");
		}

		// Initialization saveData
		saveBiofilmCon = new int[amountSegments][simTime * 4][3];
		saveBiofilmCon2 = new int[amountSegments][simTime * 4][2];
		savePartEutroph = new double[amountSegments][simTime * 4];
		saveBiofilm = new double[amountSegments][simTime * 4][3];
		saveBiofilmRipOff = new double[amountSegments][simTime * 4][3];
		saveFeed = new double[amountSegments][simTime * 4][2];
		saveDemand = new double[amountSegments][simTime * 4][3];
		saveDemandSpecies = new double[amountSegments][simTime * 4][3][4];
		saveSpecies = new double[amountSegments][simTime * 4][4][3]; // [,,species,life form]
		saveDispersalLoss = new double[simTime * 4][3][2]; //[,dispersal species, before/after]
		saveSpeciesBiomass = new double[amountSegments][simTime * 4][4]; // [,,species]
		saveFlow = new double[amountSegments][simTime * 4][2][4]; // [,,flow,species]
		saveFlowBiomass = new double[amountSegments][simTime * 4][2][4]; // [,,flow,species]
		riverOutflow = new double[simTime * 4][4];
		savePf = new double[amountSegments][simTime];
	}

	// ***********************************************Winter_break*************************************************//

	/**
	 * Act in winter months (December/January/February)
	 * @param k segment number
	 */
	public static void winterbreak(int k) {

		// no inflow or outflow changes

		// Save inflow
		saveFlow[k][ts][0][0] = 0.0;
		saveFlow[k][ts][0][1] = 0.0;
		saveFlow[k][ts][0][2] = 0.0;
		saveFlow[k][ts][0][3] = 0.0;

		saveFlowBiomass[k][ts][0][0] = 0.0;
		saveFlowBiomass[k][ts][0][1] = 0.0;
		saveFlowBiomass[k][ts][0][2] = 0.0;
		saveFlowBiomass[k][ts][0][3] = 0.0;
		
		
//		if (growWinter) {
//			//Biofilm growth trough winter
//			// Check if a biofilm rips off
//			if (RiverSeg[k].proofBeta1("BiofilmF") && RiverSeg[k].biofilmCon[0] != 2) {
//				RiverSeg[k].biofilmRipsOff("BiofilmF");
//			}
//			if (RiverSeg[k].proofBeta1("BiofilmN")) {
//				RiverSeg[k].biofilmRipsOff("BiofilmN");
//			}
//
//			if (RiverSeg[k].biofilmCon[0] == 2) {
//				if (RiverSeg[k].ageOfEF < RiverSeg[k].maxLifeCon) {
//					RiverSeg[k].ageOfEF++;
//					RiverSeg[k].BiofilmF = 0;
//				} else {
//					RiverSeg[k].ageOfEF = 0;
//					RiverSeg[k].biofilmRipsOff("BiofilmF");
//				}
//			}
//			
//			RiverSeg[k].biofilmGrowth();
//			
//			// Check for thick biofilm - handling of grazers
//			if (RiverSeg[k].proofBeta0("BiofilmF") && RiverSeg[k].biofilmCon[0] != 2) {
//				// *Biofilm could not be handled anymore*
//				RiverSeg[k].biofilmCon[0] = 1;
//			}
//			if (RiverSeg[k].proofBeta0("BiofilmN")) {
//				// *Biofilm could not be handled anymore*
//				RiverSeg[k].biofilmCon[1] = 1;
//			}
//			
//		}else {
		
		// Reset biofilm to eatable (B1)
			RiverSeg[k].biofilmCon[0] = 0;
			RiverSeg[k].biofilmCon[1] = 0;
			RiverSeg[k].ageOfEF = 0;
			RiverSeg[k].BiofilmF = B1 * (1 - RiverSeg[k].mu) * RiverSeg[k].epsilon;
			RiverSeg[k].BiofilmN = B1 * (1 - RiverSeg[k].mu) * (1 - RiverSeg[k].epsilon);
			RiverSeg[k].BiofilmM = B1 * RiverSeg[k].mu;
//		}
		
		// Life cycle issus
		//RiverSeg[k].development();
		RiverSeg[k].mortalitySpecies(scMort); // reduced mortality
		RiverSeg[k].killMinimalSpecies();

		// No biofilm growth
		RiverSeg[k].calFeed();
		// No grazing -> no drift -> no inflow -> no settlement
		// No Settlement of predator

		// Save data per time step
		// save biofilm condition
		saveBiofilmCon[k][ts][0] = RiverSeg[k].biofilmCon[0];
		saveBiofilmCon[k][ts][1] = RiverSeg[k].biofilmCon[1];
		saveBiofilmCon[k][ts][2] = RiverSeg[k].biofilmCon[2];
		saveBiofilmCon2[k][ts][0] = RiverSeg[k].biofilmCon[0];
		saveBiofilmCon2[k][ts][1] = 0;
		savePartEutroph[k][ts] = RiverSeg[k].partE();
		// Save Biofilm biomass
		saveBiofilm[k][ts][0] = RiverSeg[k].BiofilmF;
		saveBiofilm[k][ts][1] = RiverSeg[k].BiofilmN;
		saveBiofilm[k][ts][2] = RiverSeg[k].BiofilmM;
		// Save feed and demand of species
		saveFeed[k][ts][0] = RiverSeg[k].feed;
		saveFeed[k][ts][1] = RiverSeg[k].feed;
		
		saveDemand[k][ts][0] = 0.0;
		saveDemand[k][ts][1] = 0.0;
		saveDemand[k][ts][2] = 0.0;
		saveDemandSpecies[k][ts][0][0] = 0.0;
		saveDemandSpecies[k][ts][0][1] = 0.0;
		saveDemandSpecies[k][ts][0][2] = 0.0;
		saveDemandSpecies[k][ts][0][3] = 0.0;
		
		saveDemandSpecies[k][ts][1][0] = 0.0;
		saveDemandSpecies[k][ts][1][1] = 0.0;
		saveDemandSpecies[k][ts][1][2] = 0.0;
		saveDemandSpecies[k][ts][1][3] = 0.0;
		
		saveDemandSpecies[k][ts][2][0] = 0.0;
		saveDemandSpecies[k][ts][2][1] = 0.0;
		saveDemandSpecies[k][ts][2][2] = 0.0;
		saveDemandSpecies[k][ts][2][3] = 0.0;
		
		// Save abundances
		saveSpecies[k][ts][0][0] = RiverSeg[k].Limpets.abundance[0];
		saveSpecies[k][ts][0][1] = RiverSeg[k].Limpets.abundance[1];
		saveSpecies[k][ts][0][2] = RiverSeg[k].Limpets.abundance[2];
		saveSpecies[k][ts][1][0] = RiverSeg[k].Mayflys.abundance[0];
		saveSpecies[k][ts][1][1] = RiverSeg[k].Mayflys.abundance[1];
		saveSpecies[k][ts][1][2] = RiverSeg[k].Mayflys.abundance[2];
		saveSpecies[k][ts][2][0] = RiverSeg[k].Chironomids.abundance[0];
		saveSpecies[k][ts][2][1] = RiverSeg[k].Chironomids.abundance[1];
		saveSpecies[k][ts][2][2] = RiverSeg[k].Chironomids.abundance[2];
		saveSpecies[k][ts][3][0] = RiverSeg[k].Stoneflys.abundance[0];
		saveSpecies[k][ts][3][1] = RiverSeg[k].Stoneflys.abundance[1] + RiverSeg[k].Stoneflys.abundance[2]
				+ RiverSeg[k].Stoneflys.abundance[3];
		saveSpecies[k][ts][3][2] = RiverSeg[k].Stoneflys.abundance[4];
		//Save biomasse
		saveSpeciesBiomass[k][ts][0] = RiverSeg[k].Limpets.abundance[1]*Limpet.setBodymass(0) + RiverSeg[k].Limpets.abundance[2]*Limpet.setBodymass(1);
		saveSpeciesBiomass[k][ts][1] = RiverSeg[k].Mayflys.abundance[1]*Mayfly.setBodymass();
		saveSpeciesBiomass[k][ts][2] = RiverSeg[k].Chironomids.abundance[1]*Chironomid.setBodymass();
		saveSpeciesBiomass[k][ts][3] = RiverSeg[k].Stoneflys.abundance[1]*Stonefly.setBodymass(0) + RiverSeg[k].Stoneflys.abundance[2]*Stonefly.setBodymass(1) + RiverSeg[k].Stoneflys.abundance[3]*Stonefly.setBodymass(2);
			
		
		// Save outflowing species
		saveFlow[k][ts][1][0] = 0.0;
		saveFlow[k][ts][1][1] = 0.0;
		saveFlow[k][ts][1][2] = 0.0;
		saveFlow[k][ts][1][3] = 0.0;
		
		saveFlowBiomass[k][ts][1][0] = 0.0;
		saveFlowBiomass[k][ts][1][1] = 0.0;
		saveFlowBiomass[k][ts][1][2] = 0.0;
		saveFlowBiomass[k][ts][1][3] = 0.0;
			
		
		savePf[k][month-startMonth] = 0;

	}

	// **************************************************Dispersal***************************************************//

	/**
	 * Dispersal of imagines.Count imagines (per species) in each segment and
	 * calculate the probability for each segment to land there.
	 */
	public static void dispersalImagines() {
		double[][] disImagines = new double[amountSegments][3];
		double[][] landImagines = new double[amountSegments][3];

		// Count imagines of each species in each segment
		for (int k = 0; k < RiverSeg.length; k++) {
			disImagines[k][0] = RiverSeg[k].Mayflys.abundance[RiverSeg[k].Mayflys.abundance.length - 1];
			RiverSeg[k].Mayflys.abundance[RiverSeg[k].Mayflys.abundance.length - 1] = 0;

			disImagines[k][1] = RiverSeg[k].Chironomids.abundance[RiverSeg[k].Chironomids.abundance.length - 1];
			RiverSeg[k].Chironomids.abundance[RiverSeg[k].Chironomids.abundance.length - 1] = 0;
			
			if(grazingFila == 1) {
				disImagines[k][1] = disImagines[k][1] + RiverSeg[k].Chironomids.abundanceFila;
				RiverSeg[k].Chironomids.abundanceFila = 0;
			}

			disImagines[k][2] = RiverSeg[k].Stoneflys.abundance[RiverSeg[k].Stoneflys.abundance.length - 1];
			RiverSeg[k].Stoneflys.abundance[RiverSeg[k].Stoneflys.abundance.length - 1] = 0;
		}

		// Dispersal
		for (int k = 0; k < RiverSeg.length; k++) { // Taker
			for (int i = 0; i < RiverSeg.length; i++) { // Donor
				landImagines[k][0] = landImagines[k][0] + disImagines[i][0] * Mayfly.d((double) k - i);
				landImagines[k][1] = landImagines[k][1] + disImagines[i][1] * Chironomid.d((double) k - i);
				landImagines[k][2] = landImagines[k][2] + disImagines[i][2] * Stonefly.d((double) k - i);
			}
		}

		//Count dispersing and landing imagines	
		saveDispersalLoss[ts][0][0] = countImagines(disImagines,0);
		saveDispersalLoss[ts][1][0] = countImagines(disImagines,1);
		saveDispersalLoss[ts][2][0] = countImagines(disImagines,2);
		saveDispersalLoss[ts][0][1] = countImagines(landImagines,0);
		saveDispersalLoss[ts][1][1] = countImagines(landImagines,1);
		saveDispersalLoss[ts][2][1] = countImagines(landImagines,2);
		
		// Oviposition
		for (int k = 0; k < RiverSeg.length; k++) {
			RiverSeg[k].placeEggs(landImagines[k]);
		}

	} 
	
	
	/**
	 * Dispersal of imagines. Sum up the imagines and spread them equally to each segment
	 */
	public static void dispersalImaginesSum() {
		double[] disImagines = new double[] {0,0,0}; //Species: maylfy, chironomid, stonefly
		double[][] landImagines = new double[amountSegments][3];

		// Count imagines of each species 
		for (int k = 0; k < RiverSeg.length; k++) {
			disImagines[0] = disImagines[0] + RiverSeg[k].Mayflys.abundance[RiverSeg[k].Mayflys.abundance.length - 1];
			RiverSeg[k].Mayflys.abundance[RiverSeg[k].Mayflys.abundance.length - 1] = 0;

			disImagines[1] = disImagines[1] + RiverSeg[k].Chironomids.abundance[RiverSeg[k].Chironomids.abundance.length - 1];
			RiverSeg[k].Chironomids.abundance[RiverSeg[k].Chironomids.abundance.length - 1] = 0;

			disImagines[2] = disImagines[2] + RiverSeg[k].Stoneflys.abundance[RiverSeg[k].Stoneflys.abundance.length - 1];
			RiverSeg[k].Stoneflys.abundance[RiverSeg[k].Stoneflys.abundance.length - 1] = 0;
			
		}

		// Dispersal -  Equal amount in each segment
		for (int k = 0; k < RiverSeg.length; k++) { 
				landImagines[k][0] = landImagines[k][0] + (disImagines[0]/(double)amountSegments);
				landImagines[k][1] = landImagines[k][1] + (disImagines[1]/(double)amountSegments);
				landImagines[k][2] = landImagines[k][2] + (disImagines[2]/(double)amountSegments);
		}

		// Oviposition
		for (int k = 0; k < RiverSeg.length; k++) {
			RiverSeg[k].placeEggs(landImagines[k]);
		}

	} 
	
	
	
	/**
	 * Alternative method for oviposition: constant amount of eggs fall into each segment in oviposition month of each species
	 */
	public static void dispersalImaginesConstant() {
		double[] landImagines = new double[] {1,1,1}; //absolute values 
				
		// Oviposition
		for (int k = 0; k < RiverSeg.length; k++) {
			RiverSeg[k].placeEggs(landImagines);
		}
	}

	/**
	 * Count imagines 
	 * @param imagi data with imagines
	 * @param s species
	 * @return sum of imagines
	 */
	public static double countImagines(double[][] imagi, int s) {
		double sumImagi = 0;
		for(int i = 0; i < amountSegments; i++) {
			sumImagi= sumImagi+ imagi[i][s];
		}
		return sumImagi;
	}
	


	// *************************************Create_Segments_and_Drift************************************************//

	/**
	 * Create segments with environmental parameters and initial species numbers
	 */
	public static void createSegments() {
		// Cancel if segment number does not equal species distribution
		if (amountSegments <= 0 || amountSegments != InitialSpecies.length || amountSegments != waterTemperature.length
				|| amountSegments != shadeArray.length || amountSegments != epsilonArray.length || amountSegments != beta1Array.length
				|| amountSegments != muArray.length || amountSegments != deltaArray.length || amountSegments != nutArray.length || amountSegments != LUTArray.length ) {
			System.out.println("Segment not equal intial distribution length or is smaller than 1!");
			System.exit(0);
		}
		RiverSeg = new Segments[amountSegments]; // Create Segment array
		for (int i = 0; i < amountSegments; i++) {
			Segments R = new Segments(i, i + 1, LUTArray[i], waterTemperature[i], shadeArray[i], epsilonArray[i], muArray[i],
					deltaArray[i], nutArray[i], beta1Array[i], InitialSpecies[i], Stressors_Insect[i], Stressors_Herbi[i], Stressor_Event_Insect[i],
					Stressor_Event_Herbi[i]);
			RiverSeg[i] = R;
		}
	}

//	/**
//	 * Set new inflow by the upstream outflow and clear the outflow - Synchron drift
//	 *
//	 */
//	public static void createDrift() {
//		// One segment with artificial inflow
//		if (amountSegments == 1) {
//			//System.out.println("Create artificial larvae inflow...");
//			// Load inflow of previous loaded file
//			RiverSeg[0].Limpets.immigration[0] = artInflow[(w) % 12][0];
//			RiverSeg[0].Limpets.immigration[1] = artInflow[(w) % 12][1];
//			RiverSeg[0].Mayflys.immigration[0] = artInflow[(w) % 12][2];
//			RiverSeg[0].Chironomids.immigration[0] = artInflow[(w) % 12][3];
//			RiverSeg[0].Stoneflys.immigration[0] = artInflow[(w) % 12][4];
//			RiverSeg[0].Stoneflys.immigration[1] = artInflow[(w) % 12][5];
//			RiverSeg[0].Stoneflys.immigration[2] = artInflow[(w) % 12][6];
//
//			// Save and clear outflow
//			riverOutflow[w][0] = RiverSeg[0].Limpets.emigration[0] + RiverSeg[0].Limpets.emigration[1];
//			RiverSeg[0].Limpets.emigration[0] = 0;
//			RiverSeg[0].Limpets.emigration[1] = 0;
//
//			riverOutflow[w][1] = RiverSeg[0].Mayflys.emigration[0];
//			RiverSeg[0].Mayflys.emigration[0] = 0;
//
//			riverOutflow[w][2] = RiverSeg[0].Chironomids.emigration[0];
//			RiverSeg[0].Chironomids.emigration[0] = 0;
//
//			riverOutflow[w][3] = RiverSeg[0].Stoneflys.emigration[0] + RiverSeg[0].Stoneflys.emigration[1]
//					+ RiverSeg[0].Stoneflys.emigration[2];
//			RiverSeg[0].Stoneflys.emigration[0] = 0;
//			RiverSeg[0].Stoneflys.emigration[1] = 0;
//			RiverSeg[0].Stoneflys.emigration[2] = 0;
//		} else {
//
//			// Send emigrants to their lower reach as immigrants
//			for (int s = 0; s < RiverSeg.length - 1; s++) {
//				double inf1 = 0;
//				// Add outflow of recent segment as inflow of the lower segment + clear outflow
//				inf1 = RiverSeg[s].Limpets.emigration[0];
//				RiverSeg[RiverSeg[s].outSeg].Limpets.immigration[0] = inf1;
//				inf1 = RiverSeg[s].Limpets.emigration[1];
//				RiverSeg[RiverSeg[s].outSeg].Limpets.immigration[1] = inf1;
//				RiverSeg[s].Limpets.emigration[0] = 0;
//				RiverSeg[s].Limpets.emigration[1] = 0;
//
//				double inf2 = 0;
//				inf2 = RiverSeg[s].Mayflys.emigration[0];
//				RiverSeg[RiverSeg[s].outSeg].Mayflys.immigration[0] = inf2;
//				// System.out.println(RiverSeg[RiverSeg[s].outSeg].Mayflys.immigration[0] + ",
//				// "+ RiverSeg[s].Mayflys.emigration[0]);
//				RiverSeg[s].Mayflys.emigration[0] = 0;
//				// System.out.println(RiverSeg[RiverSeg[s].outSeg].Mayflys.immigration[0] + ",
//				// "+ RiverSeg[s].Mayflys.emigration[0]);
//
//				double inf3 = 0;
//				inf3 = RiverSeg[s].Chironomids.emigration[0];
//				RiverSeg[RiverSeg[s].outSeg].Chironomids.immigration[0] = inf3;
//				RiverSeg[s].Chironomids.emigration[0] = 0;
//
//				double[] infA = new double[3];
//				infA[0] = RiverSeg[s].Stoneflys.emigration[0];
//				infA[1] = RiverSeg[s].Stoneflys.emigration[1];
//				infA[2] = RiverSeg[s].Stoneflys.emigration[2];
//				RiverSeg[RiverSeg[s].outSeg].Stoneflys.immigration[0] = infA[0];
//				RiverSeg[RiverSeg[s].outSeg].Stoneflys.immigration[1] = infA[1];
//				RiverSeg[RiverSeg[s].outSeg].Stoneflys.immigration[2] = infA[2];
//				RiverSeg[s].Stoneflys.emigration[0] = 0;
//				RiverSeg[s].Stoneflys.emigration[1] = 0;
//				RiverSeg[s].Stoneflys.emigration[2] = 0;
//
//			}
//
//			// Last segment emigrants land in riverOutlflow - out of the system
//			// riverOutflow[w][0] = RiverSeg[RiverSeg.length -
//			// 1].Limpets.emigration[0] + RiverSeg[RiverSeg.length -
//			// 1].Limpets.emigration[1];
//			// actual (limpets are not able to drift):
//			riverOutflow[w][0] = 0;
//			// System.out.println(riverOutflow[w][0]);
//			RiverSeg[RiverSeg.length - 1].Limpets.emigration[0] = 0;
//			RiverSeg[RiverSeg.length - 1].Limpets.emigration[1] = 0;
//			// System.out.println(riverOutflow[w][0]);
//
//			riverOutflow[w][1] = RiverSeg[RiverSeg.length - 1].Mayflys.emigration[0];
//			// System.out.println(riverOutflow[w][1]);
//			RiverSeg[RiverSeg.length - 1].Mayflys.emigration[0] = 0;
//			// System.out.println(riverOutflow[w][1]);
//
//			riverOutflow[w][2] = RiverSeg[RiverSeg.length - 1].Chironomids.emigration[0];
//			// System.out.println(riverOutflow[w][2]);
//			RiverSeg[RiverSeg.length - 1].Chironomids.emigration[0] = 0;
//			// System.out.println(riverOutflow[w][2]);
//
//			riverOutflow[w][3] = RiverSeg[RiverSeg.length - 1].Stoneflys.emigration[0]
//					+ RiverSeg[RiverSeg.length - 1].Stoneflys.emigration[1]
//					+ RiverSeg[RiverSeg.length - 1].Stoneflys.emigration[2];
//			RiverSeg[RiverSeg.length - 1].Stoneflys.emigration[0] = 0;
//			RiverSeg[RiverSeg.length - 1].Stoneflys.emigration[1] = 0;
//			RiverSeg[RiverSeg.length - 1].Stoneflys.emigration[2] = 0;
//
//		}
//	}

	 /**
	 * Set new inflow by the upstream outflow and clear the outflow - for asynchron drift
	 * @param s segment
	 *
	 */
	 public static void createDriftSeg(int s) {
	 // One segment with artificial inflow
	 if (amountSegments == 1) {
		 // Load inflow of previous loaded file
		 RiverSeg[0].Limpets.immigration[0] = artInflow[(month) % 12][0];
		 RiverSeg[0].Limpets.immigration[1] = artInflow[(month) % 12][1];
		 RiverSeg[0].Mayflys.immigration[0] = artInflow[(month) % 12][2];
		 RiverSeg[0].Chironomids.immigration[0] = artInflow[(month) % 12][3];
		 RiverSeg[0].Stoneflys.immigration[0] = artInflow[(month) % 12][4];
		 RiverSeg[0].Stoneflys.immigration[1] = artInflow[(month) % 12][5];
		 RiverSeg[0].Stoneflys.immigration[2] = artInflow[(month) % 12][6];
	
		 // Save and clear outflow
		 riverOutflow[ts][0] = RiverSeg[0].Limpets.emigration[0] + RiverSeg[0].Limpets.emigration[1];
		 RiverSeg[0].Limpets.emigration[0] = 0;
		 RiverSeg[0].Limpets.emigration[1] = 0;
		
		 riverOutflow[ts][1] = RiverSeg[0].Mayflys.emigration[0];
		 RiverSeg[0].Mayflys.emigration[0] = 0;
		
		 riverOutflow[ts][2] = RiverSeg[0].Chironomids.emigration[0];
		 RiverSeg[0].Chironomids.emigration[0] = 0;
		
		 riverOutflow[ts][3] = RiverSeg[0].Stoneflys.emigration[0] + RiverSeg[0].Stoneflys.emigration[1] + RiverSeg[0].Stoneflys.emigration[2];
		 RiverSeg[0].Stoneflys.emigration[0] = 0;
		 RiverSeg[0].Stoneflys.emigration[1] = 0;
		 RiverSeg[0].Stoneflys.emigration[2] = 0;
	 } else {
	 // More than one segment - drift between upper and lower segments
		 if (s != amountSegments - 1) {
			 double inf11 = 0;
			 double inf12 = 0;
			 // Add outflow of recent segment as inflow of the lower segment + clear outflow
			 inf11 = RiverSeg[s].Limpets.emigration[0];
			 RiverSeg[RiverSeg[s].outSeg].Limpets.immigration[0] += inf11;
			 inf12 = RiverSeg[s].Limpets.emigration[1];
			 RiverSeg[RiverSeg[s].outSeg].Limpets.immigration[1] += inf12;
			 RiverSeg[s].Limpets.emigration[0] = 0;
			 RiverSeg[s].Limpets.emigration[1] = 0;
	
			 double inf2 = 0;
			 inf2 = RiverSeg[s].Mayflys.emigration[0];
			 RiverSeg[RiverSeg[s].outSeg].Mayflys.immigration[0] += inf2;
			 RiverSeg[s].Mayflys.emigration[0] = 0;
	
			 double inf3 = 0;
			 inf3 = RiverSeg[s].Chironomids.emigration[0];
			 RiverSeg[RiverSeg[s].outSeg].Chironomids.immigration[0] += inf3;
			 RiverSeg[s].Chironomids.emigration[0] = 0;
	
			 double[] infA = new double[3];
			 infA[0] = RiverSeg[s].Stoneflys.emigration[0];
			 infA[1] = RiverSeg[s].Stoneflys.emigration[1];
			 infA[2] = RiverSeg[s].Stoneflys.emigration[2];
			 RiverSeg[RiverSeg[s].outSeg].Stoneflys.immigration[0] += infA[0];
			 RiverSeg[RiverSeg[s].outSeg].Stoneflys.immigration[1] += infA[1];
			 RiverSeg[RiverSeg[s].outSeg].Stoneflys.immigration[2] += infA[2];
			 RiverSeg[s].Stoneflys.emigration[0] = 0;
			 RiverSeg[s].Stoneflys.emigration[1] = 0;
			 RiverSeg[s].Stoneflys.emigration[2] = 0;
	
		 } else { // last segment
			 riverOutflow[ts][0] = RiverSeg[s].Limpets.emigration[0] + RiverSeg[s].Limpets.emigration[1];
			 RiverSeg[s].Limpets.emigration[0] = 0;
			 RiverSeg[s].Limpets.emigration[1] = 0;
		
			 riverOutflow[ts][1] = RiverSeg[s].Mayflys.emigration[0];
			 RiverSeg[s].Mayflys.emigration[0] = 0;
		
			 riverOutflow[ts][2] = RiverSeg[s].Chironomids.emigration[0];
			 RiverSeg[s].Chironomids.emigration[0] = 0;
		
			 riverOutflow[ts][3] = RiverSeg[s].Stoneflys.emigration[0] + RiverSeg[s].Stoneflys.emigration[1] + RiverSeg[s].Stoneflys.emigration[2];
			 RiverSeg[s].Stoneflys.emigration[0] = 0;
			 RiverSeg[s].Stoneflys.emigration[1] = 0;
			 RiverSeg[s].Stoneflys.emigration[2] = 0;
			 
		 }
	 	}
	 }
	 
	 /**
	  * Proof the coexistence of species and breaks the simulation if one species dies out
	  * @param sz name of scenario
	  */
	 public static void proofCoexistence(String sz) {
		 //Mean over segment & last year
		 double abun1 = 0;
		 double abun2 = 0;
		 double abun3 = 0;
		 //double abun4 = 0;
		 
		 for (int s = 0; s < RiverSeg.length - 1; s++) {
			 abun1 = abun1 + RiverSeg[s].Limpets.abundance[0] + RiverSeg[s].Limpets.abundance[1] + RiverSeg[s].Limpets.abundance[2];
			 abun2 = abun2 + RiverSeg[s].Mayflys.abundance[0] + RiverSeg[s].Mayflys.abundance[1] + RiverSeg[s].Mayflys.abundance[2];
			 abun3 = abun3 + RiverSeg[s].Chironomids.abundance[0] + RiverSeg[s].Chironomids.abundance[1] + RiverSeg[s].Chironomids.abundance[2];
			 //double abun4 = RiverSeg[s].Stoneflys.abundance[0] + RiverSeg[s].Stoneflys.abundance[1] + RiverSeg[s].Stoneflys.abundance[2] + RiverSeg[s].Stoneflys.abundance[3] + RiverSeg[s].Stoneflys.abundance[4];
		 }
		 
		 abun1 = abun1/(double) amountSegments/ 600;
		 abun2 = abun2/(double) amountSegments/ 600;
		 abun3 = abun3/(double) amountSegments/ 600;
		 
		 if(abun1 < 1 || abun2 < 1 || abun3 < 1) { 
		 //if(abun1 < 0 || abun2 < 0 || abun3 < 0) { 
			 ts = simTime*4;
			 coExist = false; 
			 
			 System.out.println("Throw out (no coexistence): "+ sz);
			 
			//Not output files
			OutAir = false;
			OutWater = false;
			OutBiofilm = false;
			OutCon = false;
			OutCon2 = false;
			OutPartEu = false;
			OutpF = false;
			OutAbun = false;
			OutDemand = false;
			OutFeed = false;
			OutInfOutflow = false; 
			OutDisLoss = false;
			OutHerbi = false;
			OutInsecti = false;
			OutHerbiE = false;
			OutInsectiE = false;
			OutAbunBio = false;
		 }
	 
	 }
	 
		 /**
		  * Proof the coexistence of species and breaks the simulation if one species dies out
		  * @param sz name of scenario
		  */
		 public static void proofAbunRange(String sz) {
			 //Mean over segment & last year
			 double abun1 = 0;
			 double abun2 = 0;
			 double abun3 = 0;
			 //double abun4 = 0;
			 double abun2up = 0;
			 double abun2down = 0;
			 
			 for(int t = (simTime*4)- 48; t < (simTime*4); t++) {	//last year of simulation
				 for (int s = 0; s < RiverSeg.length - 1; s++) {	//over all segments
					 abun1 = abun1 + saveSpecies[s][t][0][1] + saveSpecies[s][t][0][2]; //Limpet
					 abun2 = abun2 + saveSpecies[s][t][1][1];							//Mayfly
					 abun3 = abun3 + saveSpecies[s][t][2][1];							//Chironomid
					 //double abun4 =  abun4 + saveSpecies[s][t][3][1] + saveSpecies[s][t][3][2] + saveSpecies[s][t][3][3]; //Stonefly
					 if(s < 200) abun2up = abun2up + saveSpecies[s][t][1][1];
					 else abun2down = abun2down + saveSpecies[s][t][1][1];
			 	}
			 }
			 
			 //Abundance converted to mean per segment, per m2, per timestep
			 abun1 = abun1/(double) amountSegments /600 /48;
			 abun2 = abun2/(double) amountSegments /600 /48;
			 abun3 = abun3/(double) amountSegments /600 /48;
			 abun2up = abun2up/ 200 /600 /48;
			 abun2down = abun2down/ 279 /600 /48;
			 
			 
			 //if(abun1 < 30 || abun1 > 200 || abun2 < 20 || abun2 > 200 || abun3 < 300 || abun3 > 1500) {
			 if(abun1 < 20 || abun1 > 200 || abun2up < 20 || abun2up > 200 || abun2down < 20 || abun2down > 100 || abun3 < 300 || abun3 > 2000) {
				 coExist = false; 
				 
				 System.out.println("Throw out(not in preferred range): "+ sz);
				 
				//Not output files
				OutAir = false;
				OutWater = false;
				OutBiofilm = false;
				OutCon = false;
				OutCon2 = false;
				OutPartEu = false;
				OutpF = false;
				OutAbun = false;
				OutDemand = false;
				OutFeed = false;
				OutInfOutflow = false; 
				OutDisLoss = false;
				OutHerbi = false;
				OutInsecti = false;
				OutHerbiE = false;
				OutInsectiE = false;
				OutAbunBio = false;
				
				
				
				deleteFile(pathString + pathParam + "Parameters_"+ sz + ".csv");
				deleteFile(pathString  + pathParam + "ParametersSeg_"+ sz + ".csv");
				deleteFile(pathString + pathParam + "SpeciesTab_"+ sz + ".csv");
				deleteFile(pathString  + pathParam + "Stressors_Insecticide_"+ sz + ".csv");
				deleteFile(pathString  + pathParam + "Stressors_Herbicide_"+ sz + ".csv");
				deleteFile(pathString  + pathParam + "Stressor_Events_"+ sz + ".csv");
				deleteFile(pathString + pathParam + "OutputFiles_"+ sz + ".csv");
				
//				deleteFile(pathString + pathOutput + saveData.getPath() + "/" + "Parameters_"+ sz + ".csv");
//				deleteFile(pathString  + pathOutput + saveData.getPath() + "/" + "ParametersSeg_"+ sz + ".csv");
//				deleteFile(pathString + pathOutput + saveData.getPath() + "/" + "SpeciesTab_"+ sz + ".csv");
//				deleteFile(pathString  + pathOutput + saveData.getPath() + "/" + "Stressors_Insecticide_"+ sz + ".csv");
//				deleteFile(pathString  + pathOutput + saveData.getPath() + "/" + "Stressors_Herbicide_"+ sz + ".csv");
//				deleteFile(pathString  + pathOutput + saveData.getPath() + "/" + "Stressor_Events_"+ sz + ".csv");
//				deleteFile(pathString + pathOutput + saveData.getPath() + "/" + "OutputFiles_"+ sz + ".csv");
				
			 } 
		 
	 }
		
	/**
	 * Delete file 
	 * @param filename file name which should be deleted
	 */
	public static void deleteFile(String filename) {
		try {

			File file = new File(filename);

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

}
