# **MASTIFF MODEL**
MASTIFF (Multiple Aquatic STressors In Flowing Food webs) is a spatially explivit simulation model for multi-stressed riverine systems which supports systematic analyses of organismic drift and dispersal processes and their functional importance for (i) the community structure of benthic grazers, (ii) the performance of grazer-mediated control of eutrophication, and (iii) the ability to counteract adverse effects of local stressors in the different river segments.

## INSTALLATION

Requirements:
> Java Version 1.8.0_271 (Java 8)

Recommended software for development:
> SceneBuilder for Java 8 (8.5.0)
>
> Eclipse IDE for Java Developers (2019-09 R, 4.13.0)


### Open model GUI via jar file:
Jar file is placed in '...\mastiff-main\Mastiff\'

- Double-click [runMASTIFF.jar](https://git.ufz.de/oesa/mastiff/-/blob/main/runMASTIFF.jar) (if it does not open, check in 'Registrierungs-Editor' under 'Computer\HKEY_CLASSES_ROOT\jarfile\shell\open\command' the path points on the right java version)

or

- via command line (cmd): 
    ```sh
    java -jar MASTIFF.jar
    ``` 
    (check `java -version` for version)

...and choose scenarios options via GUI. 


### How to use MASTIFF as model developer:

#### Download model source files
- Click at the 'download' button next to the blue 'Clone' button at the open source [git project page](https://git.ufz.de/oesa/mastiff) and unpack the model in your prefenced folder

#### Install Eclipse and open project
- Download Eclipse and install it
- Import Projects from file system
- Click Directory and select path until ../mastiff-main/Mastiff/Java model project
- Click finish

#### Install javafx
- Help &rarr; Eclipse Marketplace
- Search for 'fx' and select 'e(fx)eclipse' 
- Accept license
- Download: [JavaFX Windows x64 SDK](https://gluonhq.com/products/javafx/) (or specific SDK for your PC)  and unpack it (remember path!)
- Click Window &rarr; Preferences &rarr; Search for 'user' and select 'User Libraries' under Java/Build Path 
- Click New and choose 'JaxaFX' as a name
- Click Add External JARs and select path where you unpack jaxafx &rarr; choose lib folder and mark all .jar and click open
- Click apply and close
- Click right on your project (left hand side) &rarr; Build Path &rarr; Add Libraries &rarr; User Library &rarr; Check JavaFX &rarr; Click Finish
- Click Run &rarr; Run Configurations &rarr; click Arguments
- Tip in under VM arguments: --module-path "your/path/lib" --add-modules javafx.controls,javafx.fxml
- Click apply and click run

#### Install math package
- Download: [org.apache.commons.math3.distribution](https://jar-download.com/artifacts/org.apache.commons/commons-math3) and unpack it (remember path!)
- Click Window &rarr; Preferences &rarr; Search for 'user' and select 'User Libraries' under Java/Build Path 
- Click New and choose 'Math' as a name
- Click Add External JARs and select path where you unpack it &rarr; choose .jar and click open
- Click apply and close
- Click right on your project (left hand side) &rarr; Build Path &rarr; Add Libraries &rarr; User Library &rarr; Check Math &rarr; Click Finish

#### Install SceneBuilder and change GUI
- Download [SceneBuilder für Java 8](https://gluonhq.com/products/scene-builder/)
- Click Windows &rarr; Preferences 
- Search javafx, select SceneBuilder executable under the path where you installed the SceneBuilder
- Click apply, and click apply and close
- Go to src/application.view (e.g. in your project in Eclipse left hand side) &rarr; right click MainLayout.fxml &rarr; open with SceneBuilder

#### Run Model via Eclipse
Run Main.java (in ...\mastiff-main\Mastiff\Java model project\src\application) as Java application (e.g. via Eclipse IDE) and choose scenarios options via GUI. 

### Supplementary Ubuntu 22.04 Install Instructions

1. Install OpenJDK and OpenJFX

   ```bash
   hsudo apt install -y java-default openjfx
   ```
2. Start Mastiff

   ```sh
   java --module-path /usr/share/openjfx/lib  --add-modules javafx.controls,javafx.fxml -jar runMASTIFF.jar
   ```

## SET PARAMETERS

Mastiff is modular in design. Therefore, you can switch on and off different processes in the GUI 
interface, including options/functions/processes that are not included in ODD protocol yet. 

Left side: 
- Simulation parameters (amount segments, simulation time in months), initialisation for biofilm biomass and abundances (x:y defines the range, e.g. increasing intial abundance, if x = y every segmen has the same abundance)
- Button for intialize parameters (save parameters) and Button for run the model
- Number runs, Name Scenario, Path in which the output should be saved

Middle:
- Environmental parameters: air temperature, land use profile by defining land use types for each segment (options: individual by A-F, sequence (Holtemme), load file), minimum nutrient factor (min eN)
- Biofilm parameters (growth rate, B0) and compartments (epsilon, my), Eutrophication parameters (thresholds beta0 and beta 1, pfmax, betaf)
- Species characteristics: Mortality(mortality rates, scale, drift mortality), Reproduction (offspring, dispersal probability and distances)
- Trohic interaction

Right side:
- Stressors: constant input of herbicides and insecticides in segments with agriculture and WWTP, input of pesticides by event
- Output Files (see OUTPUT/RESULT FILES)

Table (lower right):
- Segment specific parameters (number, land use type, maximal shading factor, epsilon, my, drift mortality, nutrient factor, biofilm biomass, abundance for each species, constant herbicide/insecticide input, beta1)
&rarr; double-click: change a parameter in a segment individually

<img src="GUI.PNG" width = 900>  


## RUNNING

Single scenario: 
> Choose tab single scenario, press 'Initialize Parameter' and afterwards 'Run Model'

Multiple scenario (e.g. sensitive analysis): 
>Define parameter variation in MainController.java method onButtonClickvaryPar(). Choose tab multiple scenarios and define scenario name and path. Decide if result files should be filtered by coexistence of species or/and range of abundance. Press Run multiple. 


## OUTPUT/RESULT FILES

Output files could be choosen on the mid right. Possible are air temperature, water temperature, biofilm biomass, biofilm condition, probability of filamentous algae (pf), abundance of each species, demand of each species, Available feed, inflow and outflow of organisms, dispersal loss, abundance in biomass, diffusive herbicide and insecticides, event herbicide and insecticides.

You can find result files starting with [.jar](https://git.ufz.de/oesa/mastiff/-/blob/main/runMASTIFF.jar) in ...\mastiff-main\Data\Results

You can find result files starting manually via Eclipse in ...\mastiff-main\Mastiff\Java model project\data\Results\testfolder


## NOTE

A few options/function shown in GUI are implemented and tested but not relevant for publications and not mentioned in ODD. For further studies interfaces are build (e.g. predator 'Stonefly' eating on grazers, pesticide module) but not coupled or tested sufficiently. 

## ODD protocol
A complete, detailed model description, following the ODD protocall is provided [here](https://git.ufz.de/oesa/mastiff/-/blob/main/Model%20description.pdf).

<img src="Mastiff/Java model project/src/application/Icon_Mastiff2.png" width = 100>  
